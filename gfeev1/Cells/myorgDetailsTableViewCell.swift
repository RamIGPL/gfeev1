//
//  myorgDetailsTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 12/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class myorgDetailsTableViewCell: UITableViewCell
{
    @IBOutlet weak var icon: UIImageView!
     @IBOutlet weak var orgTtle: UILabel!
    @IBOutlet weak var orgImage: UIImageView!
     @IBOutlet weak var org_subTitle: UILabel!
    @IBOutlet weak var org_Description: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
