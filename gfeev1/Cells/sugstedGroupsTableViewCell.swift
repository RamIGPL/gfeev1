//
//  sugstedGroupsTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 14/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import  SDWebImage
class sugstedGroupsTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var groupTitle: UILabel!
    
    @IBOutlet weak var unfollowBtn: UIButton!
    @IBOutlet weak var groupSubtitle: UILabel!
    @IBOutlet weak var selectGroupTypeBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func requestImage(fromURL: String)
     {
         if let url = URL(string: fromURL)
         {
             //            imgView.bmo_setImageWithURL(url, style: .circlePie(borderShape: true), placeholderImage: PlaceHolderImages.defaultImage, completion:nil)
             
//             profilePic.af_setImage(
//                 withURL: url,
//                 placeholderImage: UIImage(named: "defaultImage"),
//                 filter: BlurFilter(blurRadius: 0),
//                 imageTransition: .crossDissolve(1.0)
//             )

            profilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
         }
     }

}
