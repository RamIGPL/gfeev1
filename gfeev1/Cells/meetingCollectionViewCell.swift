//
//  meetingCollectionViewCell.swift
//  gfeev1
//
//  Created by IGPL on 07/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class meetingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var locationDetails: UILabel!
    
    @IBOutlet weak var meetingTitle: UILabel!
}
