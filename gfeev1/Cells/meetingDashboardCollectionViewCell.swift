//
//  meetingDashboardCollectionViewCell.swift
//  gfeev1
//
//  Created by IGPL on 18/03/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class meetingDashboardCollectionViewCell: UICollectionViewCell

{
      @IBOutlet weak var meetingTitle: UILabel!
      @IBOutlet weak var meetingDescription: UILabel!
      @IBOutlet weak var meetingDate: UILabel!
      @IBOutlet weak var meetingTime: UILabel!
}
