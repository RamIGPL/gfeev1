//
//  followerTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 06/02/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class followerTableViewCell: UITableViewCell
{
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    func requestImageforIcon(fromURL: String)
    {
        if let url = URL(string: fromURL)
        {
            profileImg.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
}
