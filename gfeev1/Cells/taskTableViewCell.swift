//
//  taskTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 07/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class taskTableViewCell: UITableViewCell {

    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionTask: UILabel!
    
    @IBOutlet weak var sDate: UILabel!
     @IBOutlet weak var eDate: UILabel!
     @IBOutlet weak var remainingTime: UILabel!
     @IBOutlet weak var estimatedTime: UILabel!
     //@IBOutlet weak var sDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
