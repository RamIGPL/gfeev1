//
//  createOrgViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/12/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class createOrgViewController: UIViewController,UIPopoverPresentationControllerDelegate,selectedIndType,selectedOrgType {
    let createOrgURl = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/insertorganization"
    //internal typealias CompletionHandler = ( _ responseObject : createOrgModel?, _ errorObject : NSError?) -> ()
    internal typealias CompletionHandler = ( _ responseObject : createOrgModel?,  _ errorObject : NSError?) -> ()
    func selected(selected: String) {
        orgTypeOutlet.setTitle(selected, for: .normal)
    }
    
    func selectedIndustry(selected: String) {
        print(selected)
        indTypeOutlet.setTitle(selected, for: .normal)
        
    }
    var verification = 0
    var visibiltiy = 0
    var imagepicker = UIImagePickerController()
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var orgName: UITextField!
    @IBOutlet weak var securityNo: UITextField!
    @IBOutlet weak var contactNo: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var websiteURL: UITextField!
    
    @IBOutlet weak var check1Outlet: UIButton!
    @IBOutlet weak var check2Outlet: UIButton!
    @IBOutlet weak var orgTypeOutlet: UIButton!
     @IBOutlet weak var indTypeOutlet: UIButton!
    @IBOutlet weak var imgOutlet: UIButton!
    
 //   @IBOutlet weak var orgName: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func ch1Action(_ sender: UIButton)
    {
      //  check1Outlet.setImage(UIImage(named: "CheckFill"), for: .normal)
       if sender.isSelected
       {
            sender.isSelected = false
            visibiltiy = 0
       }
        else
       {
           sender.isSelected = true
            visibiltiy = 1
        }
    }
    
    @IBAction func ch2Action(_ sender: UIButton)
    {
       // check2Outlet.setImage(UIImage(named: "CheckFill"), for: .normal)
            if sender.isSelected
              {
                   sender.isSelected = false
                   verification = 0
               }
               else
              {
                  sender.isSelected = true
                   verification = 1
               }
    }
    @IBAction func orgType(_ sender: UIButton)
    {
        let popController = self.storyboard?.instantiateViewController(withIdentifier: "orgTypeDropdownViewController") as! orgTypeDropdownViewController
        popController.Delegate = self
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.preferredContentSize.height = 300
        popController.preferredContentSize.width =  270
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        self.present(popController, animated: true, completion: nil)
    }
        
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func industryType(_ sender: UIButton)
    {
        let popController = self.storyboard?.instantiateViewController(withIdentifier: "industryDropdownViewController") as! industryDropdownViewController
                popController.Delegate = self
                 popController.modalPresentationStyle = UIModalPresentationStyle.popover
                 popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
                 popController.preferredContentSize.height = 300
                 popController.preferredContentSize.width =  270
                 popController.popoverPresentationController?.delegate = self
                 popController.popoverPresentationController?.sourceView = sender
                 popController.popoverPresentationController?.sourceRect = sender.bounds
                 self.present(popController, animated: true, completion: nil)
        
    }
    @IBAction func attachDoc(_ sender: UIButton)
    {
        pickingImg()
    }
    @IBAction func createOrg(_ sender: UIButton)
    {
        let orgTitle = orgName.text!
        let tinNumber = Int(securityNo.text!)
        let phone = Int(contactNo.text!)
        let gmail = email.text!
        let web = websiteURL.text!
        //let tinNumber = securityNo.text!
        
        
        let userid = UserDefaults.standard.integer(forKey: "userid")
            let accesskey = UserDefaults.standard.string(forKey: "accesskey")
            let organizationtype = 2
            let industrytype = 2
            let orgname = orgTitle
            let tinNo = tinNumber
            let contact = phone
            let email  = gmail
            let weburl = web//"www.rra.com"
//            let verification = 1
//            let visibiltiy = 1
            let country = 1
            let state = 1
            let city = 1
            let landmark = "aaaaaa"
            let pincode = 1234567
        
         if self.orgName.text == "" && self.securityNo.text == "" && self.contactNo.text == ""  && self.email.text == "" && self.websiteURL.text == ""
            {
                DispatchQueue.main.async
                {
                    self.view.makeToast("Fields are empty..!")
                }
                
            }
            else
            {
            
                self.getapi(userid: userid, accesskey: accesskey!, organizationtype: organizationtype, industrytype: industrytype, orgname: orgname, tinNo: tinNo!, contact: contact!, email: email, weburl: weburl, verification: verification, visibiltiy: visibiltiy, country: country, state: state, city: city, landmark: landmark, pincode: pincode, callback:  { (orgData, error) in
                if let orgData = orgData
                {
                    DispatchQueue.main.async
                    {
                        self.view.makeToast(orgData.message as? String)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "toOrganization") as! SuggestedOrgaViewController
                        let navigationController = UINavigationController(rootViewController: vc)
                        self.present(navigationController, animated: true, completion: nil)
                        self.show(navigationController, sender: self)
                        
                    }
                    
                    for obj in orgData.orgDetails!
                    {
                        
                        print(obj.orgphone!)
                    }

                }
                else
                {
                     DispatchQueue.main.async
                    {
                        self.view.makeToast("No Data..!")
                    }
                }
                
            })
        }
}
    
    func getapi(userid:Int,accesskey:String,organizationtype:Int,industrytype:Int,orgname:String,tinNo:Int,contact:Int,email:String,weburl:String,verification:Int,visibiltiy:Int,country:Int,state:Int,city:Int,landmark:String,pincode:Int, callback: @escaping CompletionHandler)
    {
        print("GEtting data's are: --- ",userid,accesskey,organizationtype,orgname,tinNo,contact,email,verification)
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&organizationtype=\(organizationtype)".data(using: String.Encoding.utf8)!)
        postData.append("&industrytype=\(industrytype)".data(using: String.Encoding.utf8)!)
        postData.append("&orgname=\(orgname)".data(using: String.Encoding.utf8)!)
        postData.append("&tinNo=\(tinNo)".data(using: String.Encoding.utf8)!)
        postData.append("&contact=\(contact)".data(using: String.Encoding.utf8)!)
        postData.append("&email=\(email)".data(using: String.Encoding.utf8)!)
        postData.append("&weburl=\(weburl)".data(using: String.Encoding.utf8)!)
        postData.append("&verification=\(verification)".data(using: String.Encoding.utf8)!)
           postData.append("&visibiltiy=\(visibiltiy)".data(using: String.Encoding.utf8)!)
           postData.append("&country=\(country)".data(using: String.Encoding.utf8)!)
           postData.append("&state=\(state)".data(using: String.Encoding.utf8)!)
           postData.append("&city=\(city)".data(using: String.Encoding.utf8)!)
           postData.append("&landmark=\(landmark)".data(using: String.Encoding.utf8)!)
           postData.append("&pincode=\(pincode)".data(using: String.Encoding.utf8)!)
           
        
        let request = NSMutableURLRequest(url: NSURL(string: createOrgURl)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let josnResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let currentObj = createOrgModel.mycreateOrg(data:josnResult)
                       // let localObj = orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(currentObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func photoAccess(_ sender: UIButton)
    {
        pickingImg()
    }
    
    @IBAction func back(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
}

extension createOrgViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func pickingImg()//30426702617
    {
        imagepicker.delegate = self
        view.endEditing(true)
        let alert = UIAlertController(title: "ChooseImage", message: nil, preferredStyle: .alert)
        let alert1 = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        { UIAlertAction in
            self.openCamera()
        }
        let alert2 = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        { UIAlertAction in
            self.openGallery()
            
        }
        let alert3 = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        { UIAlertAction in
            
        }
        alert.addAction(alert1)
        alert.addAction(alert2)
        alert.addAction(alert3)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagepicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagepicker, animated: true, completion: nil)
        }
        else
        {
            print("Required Device")
        }
    }
    
    func openGallery()
    {
        imagepicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagepicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        picker.dismiss(animated: true, completion: nil)
        let imgdata = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
       // self.img.image = imgdata
        imgOutlet.setImage(imgdata, for: .normal)
        imgOutlet.layer.cornerRadius = 30
        imgOutlet.layer.masksToBounds = true
        //imgOutlet.layer.borderWidth = 0.5
        if let imgurl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            let imgName = imgurl.lastPathComponent
        }
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("Cancel..")
    }
}

class createOrgModel:NSObject
{
    var orgDetails:[createOrgModel]?
    var orgid:Int?
    var customerid:Int?
    var logo:String?
    var desciption:String?
    var followers:Int?
    var date:String?
    var status:Int?
    var orgurl:String?
    var orgphone:Int?
    var orgemail:String?
    var industrytypeid:Int?
    var industryname:String?
    var countryid:Int?
    var country:String?
    var stateid:Int?
    var state:String?
    var cityid:Int?
    var city:String?
    var address:String?
    var pincode:String?
    var publickey:Int?
    var response_status:Int?
    var message:String?
    var code:Int?
    
    class func mycreateOrg(data:NSDictionary) -> createOrgModel
    {
        let mycreateObj = createOrgModel()
        mycreateObj.response_status = data["status"] as? Int
        mycreateObj.message = data["message"] as? String
        mycreateObj.code = data["code"] as? Int
        
        if let myOrgdetails = data["organizationdetails"] as? NSArray
        {
            var localorgDetails:[createOrgModel] = []
            for obj in myOrgdetails
            {
                let localObj = createOrgModel.organizationDetails(data: obj as! NSDictionary)
                localorgDetails.append(localObj)
            }
            mycreateObj.orgDetails = localorgDetails
        }
            return mycreateObj
        }
    
    class func organizationDetails(data:NSDictionary) -> createOrgModel
    {
        let obj = createOrgModel()
        obj.orgid = data["orgid"] as? Int
        obj.customerid = data["customerid"] as? Int
        obj.logo = data["logo"] as? String
        obj.desciption = data["desciption"] as? String
        obj.followers = data["followers"] as? Int
        obj.date = data["date"] as? String
        obj.status = data["status"] as? Int
        obj.orgurl = data["orgurl"] as? String
        obj.orgphone = data["orgphone"] as? Int
        obj.orgemail = data["orgemail"] as? String
        obj.industrytypeid = data["industrytypeid"] as? Int
        obj.industryname = data["industryname"] as? String
        obj.countryid = data["countryid"] as? Int
        obj.country = data["country"] as? String
        obj.orgemail = data["orgemail"] as? String
        obj.stateid = data["stateid"] as? Int
        obj.state = data["state"] as? String
        obj.cityid = data["cityid"] as? Int
        obj.city = data["city"] as? String
        obj.address = data["address"] as? String
        obj.pincode = data["pincode"] as? String
        obj.city = data["city"] as? String
        obj.publickey = data["public"] as? Int
        return obj
    }
}
 
