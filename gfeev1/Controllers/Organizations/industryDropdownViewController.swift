//
//  industryDropdownViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/12/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
protocol selectedIndType {
    func selectedIndustry(selected:String)
}
class industryDropdownViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    internal typealias CompletionHandler = ( _ responseObject : indTypModel?,  _ errorObject : NSError?) -> ()
    let industryTypeURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/industrytype"
    var orgType = ["Select Industry Type","Company","School","College","Institute","Other"]
    @IBOutlet weak var tv: UITableView!
    var myTableArray:[String] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        let userid = 174
//        let accesskey = "a4a6347ebfaded84d6c8c2895c9d2399"
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
        
        self.getapi(userid:userid,accesskey:accesskey!, callback: { (logdata, error) in
            if let logdata = logdata
        {
           // print(logdata.status!,"mylogdata")
            print("in01 \(String(describing: logdata.message))")
            for obj in logdata.industrytype!
            {
               // print(obj.name!,"Different types are:")
                self.myTableArray.append(obj.name!)
                DispatchQueue.main.sync
                {
                    self.tv.reloadData()
                }
             }
         }
        })
    }
    
    func getapi(userid:Int,accesskey:String,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: industryTypeURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let josnResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        //let localObj = orgTypModel.orgTypefirstLayer(data: josnResult)
                        let localObj = indTypModel.indTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    var Delegate:selectedIndType? 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return orgType.count
        return myTableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "industryDropdownViewController", for: indexPath)
        cell.textLabel?.text = myTableArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // typeDelegate?.selectedOrg(selected: orgType[indexPath.row])
        Delegate?.selectedIndustry(selected: myTableArray[indexPath.row])
        dismiss(animated: true, completion: nil)
    }

}


class indTypModel : NSObject
{
    var industrytype:[indTypModel]?
    var id:Int?
    var name:String?
    var status:Int?
    var message:String?
    var code:Int?
    
    class func indTypefirstLayer(data:NSDictionary) -> indTypModel
    {
        let myobj = indTypModel()
        myobj.status = data["status"] as? Int
        myobj.code = data["code"] as? Int
        myobj.message = data["message"] as? String
       // myobj.orgtype = data["orgtype"] as? NSArray
         if let myIndData = data["industrytype"] as? NSArray
         {
            var myDataArray:[indTypModel] = []
            for obj in myIndData
            {
                let localObj = indTypModel.myindType(data: obj as! NSDictionary)
                print(localObj,"individual object of data")
                myDataArray.append(localObj)
            }
            myobj.industrytype = myDataArray
        }
        return myobj
    }
    
   class func myindType(data:NSDictionary) -> indTypModel
   {
        let myobj = indTypModel()
        myobj.id = data["id"] as? Int
        myobj.name = data["name"] as? String
        return myobj
  }
}

//enum myerror:Error {
//    case responseError(reason:String)
//}
