//
//  showImgViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/03/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class showImgViewController: UIViewController {
    var sharedText = ""
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var back: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        details.text = sharedText
        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
}
