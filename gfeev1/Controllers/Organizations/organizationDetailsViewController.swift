//
//  organizationDetailsViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class organizationDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var indexVal = 0
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //return Title.count
        return postMsgDescription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hmcell", for: indexPath) as! myorgDetailsTableViewCell
//        cell.org_subTitle.text = subTitle[indexPath.row]
//        cell.orgTtle.text = Title[indexPath.row]
//        cell.org_Description.text = descriptionDetails[indexPath.row]
        cell.org_subTitle.text = postDateArr[indexPath.row]
       // cell.orgTtle.text = postMsgTitle[indexPath.row]
        cell.org_Description.text = postMsgDescription[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 260
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexVal = indexPath.row
      //  performSegue(withIdentifier: "toView", sender: self)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "toView") as! showImgViewController
        vc.sharedText = postMsgDescription[indexVal]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "toView") as! showImgViewController
//        vc.sharedText = postDateArr[indexVal]
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    var orgName = ""
    var orgID = 594
    var orgType = 2
    var Title = ["HM Group","IGPL"]
    var subTitle = ["HM Group Details","IGPL Details"]
    var descriptionDetails = ["HM Constructions is one of Bangalore's largest real estate companies in terms of revenue, earnings, market capitalisation and developable area. It has 27 years track record of sustained growth, customer satisfaction and innovation.","To end up as a prime entertainer in the worldwide commercial center by giving exceedingly imaginative web planning, web improvement and web showcasing administrations that will drive our customers' business towards development. Overall notoriety is the fantasy of each organization, and we need to carry it out through our work."]
    
    var postMsgTitle:[String] = []
    var postMsgDescription:[String] = []
    var postMsgImage:[String]?
    var postIcon:[String] = []
    var postDateArr:[String] = []
   // var group_postTextArray:[String] = []
    var startNumber = 0
    
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var currentOrganisation: UILabel!
    @IBOutlet weak var mainAddress: UILabel!
    @IBOutlet weak var web: UILabel!
    @IBOutlet weak var industry: UILabel!
    @IBOutlet weak var City: UILabel!
    @IBOutlet weak var State: UILabel!
    @IBOutlet weak var Country: UILabel!
    @IBOutlet weak var emailID: UILabel!
    @IBOutlet weak var contactNo: UILabel!
    @IBOutlet weak var OrgDescription: UILabel!
   @IBOutlet weak var connections: UILabel!
   //@IBOutlet weak var fullAddress: UILabel!
    
    @IBOutlet weak var completeAddress: UILabel!
    
    let SuggestedorgDetailsURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/organizationdetails"
    
    let Orgpost_DataURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/orgpostdata"
    
    internal typealias CompletionHandler = ( _ responseObj:orgDetailsModel?, _ errorOBj:NSError?) -> ()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        currentOrganisation.text = orgName
      //  print(orgName,orgID,orgType)
        self.getSuggOrgDetailswithID(accesskey:accesskey!,userid:userid,organizationtype:orgType,organizationid:orgID, callback:
            { (orgdata, error) in
            if let groupdata = orgdata
        {
            print(groupdata.status!,"my group details data")
            print("in01 \(String(describing: groupdata.message))")
            if groupdata.status! == 0
            {
                print("No data..")
            }
            else
            {
               DispatchQueue.main.sync
                {
                    for obj in groupdata.organizationdetails!
                    {
                       self.currentOrganisation.text = obj.orgname!
                       self.OrgDescription.text = obj.desciption!
                       self.industry.text = obj.industryname!
                        self.web.text = obj.orgurl!
                        self.emailID.text = obj.orgemail!
                        self.contactNo.text = String(obj.orgphone!)
                        self.mainAddress.text = obj.address!
                        self.connections.text = String(obj.followers!)
                        // self.completeAddress.text = obj.city! +  "," + obj.state! + "," + obj.country!
                        self.completeAddress.text = "\(obj.city!),\(obj.state!),\(obj.country!)"
                        self.tv.reloadData()
                 }
             }
            }
        }
        })
        
        
//        userid:174
//        accesskey:840bbfc73b7db41b10e666508e08c605
//        orgtype:2
//        start:0
//        ge_id:594
        self.SuggPostOrgDetailswithID(userid:userid,accesskey:accesskey!,orgtype:orgType,start:startNumber,ge_id:orgID,callback:
            {(orgdata, error) in
            if let orgdata = orgdata
        {
            if orgdata.post_status == 0
            {
                print("No data.. ok")
            }
            else
            {
               DispatchQueue.main.sync
                {
                    for obj in orgdata.posts!
                    {
                        print("obj in group post details 11111, \(obj)")
                        print("industryname...1",obj.posttext!)
                        print("obj.postdate!...1",obj.mypostdate!)
                        self.postMsgDescription.append(obj.posttext!)
                        self.postDateArr.append(obj.mypostdate!)
                    }
//                    for obj in groupdata.posts!
//                    {
////                        print("obj in group post details 22222, \(obj)")
////                        print("industryname...2",obj.posttext!)
////                    //    self.postMsgTitle.append(obj.orgname!)
////                        self.postDate.append(obj.postdate!)
////                        self.postMsgDescription.append(obj.posttext!)
////                       // self.postDate.append(obj.postdate!)
////                        print(self.postMsgDescription,"Data checking hereeeeeeeeee..!")
////                        print(self.postDate as Any,"Data checking hereeeeeeeeee")
//
////                       self.currentOrganisation.text = obj.orgname!
////                       self.OrgDescription.text = obj.desciption!
////                       self.industry.text = obj.industryname!
////                        self.web.text = obj.orgurl!
////                        self.emailID.text = obj.orgemail!
////                        self.contactNo.text = String(obj.orgphone!)
////                        self.mainAddress.text = obj.address!
////                        self.connections.text = String(obj.followers!)
////                        // self.completeAddress.text = obj.city! +  "," + obj.state! + "," + obj.country!
////                        self.completeAddress.text = "\(obj.city!),\(obj.state!),\(obj.country!)"
////                        self.tv.reloadData()
//                 }
             }
            }
        }
        })
        
        
    }
  
    func getSuggOrgDetailswithID(accesskey:String!,userid:Int,organizationtype:Int!,organizationid:Int!, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&organizationtype=\(organizationtype!)".data(using: String.Encoding.utf8)!)
        postData.append("&organizationid=\(organizationid!)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: SuggestedorgDetailsURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = orgDetailsModel.orgDetailsStatus(data: jsonResult)
                        callback(localObj,nil)
                        //groupDetailsModel.Groupdetails(data: jsonResult)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }

    func SuggPostOrgDetailswithID(userid:Int,accesskey:String!,orgtype:Int,start:Int,ge_id:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&orgtype=\(orgtype)".data(using: String.Encoding.utf8)!)
        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: Orgpost_DataURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = orgDetailsModel.postStatus(data: jsonResult)
                        callback(localObj,nil)
                        //groupDetailsModel.Groupdetails(data: jsonResult)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func backAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
}

class orgDetailsModel:NSObject
 {
    var organizationdetails:[orgDetailsModel]?
    var orgid:Int?
    var customerid:Int?
    var orgname:String?
    var logo:String?
    var desciption:String?
    var followers:Int?
    var date:String?
    var org_status:Int?
    var orgurl:String?
    var orgphone:Int?
    var orgemail:String?
    var industrytypeid:Int?
    var industryname:String?
    var countryid:Int?
    var country:String?
    var stateid:Int?
    var state:String?
    var cityid:Int?
    var city:String?
    var address:String?
    var pincode:Int?
    //var public:Int?
    var status:Int?
    var message:String?
    var code:Int?
       
//    "id": 77,
//    "posttext": "seven",
//    "posttype": 1,
//    "likes": 0,
//    "date": "2019-12-03 18:53:34",
//    "orgtype_id": 2,
//    "userlogo": "https://innovativegraphics.in/projects/gfee/superadmin/images/organizations/thumb-37X37sUzZW9B77S.jpg",
//    "postdata": "https://innovativegraphics.in/projects/gfee/superadmin/images/posts/schoolposts/post3aGqprXMAx.jpg"
    var posts:[orgDetailsModel]?
    var postid:Int?
    var posttext:String?
    var posttype:Int?
    var likes:Int?
    var mypostdate:String?
    var orgtype_id:Int?
    var userlogo:String?
    var postdata:String?
    var post_status:Int?
    var post_message:String?
    var post_code:Int?
    
   
    class func orgDetailsStatus(data:NSDictionary) -> orgDetailsModel
    {
        let myorgObj = orgDetailsModel()
        myorgObj.status = data["status"] as? Int
        myorgObj.code = data["code"] as? Int
        myorgObj.message = data["message"] as? String
        
        if let myorgDetails = data["organizationdetails"] as? NSArray
        {
            var myorgData:[orgDetailsModel] = []
            for obj in myorgDetails
            {
                let orgdata = orgDetailsModel.orgDetails(data: obj as! NSDictionary)
                myorgData.append(orgdata)
            }
            myorgObj.organizationdetails = myorgData
        }
        return myorgObj
    }
    
    
    class func orgDetails(data:NSDictionary) -> orgDetailsModel
    {
        let myorg = orgDetailsModel()
        myorg.orgid = data["orgid"] as? Int
        myorg.customerid = data["customerid"] as? Int
        myorg.orgname = data["orgname"] as? String
        myorg.logo = data["logo"] as? String
        myorg.desciption = data["desciption"] as? String
        myorg.followers = data["followers"] as? Int
        myorg.date = data["date"] as? String
        myorg.status = data["status"] as? Int
        myorg.orgurl = data["orgurl"] as? String
        myorg.orgphone = data["orgphone"] as? Int
        myorg.orgemail = data["orgemail"] as? String
        myorg.industrytypeid = data["industrytypeid"] as? Int
        myorg.industryname = data["industryname"] as? String
        myorg.countryid = data["countryid"] as? Int
        myorg.country = data["country"] as? String
        myorg.stateid = data["stateid"] as? Int
        myorg.state = data["state"] as? String
        myorg.cityid = data["cityid"] as? Int
        myorg.city = data["city"] as? String
        myorg.address = data["address"] as? String
         myorg.pincode = data["pincode"] as? Int
        return myorg
    }
    
     class func postStatus(data:NSDictionary) -> orgDetailsModel
        {
            let postObj = orgDetailsModel()
            postObj.post_status = data["status"] as? Int
            postObj.post_code = data["code"] as? Int
            postObj.post_message = data["message"] as? String
            if let myorgpost = data["posts"] as? NSArray
            {
                var myorgpostvar :[orgDetailsModel] = []
                for obj in myorgpost
                {
                    let orgpostdata = orgDetailsModel.org_postDetails(data: obj as! NSDictionary)
                    myorgpostvar.append(orgpostdata)
                }
                postObj.posts = myorgpostvar
                
            }
            return postObj
        }
        
        class func org_postDetails(data:NSDictionary) -> orgDetailsModel
        {
            let myorg = orgDetailsModel()
            myorg.postid = data["id"] as? Int
            myorg.posttext = data["posttext"] as? String
            myorg.posttype = data["posttype"] as? Int
            myorg.likes = data["likes"] as? Int
            myorg.mypostdate = data["date"] as? String
            myorg.orgtype_id = data["orgtype_id"] as? Int
            myorg.userlogo = data["date"] as? String
            myorg.postdata = data["postdata"] as? String
            return myorg
        }
}

  
