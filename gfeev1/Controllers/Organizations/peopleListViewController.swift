//
//  peopleListViewController.swift
//  gfeev1
//
//  Created by IGPL on 12/12/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
protocol selectPeople {
    func selectNames(select:String)
}
class peopleListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    
    @IBOutlet weak var tv: UITableView!
    var myTableArray:[String] = []
    let userListURRL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/getpeoplelist"
     var orgType = ["Select organization Type","Company","School","College","Institute","Other"]
  internal typealias CompletionHandler = ( _ responseObject : peopleListModel?,  _ errorObject : NSError?) -> ()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        somefunc(val: "someval")
        somefunc(val: "someval For Guard")
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
      // print(userid,"and",accesskey!)
    
        self.getpeopleListApi(userid:userid,accesskey:accesskey!, callback: { (logdata, error) in
                if let logdata = logdata
                {
                   print(logdata.status!,"mylogdata")
                    print(logdata.code!,"code")
                    print(logdata.message!,"mylogdata")
                  // print("in01 \(String(describing: logdata.message))")
                for obj in logdata.people!
                {
                    print(obj.firstname!,"Different types are:")
                    self.myTableArray.append(obj.firstname!)
                       DispatchQueue.main.sync
                       {
                           self.tv.reloadData()
                       }
                }
                }
               })
        
    }
    
    func getpeopleListApi(userid:Int,accesskey:String,callback: @escaping CompletionHandler)
       {
           let headers = ["Content-Type": "application/x-www-form-urlencoded"]

           let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
           
           postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
           
           let request = NSMutableURLRequest(url: NSURL(string: userListURRL)! as URL,
           cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
           
           request.httpMethod = "POST"
           request.allHTTPHeaderFields = headers
           request.httpBody = postData as Data
           let session = URLSession.shared
           let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
               
               if let error = error
               {
                   callback(nil, error as NSError)
               }
               else
               {
                   do
                   {
                       if let josnResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                       {
                           let localObj = peopleListModel.myuserfunc(data: josnResult)
                        
                           callback(localObj, nil)
                       }
                       else
                       {
                           callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                       }
                   }
                   catch let error as NSError
                   {
                       callback(nil,error)
                   }
               }
           })
           dataTask.resume()
       }
      
    var Delegate:selectPeople?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myTableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "peopleListViewController", for: indexPath)
        cell.textLabel?.text = myTableArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // typeDelegate?.selectedOrg(selected: orgType[indexPath.row])
        Delegate?.selectNames(select: myTableArray[indexPath.row])
       dismiss(animated: true, completion: nil)
    }

    func somefunc(val:String?)
    {
        if let myvar = val
        {
            print("val of myvar1",myvar)
        }
        else
        {
            print("No value")
        }
    }
    // print("val of myvar1",myvar)
    func someotherfunc(val:String?)
    {
        guard let someval = val
        else {
        return
      }
    print("someval of guard",someval)
    }
}

class peopleListModel : NSObject
{
    var people : [peopleListModel]?
    var id:Int?
    var firstname:String?
    var lastname:String?
    var email:String?
    var status:Int?
    var message:String?
    var code:Int?
    
    class func myuserfunc(data:NSDictionary) -> peopleListModel
    {
        let obj = peopleListModel()
        obj.status = data["status"] as? Int
        obj.message = data["message"] as? String
        obj.code = data["code"] as? Int
        if let peopleobj = data["people"] as? NSArray
        {
            var mypeopleObj:[peopleListModel] = []
            for obj in peopleobj
            {
                let localobj = peopleListModel.mypeopleList(data: obj as! NSDictionary)
                mypeopleObj.append(localobj)
            }
            obj.people = mypeopleObj
         }
         return obj
    }
    
    class func mypeopleList(data:NSDictionary) -> peopleListModel
    {
        let mypeopleObj = peopleListModel()
        mypeopleObj.email = data["email"] as? String
        mypeopleObj.firstname = data["firstname"] as? String
        mypeopleObj.lastname = data["lastname"] as? String
        mypeopleObj.id = data["id"] as? Int
        return mypeopleObj
    }
}


//
//"people": [
//    {
//        "id": 171,
//        "firstname": "a",
//        "lastname": "a",
//        "email": "y@gmail.com"
//    },
//    {
//        "id": 157,
//        "firstname": "manu",
//        "lastname": "ac",
//        "email": "manuac@innovativegraphics.in"
//    },
//    {
//        "id": 155,
//        "firstname": "NIKHIL",
//        "lastname": "Gowda",
//        "email": "nikhil@innovativegraphics.in"
//    },
//    {
//        "id": 173,
//        "firstname": "Raghav",
//        "lastname": "agadi",
//        "email": "raghav@gmail.com"
//    },
//    {
//        "id": 172,
//        "firstname": "Raj",
//        "lastname": "A",
//        "email": "raj@gmail.com"
//    },
//    {
//        "id": 175,
//        "firstname": "Ram",
//        "lastname": "A",
//        "email": "rameshragadi@gmail.com"
//    },
//    {
//        "id": 156,
//        "firstname": "sachin",
//        "lastname": "ganiger",
//        "email": "sachinganiger@innovativegraphics.in"
//    }
//],
//"status": 1,
//"message": "Success",
//"code": 200
