//
//  milestoneTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class milestoneTableViewCell: UITableViewCell {

    @IBOutlet weak var milestone: UILabel!
    @IBOutlet weak var milestone_sdate: UILabel!
    @IBOutlet weak var milestone_edate: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
