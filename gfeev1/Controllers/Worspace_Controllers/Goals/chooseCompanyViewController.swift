//
//  chooseCompanyViewController.swift
//  Gfee_workspace
//
//  Created by Ramesh on 6/3/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class chooseCompanyViewController: UIViewController,UIPopoverPresentationControllerDelegate,getCompany {
    func getcompName(comp: String, subcomp: String) {
        selectedCompanyName.setTitle(comp, for: .normal)
       // subCompanyName.text! = subcomp
        subCompanyName_withAction.setTitle(subcomp, for: .normal)
    }
    
    
    
 @IBOutlet weak var subCompanyName: UILabel!
    @IBOutlet weak var subCompanyName_withAction: UIButton!
    @IBOutlet weak var selectedCompanyName: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func close(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func sub_action(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func chooseComp(_ sender: UIButton)
    {
        let popController = storyboard?.instantiateViewController(withIdentifier: "chooseCompanyDropdownVC") as! chooseCompanyDropdownVC
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
    popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.delegate = self 
        popController.preferredContentSize.height = 100
        popController.preferredContentSize.width =  375
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        self.present(popController, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }

}
