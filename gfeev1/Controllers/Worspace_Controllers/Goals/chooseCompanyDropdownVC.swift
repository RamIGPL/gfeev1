//
//  chooseCompanyDropdownVC.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/12/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit
protocol getCompany
{
    func getcompName(comp:String,subcomp:String)
}

class chooseCompanyDropdownVC: UIViewController
{
    var cname:String?
    var sub_cname:String?
    @IBOutlet weak var companyName: UIButton!
    var delegate : getCompany?
     internal typealias CompletionHandler = ( _ responseObj:workspaceaccess_Model?, _ errorOBj:NSError?) -> ()
     let workspaceURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/workspaceaccess"
    var companynameArray:[String] = []
    var companyIdArray:[String] = []
    var subcompanynameArray:[String] = []
    var subcompanyIdArray:[Int] = []
    var depIdArray:[Int] = []
    var mysubcompany:workspaceaccess_Model?
    var subcompanydataObj:[workspaceaccess_Model] = []
    var mydataObj:[workspaceaccess_Model] = []
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        let Userid = 176
//        let Accesskey = "8fefe614ab61645518fed261b25de8a7"
//
        
        self.getapi(userid:currentUserId, accesskey:currentAccesskey, callback: { (groupdata, error) in
            if let groupdata = groupdata
            {
                print(groupdata.status!,"my group details data")
                print("My workspace access = \(String(describing: groupdata.message))")
                if groupdata.status! == 0
                {
                    print("No data..")
                }
                else
                {
                    for obj in groupdata.workspaceaccess!
                    {
                        self.companynameArray.append(obj.companyname!)
                        self.companyIdArray.append(obj.companyid!)
                        print("cid =\(obj.companyid!)")
                      defaults.set(obj.companyid!, forKey: "cid")
                        defaults.set(obj.companyid!, forKey: "ccid")
                        defaults.set(true, forKey: "CID")
                        print("d val is =\(String(describing: defaults.string(forKey: "ccid")!))")
                        //print("my default value is = \(defaults.object(forKey: "cid") as! Int)")
                       // print(UserDefaults.standard.integer(forKey: "cid"))
                        print(obj.companyname!,"Comp Name")
                        print(obj,"Obj data")
                        print(obj.companyid!)
                        
                        DispatchQueue.main.async
                        {
                            self.companyName.setTitle(obj.companyname!, for: .normal)
                            self.cname = obj.companyname!
                        }
                        
                        for obj1 in obj.subcompanies!
                        {
                            print(obj1.subcompanyname!)
                            print(obj1.subcompanyid!)
                            print(obj1.department_id!,"dept IDDDDD")
                            self.sub_cname = obj1.subcompanyname!
                            self.subcompanynameArray.append(obj1.subcompanyname!)
                            self.subcompanyIdArray.append(obj1.subcompanyid!)
                            self.depIdArray.append(obj1.department_id!)
                        }
                       
                    }

                }
            }
        })
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        <#code#>
//    }
    @IBAction func selectcompany(_ sender: UIButton)
    {
        //delegate?.getsubs(sub: )
        //delegate?.getcompName(sub:companyName.title(for: .normal)!)
        if cname == nil
        {
            print("No companies..")
        }
        else
        {
            delegate?.getcompName(comp: cname!, subcomp: sub_cname!)
            dismiss(animated: true, completion: nil)
        }
    }
    func getapi(userid:Int, accesskey:String, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: workspaceURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = workspaceaccess_Model.workspaceaccess_status(data: jsonResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
        
    }
  }

class workspaceaccess_Model:NSObject
{
    var workspaceaccess:[workspaceaccess_Model]?
    var companyname:String?
    var companyid:String?
    var subcompanies:[workspaceaccess_Model]?
    var subcompanyid:Int?
    var subcompanyname:String?
    var department_id:Int?
    
    var status:Int?
    var message:String?
    var code:Int?
    
    class func workspaceaccess_status(data:NSDictionary) -> workspaceaccess_Model
    {
        let goalObj = workspaceaccess_Model()
        goalObj.code = data["code"] as? Int
        goalObj.status = data["status"] as? Int
        goalObj.message = data["message"] as? String
        //var mydataObj:[workspaceaccess_Model] = []
        if let  workspaceaccessData = data["workspaceaccess"] as? NSArray
        {
            var mydataObj:[workspaceaccess_Model] = []
            for obj in workspaceaccessData
            {
                let localdata = workspaceaccess_Model.workspaceaccessdata(data: obj as! NSDictionary)
                mydataObj.append(localdata)
            }
            goalObj.workspaceaccess = mydataObj
        }
//        if let  subcompaniesData = data["subcompanies"] as? NSArray
//        {
//            var mydataObj1:[workspaceaccess_Model] = []
//            for obj in subcompaniesData
//            {
//                let localdata = workspaceaccess_Model.subcompaniesdata(data: obj as! NSDictionary)
//                mydataObj1.append(localdata)
//            }
//            goalObj.subcompanies = mydataObj1
//            //            goalObj.myorganization1 = mydataObj
//        }
        return goalObj
    }
    
    
    class func workspaceaccessdata(data:NSDictionary) -> workspaceaccess_Model
    {
        let myworkspaceObj = workspaceaccess_Model()
        myworkspaceObj.companyname = data["companyname"] as? String
        myworkspaceObj.companyid = data["companyid"] as? String
        if let  subcompaniesData = data["subcompanies"] as? NSArray
        {
            var mydataObj1:[workspaceaccess_Model] = []
            for obj in subcompaniesData
            {
                let localdata = workspaceaccess_Model.subcompaniesdata(data: obj as! NSDictionary)
                mydataObj1.append(localdata)
            }
            myworkspaceObj.subcompanies = mydataObj1
        }
        return myworkspaceObj
    }

    class func subcompaniesdata(data:NSDictionary) -> workspaceaccess_Model
    {
        let myworkspaceObj = workspaceaccess_Model()
        myworkspaceObj.subcompanyid = data["subcompanyid"] as? Int
        myworkspaceObj.subcompanyname = data["subcompanyname"] as? String
        myworkspaceObj.department_id = data["department_id"] as? Int
        return myworkspaceObj
    }
}
