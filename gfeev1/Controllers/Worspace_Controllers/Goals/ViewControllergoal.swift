//
//  ViewController.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/7/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit
var selected_company = 0
//let Userid = 176
//let Accesskey = "9e10d43a4c9c9df913ae3774892d16e1"
//let Companyid = "1_365"
//let subcompanyid = 1
class ViewControllergoal: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource
{
    var indexVal = 0
    
    var images = ["goal","project","milestone","task","meeting","performance"]
    var titles = ["Goal","Project","Milestone","Task","Meeting","Performance"]
    internal typealias CompletionHandler = ( _ responseObj:goalView_Model?, _ errorOBj:NSError?) -> ()
       
//       var mydataObj:[goalView_Model] = []
       var mygoals2:[goalView_Model] = []
//       var selected_goalid:goalView_Model?
//       var actionButton : ActionButton!
       @IBOutlet weak var tv: UITableView!
    //   let viewGoalsURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/viewgoals"
        let privilegeaccessURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/privilegeaccess"
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! workspaceCollectionViewCell
        cell.img.image = UIImage(named: images[indexPath.row])
        cell.heading.text = titles[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("didSelectItemAt Goal")
        if titles[indexPath.row] == "Goal"
        {
            performSegue(withIdentifier: "toGoal", sender: self)
        }
        if titles[indexPath.row] == "Performance"
        {
            performSegue(withIdentifier: "toPerformance", sender: self)
        }
        if titles[indexPath.row] == "Project"
        {
            performSegue(withIdentifier: "toProject", sender: self)
//            let storybboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let viewprojectVC:viewProjectViewController = storybboard.instantiateViewController(withIdentifier: "viewProjectViewController") as! viewProjectViewController
//            //present(viewprojectVC, animated: true, completion: nil)
//            showDetailViewController(viewprojectVC, sender: self)
        }
        if titles[indexPath.row] == "Milestone"
        {
            performSegue(withIdentifier: "toMilestone", sender: self)
        }
        if titles[indexPath.row] == "Task"
        {
            performSegue(withIdentifier: "toTask", sender: self)
        }
        if titles[indexPath.row] == "Meeting"
        {
            performSegue(withIdentifier: "toMeeting", sender: self)
        }
//        let storyBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: nil);
//            let editViewController :editgoalViewVC = storyBoard.instantiateViewController(withIdentifier: "editVC") as! editgoalViewVC
//        editViewController.goal_name = self.mygoals[indexPath.row].goal_name
//        editViewController.goal_alias =   self.mygoals[indexPath.row].goal_alias
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //let Subcompanyid = 1
        //  _ = 0
        print("viewDidLoad()")
        print("defaults value =\(UserDefaults.standard.integer(forKey: "cid"))")
        var count = "\(UserDefaults.standard.integer(forKey: "cid"))"
        let id = defaults.object(forKey: "cid") as? Int
        print("CID== \(defaults.bool(forKey: "CID")))")
        print(defaults.bool(forKey: "CID"))
        if (defaults.bool(forKey: "CID") == false)
        {
            let nav = storyboard?.instantiateViewController(withIdentifier: "pop") as! chooseCompanyViewController
            present(nav, animated: true, completion: nil)
        }
        else
        {
                print("Default Tab bar screen")
        }
        self.getaccessPrevilizes(userid:Userid, accesskey:Accesskey,companyid:Companyid,subcompanyid:subcompanyid, callback: { (groupdata, error) in
        if let groupdata = groupdata
        {
            print(groupdata.privilege_status!,"my group details data")
                        print("in01 \(String(describing: groupdata.message))")
                        if groupdata.privilege_status! == 0
                        {
                            print("No data..")
                        }
                        else
                        {
                            //33591152
                           // defaults.set(groupdata.privileges!, forKey: "all_previlizes")
                            for obj in groupdata.privileges!
                            {
                               // print("whole object start : ",obj, "whole object end")
//                                defaults.set(obj.privilege!, forKey: "privilegeTitle")
//                                defaults.set(obj.acccess!, forKey: "privilegeAccess")
                                //defaults.set(obj, forKey: "privilegeObj")
                              //  defaults.removeObject(forKey: "privilegeObj")
                              //  defaults.removeObject(forKey: "privilegeAccess")
//                                print("privilegeTitle == \(defaults.string(forKey: "privilegeTitle")!)")
//                                print("privilegeAccess == \(defaults.string(forKey: "privilegeAccess")!)")
                                
                                if obj.privilege! == "Create_Goal"
                                {
                                    if obj.acccess! == 1
                                    {
                                        print("You can create the goal buddy")
                                    }
                                }
                                if obj.privilege! == "Create_Milestone"
                                {
                                    if obj.acccess! == 1
                                    {
                                        print("You can create the Milestone buddy")
                                    }
                                    else
                                    {
                                        print("No buddy, You can not Create the Milestone")
                                    }
                                }
                            print(obj.privilege!, "privilege is in VCCC")
                            print(obj.acccess!, "access is")
        //                        DispatchQueue.main.sync
        //                            {
        //                                self.tv.reloadData()
        //                        }
                            }
                            //print(self.goalArray,"Complete goal title..")//
                        }
                        
                    }
                })
    }
    //workspaceCollectionViewCell
    //cell
    func getaccessPrevilizes(userid:Int, accesskey:String,companyid:String,subcompanyid:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
       // postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: privilegeaccessURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                        let localObj = goalView_Model.privilegeaccess_status(data: jsonResult)
                        //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
        
    }
    override func viewDidAppear(_ animated: Bool)
    {
         print("viewDidAppear()")
//        if selected_company == 0
//        {
//            let nav = storyboard?.instantiateViewController(withIdentifier: "pop") as! chooseCompanyDropdownVC
//            present(nav, animated: true, completion: nil)
//
//        }
//        else
//        {
//            print("Default Tab bar screen")
//        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
        print("viewWillAppear()")
//        if selected_company == 0
//        {
//            let nav = storyboard?.instantiateViewController(withIdentifier: "pop") as! chooseCompanyDropdownVC
//            present(nav, animated: true, completion: nil)
//
//        }
//        else
//        {
//            print("Default Tab bar screen")
//        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear()")
    }
    override func viewDidDisappear(_ animated: Bool) {
         print("viewDidDisappear()")
    }
    
    
    @IBAction func back(_ sender: Any)
    {
//        let storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
//        let destVC: homeViewController = storyboard.instantiateViewController(withIdentifier: "toHome") as! homeViewController
//        if let nav = self.navigationController
//        {
//            nav.pushViewController(destVC, animated: true)
//        }
//        else
//        {
//            self.present(destVC, animated: true, completion: nil)
//        }
//
    }
    
}

