//
//  editgoalViewVC.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/19/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit
var goalVC = goalViewController()
class editgoalViewVC: UIViewController,UITextFieldDelegate
{
    var goal_id:Int?
    var goal_name:String?
    var goal_alias:String?
    var goal_startdate:String?
    var goal_enddate:String?
    var goal_description:String?
    internal typealias CompletionHandler = ( _ responseObj:editGoal_Model?, _ errorOBj:NSError?) -> ()
    let editGoalURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/editgoal"
    @IBOutlet weak var goalTitle: UITextField!
    @IBOutlet weak var goalAlias: UITextField!
    @IBOutlet weak var sdate: UITextField!
    @IBOutlet weak var edate: UITextField!
    @IBOutlet weak var goal_desc: UITextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        navigationController?.topbarHeight
//        let navHeight = topbarHeight
//        navHeight.
        goalTitle.text = goal_name
        goalAlias.text = goal_alias
        goal_desc.text = goal_description
        goalTitle.text = goal_name
        sdate.text = goal_startdate
        edate.text = goal_enddate
        sdate.addInputViewDatePicker(target: self, selector: #selector(buttonPressed))
        edate.addInputViewDatePicker(target: self, selector: #selector(buttonPressed2))
    }
    @objc func buttonPressed()
    {
        if let datePick = self.sdate.inputView as? UIDatePicker
        {
            let dateformat = DateFormatter()
            dateformat.dateStyle = .full
            dateformat.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.sdate.text = dateformat.string(from: datePick.date)
        }
        self.sdate.resignFirstResponder()
    }
    
    @objc func buttonPressed2()
    {
        if let datePick = self.edate.inputView as? UIDatePicker
        {
            let dateFormat = DateFormatter()
            dateFormat.dateStyle = .full
            dateFormat.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.edate.text = dateFormat.string(from: datePick.date)
        }
        self.edate.resignFirstResponder()
    }
    
    @IBAction func back(_ sender: Any)
    {
        go_back_to_previous()
        //navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Submit(_ sender: Any)
    {
//        print(goal_name!)
//        print(goalTitle.text!)
//        goalTitle.text = goal_name
//        goalAlias.text = goal_alias
//        goal_desc.text = goal_description
//        goalTitle.text = goal_name

//        let startdate = "2018-09-01 00:00:00"
//        let enddate = "2020-12-31 00:00:00"
        let Subcompanyid = 1
        self.editGoal(userid:currentUserId, accesskey:currentAccesskey,goalname:goalTitle.text!, goalalias:goalAlias.text!, startdate:sdate.text!,enddate:edate.text!,companyid:Companyid,subcompanyid:Subcompanyid,description:goal_desc.text!, goalid:goal_id!, callback: { (groupdata, error) in
            if let groupdata = groupdata
            {
                //NotificationCenter.default.post(name: NSNotification.Name.init("refreshGoal"), object: goalVC)
                NotificationCenter.default.post(name: .refreshGoal, object: nil)
                print(groupdata.status!,"my group details data")
                print(groupdata.message!,"groupdata.message!")
                DispatchQueue.main.async
                {
                    
                   // view.toastViewForMessage(groupdata.message!, title: "Donetoast", image: <#UIImage?#>)
                    //self.showAlert(msg: groupdata.message!)
                    // NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "refresh_forms"), object: MasterController3, userInfo: userinfo)
                    //NotificationCenter.default.post(name: NSNotification.Name.init("refreshGoal"), object: goalVC)
                    self.go_back_to_previous()
                    //self.view.makeToast(groupdata.message!)
                }
                if groupdata.status! == 0
                {
                    print("No data..")
                }
            }
        })
    }
    
    func showAlert(msg:String)
    {
        let alert = UIAlertController.init(title: "Alert..!", message: msg, preferredStyle: .alert)
        let ok  = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        
    }
    func editGoal(userid:Int, accesskey:String,goalname:String,goalalias:String,startdate:String,enddate:String,companyid:String,subcompanyid:Int,description:String,goalid:Int,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&goalname=\(goalname)".data(using: String.Encoding.utf8)!)
        postData.append("&goalalias=\(goalalias)".data(using: String.Encoding.utf8)!)
        postData.append("&startdate=\(startdate)".data(using: String.Encoding.utf8)!)
        postData.append("&enddate=\(enddate)".data(using: String.Encoding.utf8)!)
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
        postData.append("&description=\(description)".data(using: String.Encoding.utf8)!)
        postData.append("&goalid=\(goalid)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: editGoalURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = editGoal_Model.editGoalView_status(data: jsonResult)                        //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
}
class editGoal_Model:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
   // uicollectionview
    class func editGoalView_status(data:NSDictionary) -> editGoal_Model
    {
        let goalObj = editGoal_Model()
        goalObj.code = data["code"] as? Int
        goalObj.status = data["status"] as? Int
        goalObj.message = data["message"] as? String
        return goalObj
    }
}

extension UIViewController {

    var topbarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
        } else {
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
            return topBarHeight
        }
    }
}



