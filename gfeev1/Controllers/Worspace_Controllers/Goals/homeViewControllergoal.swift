//
//  homeViewController.swift
//  Gfee_workspace
//
//  Created by Ramesh on 6/4/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class homeViewControllergoal: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func home(_ sender: UIButton)
    {
        defaults.removeObject(forKey: "CID")
        self.showAlert(msg: "You are Logged Out Successfully..!")
    }
    @IBAction func switchComp(_ sender: UIButton)
    {
        
        let nav = storyboard?.instantiateViewController(withIdentifier: "pop") as! chooseCompanyViewController
        present(nav, animated: true, completion: nil)
    }
    func showAlert(msg:String)
    {
        let alert = UIAlertController.init(title: "Alert..!", message: msg, preferredStyle: .alert)
        let ok  = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        
    }
}
