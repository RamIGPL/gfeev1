//
//  viewGoalDashboardVC.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/20/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit
import WebKit
class viewGoalDashboardVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var goalName:String?
    var goalID:Int?
    var mydashboardObj:[String]?
    @IBOutlet weak var dashboardgoalTitle: UILabel!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "goaldashboardcell", for: indexPath) as! goaldashboardTableViewCell
        cell.img.image = UIImage(named: images[indexPath.row])
        cell.total_countTitle.text = countsTitle[indexPath.row]
        cell.total_count.text = String(counts[indexPath.row])
        //mydashboardObj
            //String(counts[indexPath.row])
        return cell
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    
    let goalDashboardURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/goaldashboard"
    internal typealias CompletionHandler = ( _ responseObj:viewGoal_DashboardModel?, _ errorOBj:NSError?) -> ()
    @IBOutlet weak var wv: WKWebView!
    @IBOutlet weak var tv: UITableView!
    var countsTitle = ["Overall Projects : ","Overall Tasks : ","Overall Meetings : "]
    var counts = [0,1,2]
    var images = ["task36","project36","meeting36"]
    
    @IBOutlet weak var ProjectViewActionBtn: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let goalid = 1
        let subcompanyid = 1
        dashboardgoalTitle.text = goalName
        //wv.load(URLRequest(url: URL(string: "https://www.infocons.ro/ro/trimitefoto/")!))
                wv.load(URLRequest(url: URL(string: "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/goalgraph/176/4c2d19b73b8e7c3c10944b6083f5c11b/1_365/1/2/")!))
         //wv.load(URLRequest(url: URL(string: "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/goalgraph/+Userid+/Accesskey/Companyid/subcompanyid/goalID/")!))
        self.getgoalDashboardAPI(userid:Userid,goalid:goalid, accesskey:Accesskey,companyid:Companyid,subcompanyid:subcompanyid, callback: { (groupdata, error) in
        if let groupdata = groupdata
        {
        print(groupdata.status!,"my group details data")
        print("in01 \(String(describing: groupdata.message))")
        if groupdata.status! == 0
        {
            print("No data..")
        }
        else
        {
//            for (key,val) in groupdata as! [String:Any]
//            {
//                print(val,"val")
//            }
          
//            print(groupdata as! [String:Any],"counts required")
            print(groupdata.projects_count!,"projects_count")
            print(groupdata.meetings_count!,"meetings_count")
            print(groupdata.tasks_count!,"tasks_count")
            print(groupdata.goal_performance!,"goal_performance")
        }
        }
        })
        let action = UITapGestureRecognizer(target: self, action: #selector(self.projectViewActionMethod(_:)))
        self.ProjectViewActionBtn.addGestureRecognizer(action)
    }
    
        
        @objc func projectViewActionMethod(_ sender:UITapGestureRecognizer)
        {
            print("action is performing..")
            let storyBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: nil);
            let goalprojectViewController :goalProjectViewController = storyBoard.instantiateViewController(withIdentifier: "goalProjectViewController") as! goalProjectViewController
            if let nav = self.navigationController
            {
                nav.pushViewController(goalprojectViewController, animated: true)
            }
            else
            {
                self.present(goalprojectViewController, animated: true, completion: nil)
            }
            
         //   let mystoryboard : UIStoryboard = storyboard?.instantiateViewController(identifier: "Main")
//            self.homeView.isHidden = true
//            self.timelineView.isHidden = false
//            print("time line action is happening..")
//             let userid = UserDefaults.standard.integer(forKey: "userid")
//             let accesskey = UserDefaults.standard.string(forKey: "accesskey")
//                    let startVal = 0
//                    self.getOwnGroupAPI(userid:userid,accesskey:accesskey!,start:startVal, callback: { (orgdata, error) in
//                    if let orgdata = orgdata
//                    {
//                        print(orgdata.status!,"my orgdata responsee..")
//                      //  print(orgdata.myorganization1!,"my orgdata details ..")
//                        print("in01 \(String(describing: orgdata.message))")
//                        if orgdata.status! == 1
//                        {
//                            for obj in orgdata.timeline!
//                            {
//                            print(obj.ge_organizationname!, obj.ge_date!, obj.ge_posttext!,"data guru..!")
//                                self.timeLine_Groupname.append(obj.ge_organizationname!)
//                                self.timeLine_date.append(obj.ge_date!)
//                                self.timeLine_Groupname.append(obj.ge_organizationname!)
//
//    //                          DispatchQueue.main.sync
//    //                            {
//    //                               //wwww self.timelineTV.reloadData()
//    //                            }
//                            }
//
//                        }
//                        else
//                        {
//                            print("No data")
//                        }
//                    }
//                })
        }
    
    
    func getgoalDashboardAPI(userid:Int,goalid:Int, accesskey:String,companyid:String,subcompanyid:Int,callback: @escaping CompletionHandler)
        
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&goalid=\(goalid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: goalDashboardURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? [String:Any]
                    {
                        let localObj = viewGoal_DashboardModel.GoaldashboardView(data: jsonResult as [String:Any])
                        callback(localObj, nil)
                    }
                    else
                    {
                        
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func back(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
 }

class viewGoal_DashboardModel:NSObject
{
    var projects_count:Int?
    var tasks_count:Int?
    var meetings_count:Int?
    var goal_performance:Int?
    var code:Int?
    var status:Int?
    var message:String?

    class func GoaldashboardView(data:[String:Any]) -> viewGoal_DashboardModel
    {
        let goalObj = viewGoal_DashboardModel()
        goalObj.projects_count = data["projects_count"] as? Int
        goalObj.tasks_count = data["tasks_count"] as? Int
        goalObj.meetings_count = data["meetings_count"] as? Int
        goalObj.goal_performance = data["goal_performance"] as? Int
        goalObj.code = data["code"] as? Int
        goalObj.status = data["status"] as? Int
        goalObj.message = data["message"] as? String
        return goalObj
    }
}

