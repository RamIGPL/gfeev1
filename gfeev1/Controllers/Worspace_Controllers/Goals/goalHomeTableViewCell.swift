//
//  goalHomeTableViewCell.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/11/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class goalHomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var goalIcon: UIImageView!
    @IBOutlet weak var goalTitle: UILabel!
    @IBOutlet weak var goalSdate: UILabel!
    @IBOutlet weak var goalEdate: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}

//import UIKit
//let Userid = 176
//let Accesskey = "8fefe614ab61645518fed261b25de8a7"
//let Companyid = "1_365"
//class goalViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate
//{
//    internal typealias CompletionHandler = (  responseObj:goalView_Model?,  errorOBj:NSError?) -> ()
//    var mydataObj:[goalView_Model] = []
//    var mygoals:[goalView_Model] = []
//    var actionButton : ActionButton!
//    @IBOutlet weak var tv: UITableView!
//    let viewGoalsURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/viewgoals"
//    let privilegeaccessURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/privilegeaccess"
//    var goal_Title = ["goal","project","milestone","task","meeting","performance"]
//    var goal_sDate = ["12-12-20","1-2-20","20-2-20","12-2-20","3-3-20","5-5-20"]
//    var goal_eDate = ["12-12-20","1-2-20","20-2-20","12-2-20","3-3-20","5-5-20"]
//    var goal_icons = ["Goal","Project","Milestone","Task","Meeting","Performance"]
//    var goal_sdateArray:[String] = []
//    var goal_edateArray:[String] = []
//    var goalArray:[String] = []
//    var goal_descArray:[String] = []
//    var goal_aleasArray:[String] = []
//    var goalIDArray:[Int] = []
//    //    var group_postimgArray:[String] = []
//    //    var group_postUserIcon:[String] = []
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        //return goalArray.count
//        
//        return self.mygoals.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "goalcell", for: indexPath) as! goalHomeTableViewCell
//        //        cell.goalTitle.text = goalArray[indexPath.row]
//        //        cell.goalSdate.text = goal_sdateArray[indexPath.row]
//        //        cell.goalEdate.text = goal_edateArray[indexPath.row]
//        
//        cell.goalTitle.text = mygoals[indexPath.row].goal_name
//        cell.goalSdate.text = mygoals[indexPath.row].goal_startdate
//        cell.goalEdate.text = mygoals[indexPath.row].goal_enddate
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 100
//    }
//    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete{
//            self.delete_Course(model_obj:self.mygoals[indexPath.row])
//        }
//        
//    }
//    func delete_Course(model_obj:goalView_Model)
//    {
//        
//        
//        
//        if let my_goal_id = model_obj.goal_id{
//            deleteGoal(userid: Userid, accesskey: Accesskey, goalid: my_goal_id, companyid: Companyid, subcompanyid: subcompanyid) { (deletedataStatus, error) in
//                if  let deletedataStatus = deletedataStatus
//                {
//                    print(deletedataStatus.delete_message!,"status of delete action")
//                    DispatchQueue.main.async {
//                        self.tv.reloadData()
//                    }
//                }
//            }
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
//    {
//        let delete = UITableViewRowAction(style: .destructive, title: "Delete")
//        { (action, indexPath) in
//            let alert = UIAlertController(title: "Alert", message: "You want to Delete?", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "OK", style: .default, handler:
//            {
//                UIAlertAction in
//                //self.delete_Course()
//            })
//            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//            alert.addAction(ok)
//            
//            alert.addAction(cancel)
//            self.present(alert, animated: true, completion: nil)
//            //}
//        }
//        
//        let edit = UITableViewRowAction(style: .normal, title: "Edit")
//        { (action, indexPath) in
//            // self.selected_courseobj = self.useridObj[indexPath.row]
//            
//            //            if self.selected_courseobj?.cid != nil
//            //            {
//            let storyBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: nil);
//            let editViewController :editgoalViewVC = storyBoard.instantiateViewController(withIdentifier: "editVC") as! editgoalViewVC
//            editViewController.goal_name = self.goalArray[indexPath.row]
//            editViewController.goal_alias =   self.goal_aleasArray[indexPath.row]
//            editViewController.goal_description = self.goal_descArray[indexPath.row]
//            editViewController.goal_startdate =   self.goal_sdateArray[indexPath.row]
//            editViewController.goal_enddate = self.goal_edateArray[indexPath.row]
//            editViewController.goal_id = self.goalIDArray[indexPath.row]
//            //editViewController.goalAlias =   self.goalArray[indexPath.row]
//            //objSecondryViewController.editId = self.selected_courseobj?.cid
//            self.showDetailViewController(editViewController, sender: self)
//            // }
//        }
//        // edit.backgroundColor = UIColor.blue
//        return [delete, edit]
//        //        return delete
//        //    }
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        performSegue(withIdentifier: "togoalDashboard", sender: self)
//    }
//    override func viewDidLoad()
//    {
//        super.viewDidLoad()
//        setupButtons()
//        let Subcompanyid = 1
//        let Start = 0
//        self.getapi(userid:Userid, accesskey:Accesskey,companyid:Companyid,subcompanyid:Subcompanyid,start:Start, callback: { (groupdata, error) in
//            if let groupdata = groupdata
//            {
//                print(groupdata.status!,"my group details data")
//                if groupdata.status! == 0
//                {
//                    print("No data..")
//                }
//                else
//                {
//                    
//                    if let goals = groupdata.goals{
//                        self.mygoals = goals
//                        DispatchQueue.main.sync
//                            {
//                                self.tv.reloadData()
//                        }
//                    }
//                    
//                    for obj in groupdata.goals!
//                    {
//                        self.goalArray.append(obj.goal_name!)
//                        self.goal_sdateArray.append(obj.goal_startdate!)
//                        self.goal_edateArray.append(obj.goal_enddate!)
//                        self.goal_descArray.append(obj.goal_description!)
//                        self.goal_aleasArray.append(obj.goal_alias!)
//                        self.goalIDArray.append(obj.goal_id!)
//                        print(obj.goal_name!, "Available Groupname")
//                        DispatchQueue.main.sync
//                            {
//                                self.tv.reloadData()
//                        }
//                    }
//                    print(self.goalArray,"Complete goal title..")
//                }
//                
//            }
//        })
//        
//        self.getaccessPrevilizes(userid:Userid, accesskey:Accesskey,companyid:Companyid,subcompanyid:Subcompanyid, callback: { (groupdata, error) in
//            if let groupdata = groupdata
//            {
//                print(groupdata.privilege_status!,"my group details data")
//                print("in01 \(String(describing: groupdata.message))")
//                if groupdata.privilege_status! == 0
//                {
//                    print("No data..")
//                }
//                else
//                {
//                    for obj in groupdata.privileges!
//                    {
//                        //
//                        print(obj.privilege!, "privilege is")
//                        print(obj.acccess!, "access is")
//                        //                        DispatchQueue.main.sync
//                        //                            {
//                        //                                self.tv.reloadData()
//                        //                        }
//                    }
//                    print(self.goalArray,"Complete goal title..")
//                }
//                
//            }
//        })
//        
//    }
//    func setupButtons()
//    {
//        let google = ActionButtonItem(title: "Insert Goal", image: #imageLiteral(resourceName: "goal"))
//        google.action = { item in self.performSegue(withIdentifier: "tocreate", sender: self) }
//        //        let twitter = ActionButtonItem(title: "Twitter", image: #imageLiteral(resourceName: "filter36"))
//        //        twitter.action = { item in self.view.backgroundColor = UIColor.blue }
//        //        let facebook = ActionButtonItem(title: "Facebook", image: #imageLiteral(resourceName: "survey36"))
//        //        let linkedin = ActionButtonItem(title: "Linkedin", image: #imageLiteral(resourceName: "home green36"))
//        actionButton = ActionButton(attachedToView: self.view, items: [google])
//        actionButton.setTitle("+", forState: UIControl.State())
//        actionButton.backgroundColor = UIColor(red: 138.0/255.0, green: 230.0/255.0, blue: 238.0/255.0, alpha: 1)
//        actionButton.action = { button in button.toggleMenu()}
//    }
//    
//    @IBAction func popBtn(_ sender: UIButton)
//    {
//        let popcontrol = self.storyboard?.instantiateViewController(withIdentifier: "filtervc") as! filterViewController
//        //        popcontrol.dep = true
//        //        popcontrol.somestring = rows2[indexPath.row]
//        //        print("rows2[indexPath.row] = \(rows2[indexPath.row])")
//        popcontrol.modalPresentationStyle = UIModalPresentationStyle.popover
//        popcontrol.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
//        popcontrol.preferredContentSize.height = 200
//        popcontrol.preferredContentSize.width = 200
//        popcontrol.popoverPresentationController?.delegate = self
//        popcontrol.popoverPresentationController?.sourceView = sender
//        popcontrol.popoverPresentationController?.sourceRect =  sender.bounds
//        self.present(popcontrol, animated: true, completion: nil)
//    }
//    
//    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
//    {
//        return UIModalPresentationStyle.none
//    }
//    
//    func getapi(userid:Int, accesskey:String,companyid:String,subcompanyid:Int,start:Int,callback: @escaping CompletionHandler)
//    {
//        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
//        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
//        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
//        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
//        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
//        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
//        let request = NSMutableURLRequest(url: NSURL(string: viewGoalsURL)! as URL,
//                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
//        
//        request.httpMethod = "POST"
//        request.allHTTPHeaderFields = headers
//        request.httpBody = postData as Data
//        
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            
//            if let error = error
//            {
//                callback(nil, error as NSError)
//            }
//            else
//            {
//                do
//                {
//                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
//                    {
//                        //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
//                        let localObj = goalView_Model.goalView_status(data: jsonResult)
//                        //orgTypModel.orgTypefirstLayer(data: josnResult)
//                        callback(localObj, nil)
//                    }
//                    else
//                    {
//                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
//                    }
//                }
//                catch let error as NSError
//                {
//                    callback(nil,error)
//                }
//            }
//        })
//        dataTask.resume()
//        
//        //    }
//    }
//    func getaccessPrevilizes(userid:Int, accesskey:String,companyid:String,subcompanyid:Int, callback: @escaping CompletionHandler)
//    {
//        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
//        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
//        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
//        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
//        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
//        // postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
//        let request = NSMutableURLRequest(url: NSURL(string: privilegeaccessURL)! as URL,
//                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
//        
//        request.httpMethod = "POST"
//        request.allHTTPHeaderFields = headers
//        request.httpBody = postData as Data
//        
//        let session = URLSession.shared
//        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//            
//            if let error = error
//            {
//                callback(nil, error as NSError)
//            }
//            else
//            {
//                do
//                {
//                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
//                    {
//                        //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
//                        let localObj = goalView_Model.privilegeaccess_status(data: jsonResult)
//                        //orgTypModel.orgTypefirstLayer(data: josnResult)
//                        callback(localObj, nil)
//                    }
//                    else
//                    {
//                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
//                    }
//                }
//                catch let error as NSError
//                {
//                    callback(nil,error)
//                }
//            }
//        })
//        dataTask.resume()
//        
//    }
//    
//    
//}
//
//
//
//class goalView_Model:NSObject
//{
//    var goals:[goalView_Model]?
//    var goal_id:Int?
//    var project_id:Int?
//    var milestone_id:Int?
//    var goal_name:String?
//    var goal_alias:String?
//    var goal_description:String?
//    var goal_startdate:String?
//    var goal_enddate:String?
//    var goal_createdby:Int?
//    var projects_count:Int?
//    var tasks_count:Int?
//    var meetings_count:Int?
//    var goal_performance:Int?
//    
//    var status:Int?
//    var message:String?
//    var code:Int?
//    
//    var privileges:[goalView_Model]?
//    var privilege:String?
//    var acccess:Int?
//    var privilege_status:Int?
//    var privilege_message:String?
//    var privilege_code:Int?
//    
//    
//    class func goalView_status(data:NSDictionary) -> goalView_Model
//    {
//        let goalObj = goalView_Model()
//        goalObj.code = data["code"] as? Int
//        goalObj.status = data["status"] as? Int
//        goalObj.message = data["message"] as? String
//        
//        if let  viewGoalData = data["goals"] as? NSArray
//        {
//            var mydataObj:[goalView_Model] = []
//            for obj in viewGoalData
//            {
//                let localdata = goalView_Model.viewgoaldata(data: obj as! NSDictionary)
//                mydataObj.append(localdata)
//            }
//            goalObj.goals = mydataObj
//            //goalObj.myorganization1 = mydataObj
//        }
//        return goalObj
//    }
//    
//    
//    class func viewgoaldata(data:NSDictionary) -> goalView_Model
//    {
//        let mygoalObj = goalView_Model()
//        mygoalObj.goal_id = data["goal_id"] as? Int
//        mygoalObj.project_id = data["project_id"] as? Int
//        mygoalObj.milestone_id = data["milestone_id"] as? Int
//        mygoalObj.goal_name = data["goal_name"] as? String
//        mygoalObj.goal_alias = data["goal_alias"] as? String
//        mygoalObj.goal_description = data["goal_description"] as? String
//        mygoalObj.goal_startdate = data["goal_startdate"] as? String
//        mygoalObj.goal_enddate = data["goal_enddate"] as? String
//        mygoalObj.goal_createdby = data["goal_createdby"] as? Int
//        mygoalObj.projects_count = data["projects_count"] as? Int
//        mygoalObj.tasks_count = data["tasks_count"] as? Int
//        mygoalObj.meetings_count = data["meetings_count"] as? Int
//        mygoalObj.goal_performance = data["goal_performance"] as? Int
//        return mygoalObj
//    }
//    class func privilegeaccess_status(data:NSDictionary) -> goalView_Model
//    {
//        let goalObj = goalView_Model()
//        goalObj.privilege_status = data["code"] as? Int
//        goalObj.privilege_status = data["status"] as? Int
//        goalObj.privilege_message = data["message"] as? String
//        
//        if let  privilegeaccessData = data["privileges"] as? NSArray
//        {
//            var mydataObj:[goalView_Model] = []
//            for obj in privilegeaccessData
//            {
//                let localdata = goalView_Model.privilegeaccessdata(data: obj as! NSDictionary)
//                mydataObj.append(localdata)
//            }
//            goalObj.privileges = mydataObj
//            //goalObj.myorganization1 = mydataObj
//        }
//        return goalObj
//    }
//    
//    
//    class func privilegeaccessdata(data:NSDictionary) -> goalView_Model
//    {
//        let mypriviligesObj = goalView_Model()
//        mypriviligesObj.privilege = data["privilege"] as? String
//        mypriviligesObj.acccess = data["acccess"] as? Int
//        return mypriviligesObj
//    }
//    
//}
//
///*
// class privilegeaccess_Model:NSObject
// {
// 
// 
// 
// var privileges:[privilegeaccess_Model]?
// var privilege:String?
// var acccess:Int?
// var status:Int?
// var message:String?
// var code:Int?
// 
// 
// 
// class func privilegeaccess_status(data:NSDictionary) -> privilegeaccess_Model
// {
// let goalObj = privilegeaccess_Model()
// goalObj.code = data["code"] as? Int
// goalObj.status = data["status"] as? Int
// goalObj.message = data["message"] as? String
// 
// if let  privilegeaccessData = data["privileges"] as? NSArray
// {
// var mydataObj:[privilegeaccess_Model] = []
// for obj in privilegeaccessData
// {
// let localdata = privilegeaccess_Model.privilegeaccessdata(data: obj as! NSDictionary)
// mydataObj.append(localdata)
// }
// goalObj.privileges = mydataObj
// //goalObj.myorganization1 = mydataObj
// }
// return goalObj
// }
// 
// 
// class func privilegeaccessdata(data:NSDictionary) -> privilegeaccess_Model
// {
// let mypriviligesObj = privilegeaccess_Model()
// mypriviligesObj.privilege = data["privilege"] as? String
// mypriviligesObj.acccess = data["acccess"] as? Int
// return mypriviligesObj
// }
// 
// 
// }
// */
