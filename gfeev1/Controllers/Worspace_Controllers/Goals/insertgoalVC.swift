//
//  insertgoalVC.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/19/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class insertgoalVC: UIViewController,UITextFieldDelegate
{
    internal typealias CompletionHandler = ( _ responseObj:insertGoal_Model?, _ errorOBj:NSError?) -> ()
    var createGoalURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/creategoal"
    
    
    @IBOutlet weak var goalName: UITextField!
    @IBOutlet weak var goalAlias: UITextField!
    @IBOutlet weak var goalsdate: UITextField!
    @IBOutlet weak var goaledate: UITextField!
//    @IBOutlet weak var goalsdate: UIButton!
//    @IBOutlet weak var goaledate: UIButton!
    @IBOutlet weak var goalDescr: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        goalsdate.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed))
        goaledate.addInputViewDatePicker(target: self, selector: #selector(end_doneButtonPressed))
//        var userid:176
//        var accesskey:d84669f4024119d1cb4d9e82f1e51535
//        let goalname = goalName.text!
//        let goalalias = goalAlias.text!
//        let startdate = "2018-09-01 00:00:00"
//        let enddate = "2020-12-31 00:00:00"
//       // let companyid = "1_365"
//        let Subcompanyid = 1
//        let description = goalDescr.text!
//        let email = "rameshragadi@innovativegraphics.in"
        
//        self.insertGoal(userid:Userid, accesskey:Accesskey,goalname:goalname,goalalias:goalalias,startdate:startdate,enddate:enddate,companyid:Companyid,subcompanyid:Subcompanyid,description:description,email:email, callback: { (groupdata, error) in
//            if let groupdata = groupdata
//            {
//                print(groupdata.status!,"my group details data")
//                //print(groupdata.privilege_status!,"privilege_status details data")
//
//                print("in01 \(String(describing: groupdata.message))")
//                if groupdata.status! == 0
//                {
//                    print("No data..")
//                }
//            }
//        })
    }
    
    @IBAction func back(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doneButtonPressed()
    {
        if let  datePicker = self.goalsdate.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.goalsdate.text = dateFormatter.string(from: datePicker.date)
        }
        self.goalsdate.resignFirstResponder()
    }
    
    @objc func end_doneButtonPressed()
    {
        if let  datePicker = self.goaledate.inputView as? UIDatePicker
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.goaledate.text = dateFormatter.string(from: datePicker.date)
        }
        self.goaledate.resignFirstResponder()
    }
    
    
    
//    userid:Userid, accesskey:Accesskey,goalname:goalname,goalalias:goalalias,startdate:startdate,enddate:enddate,companyid:Companyid,subcompanyid:Subcompanyid,description:description,email:email,
    func insertGoal(userid:Int, accesskey:String,goalname:String,goalalias:String,startdate:String,enddate:String,companyid:String,subcompanyid:Int,description:String,email:String,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&goalname=\(goalname)".data(using: String.Encoding.utf8)!)
        postData.append("&goalalias=\(goalalias)".data(using: String.Encoding.utf8)!)
        postData.append("&startdate=\(startdate)".data(using: String.Encoding.utf8)!)
        postData.append("&enddate=\(enddate)".data(using: String.Encoding.utf8)!)
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
        postData.append("&description=\(description)".data(using: String.Encoding.utf8)!)
        postData.append("&email=\(email)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: createGoalURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                        let localObj = insertGoal_Model.insertGoalView_status(data: jsonResult)
                        //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func insertGoaldetails(_ sender: UIButton)
    {
        let goalname = goalName.text!
        let goalalias = goalAlias.text!
        let startdate = goalsdate.text!
        let enddate = goaledate.text!
        // let companyid = "1_365"
        let Subcompanyid = 1
        let description = goalDescr.text!
        let email = "rameshragadi@innovativegraphics.in"
        
        self.insertGoal(userid:currentUserId, accesskey:currentAccesskey,goalname:goalname,goalalias:goalalias,startdate:startdate,enddate:enddate,companyid:Companyid,subcompanyid:Subcompanyid,description:description,email:email, callback: { (groupdata, error) in
            if let groupdata = groupdata
            {
                print(groupdata.status!,"my group details data")
                print("Done inserting Goal")
                print("Alert here : \(String(describing: groupdata.message))")
//                 NotificationCenter.default.post(name: .refreshGoal, object: nil)
             
                DispatchQueue.main.async {
                    //self.tv.reloadData()
                   
                    NotificationCenter.default.post(name: .refreshGoal, object: nil)
                    self.go_back_to_previous()
                    self.showAlert(msg: groupdata.message!)
                }
//                if groupdata.status! == 1
//                {
//                    let alert = UIAlertController(title: "Alert..!", message: "msg", preferredStyle: .alert)
//                    let ok  = UIAlertAction(title: "OK", style: .default, handler: nil)
//                    alert.addAction(ok)
//                    self.present(alert, animated: false)
//                }
//
//                if groupdata.status! == 0
//                {
//                    print("No data..")
//
//                }
            }
        })
    }
    func showAlert(msg:String)
    {
        let alert = UIAlertController.init(title: "Alert..!", message: msg, preferredStyle: .alert)
        let ok  = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        
    }
}

class insertGoal_Model:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    
    class func insertGoalView_status(data:NSDictionary) -> insertGoal_Model
    {
        let goalObj = insertGoal_Model()
        goalObj.code = data["code"] as? Int
        goalObj.status = data["status"] as? Int
        goalObj.message = data["message"] as? String
        return goalObj
    }
}
