//
//  workspaceCollectionViewCell.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/8/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class workspaceCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet weak var backgroundBorder: UIView!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var heading: UILabel!
    
}
