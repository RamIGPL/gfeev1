//
//  filterViewController.swift
//  Gfee_workspace
//
//  Created by Ramesh on 5/18/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class filterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var filterArray = ["Points High to Low","Points Low to High","By Date Low to  High","By Date High to Low"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = filterArray[indexPath.row]
        return cell
    }
    

    @IBOutlet weak var tv: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    


}
