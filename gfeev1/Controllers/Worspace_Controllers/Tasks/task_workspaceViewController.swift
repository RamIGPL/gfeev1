//
//  task_workspaceViewController.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class task_workspaceViewController: UIViewController {
    var goal_Title = ["goal","project","milestone","task","meeting","performance"]
    
   let TaskTitle = ["Task Title 1","Task Title 2","Task Title 3"]
     let descriptionText = ["Task Title 1 Task Title 1 Task Title 1 Task Title 1 Task Title 1 ","Task Title 2Task Title 2Task Title 2","Task Title 3Task Title 3Task Title 3Task Title 3Task Title 3"]
      let start = ["Task Title 1","Task Title 2","Task Title 3"]
      let end = ["Task Title 1","Task Title 2","Task Title 3"]
      let remaining = [0,4,2]
      let estimated = [8,6,9]
      let modes = ["Safe Mode","Warning Mode","Danger Mode"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func back(_ sender : UIButton)
    {
        navigationController?.popViewController(animated: true)
    }


}

extension task_workspaceViewController :UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //return goalArray.count
        return self.goal_Title.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //myindex = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskcell", for: indexPath) as! task_workspaceTableViewCell
        cell.TaskTitle.text = goal_Title[indexPath.row]
            //goal_Title[indexPath.row].goal_name //
        //cell.goalSdate.text = mygoals[indexPath.row].goal_startdate//goal_sdateArray[indexPath.row]
        //cell.goalEdate.text = mygoals[indexPath.row].goal_enddate //goal_edateArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
