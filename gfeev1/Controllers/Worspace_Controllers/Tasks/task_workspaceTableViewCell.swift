//
//  task_workspaceTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class task_workspaceTableViewCell: UITableViewCell {

    
     @IBOutlet weak var TaskTitle:UILabel!
    @IBOutlet weak var TaskDescription:UILabel!
    @IBOutlet weak var task_sdate :UILabel!
    @IBOutlet weak var task_edate:UILabel!
    @IBOutlet weak var task_remainingTime:UILabel!
    @IBOutlet weak var task_estimatedTime:UILabel!
    @IBOutlet weak var profileIcon:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
