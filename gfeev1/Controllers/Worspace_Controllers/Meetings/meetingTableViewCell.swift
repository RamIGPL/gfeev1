//
//  meetingTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class meetingTableViewCell: UITableViewCell {

    @IBOutlet weak var meetingTitle:UILabel!
    @IBOutlet weak var meetingDescr:UILabel!
    @IBOutlet weak var meetingDate:UILabel!
    @IBOutlet weak var meetingDuration:UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
