//
//  viewProjectViewController.swift
//  Gfee_workspace
//
//  Created by IGPL on 10/06/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class viewProjectViewController: UIViewController
{
    var goal_Title = ["goal","project","milestone","task","meeting","performance"]
    var goal_sDate = ["12-12-20","1-2-20","20-2-20","12-2-20","3-3-20","5-5-20"]
    var goal_eDate = ["12-12-20","1-2-20","20-2-20","12-2-20","3-3-20","5-5-20"]
    var
    myprojects:[projectView_Model] = []
    internal typealias CompletionHandler = ( _ responseObj:projectView_Model?, _ errorOBj:NSError?) -> ()
    var viewProjectURl = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/viewprojects"
    var deleteURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/deleteproject"
    @IBOutlet weak var tv: UITableView!
   
    @IBAction func addProjectAction(_ sender: Any)
    {
      
        let storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
               let destVC: project_InsertViewController = storyboard.instantiateViewController(withIdentifier: "insertProject") as! project_InsertViewController
               if let nav = self.navigationController
               {
                   nav.pushViewController(destVC, animated: true)
               }
               else
               {
                       self.present(destVC, animated: true, completion: nil)
               }
    }
    
//        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//            return true
//        }
//        
//        func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//            if editingStyle == .delete{
//                self.delete_Course(model_obj: mygoals[indexPath.row])
//            }
//            
//        }
//        func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
//        {
//            let delete = UITableViewRowAction(style: .destructive, title: "Delete")
//            { (action, indexPath) in
//                let alert = UIAlertController(title: "Alert", message: "You want to Delete?", preferredStyle: .alert)
//                let ok = UIAlertAction(title: "OK", style: .default, handler:
//                {
//                    UIAlertAction in
//                    self.delete_Course(model_obj: self.mygoals[indexPath.row])
//                })
//                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                alert.addAction(ok)
//                alert.addAction(cancel)
//                self.present(alert, animated: true, completion: nil)
//                //}
//            }
//
//            let edit = UITableViewRowAction(style: .normal, title: "Edit")
//            { (action, indexPath) in
//               // self.selected_courseobj = self.useridObj[indexPath.row]
//
//    //            if self.selected_courseobj?.cid != nil
//    //            {
//                    let storyBoard : UIStoryboard = UIStoryboard (name: "Main", bundle: nil);
//                    let editViewController :editgoalViewVC = storyBoard.instantiateViewController(withIdentifier: "editVC") as! editgoalViewVC
//                editViewController.goal_name = self.mygoals[indexPath.row].goal_name
////                editViewController.goal_alias =   self.mygoals[indexPath.row].goal_alias
////                editViewController.goal_description = self.mygoals[indexPath.row].goal_description
////                editViewController.goal_startdate =   self.mygoals[indexPath.row].goal_startdate
////                editViewController.goal_enddate =      self.mygoals[indexPath.row].goal_enddate
////                editViewController.goal_id =        self.mygoals[indexPath.row].goal_id
//
//                if let nav = self.navigationController
//                {
//                    nav.pushViewController(editViewController, animated: true)
//                }
//                else
//                {
//                    self.present(editViewController, animated: true, completion: nil)
//                }
//    //
//    //            if let nav = self.navigationController
//    //            {
//    //                nav.pushViewController(editViewController, animated: true)
//    //            }
//    //            else
//    //
//    //            {
//    //                self.present(editViewController, animated: true, completion: nil)
//    //            }
//
//              //  self.showDetailViewController(editViewController, sender: self)
//               // }
//            }
//           // edit.backgroundColor = UIColor.blue
//            return [delete, edit]
//    //        return delete
//    //    }
//        }
    @IBAction func back(_ sender: Any)
    {
        let storyboard:UIStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
        let destVC: ViewControllergoal = storyboard.instantiateViewController(withIdentifier: "toDashboard") as! ViewControllergoal
        if let nav = self.navigationController
        {
            nav.pushViewController(destVC, animated: true)
        }
        else
        {
                self.present(destVC, animated: true, completion: nil)
        }
    }
    
 //   Setters and Getters
//    Design patterns
//
//
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(projectRefreshList(notification:)), name: .refreshproject, object: nil)
        
        self.getapi(userid:currentUserId, accesskey:currentAccesskey,companyid:Companyid,subcompanyid:subcompanyid,start:Start,goalid:Goalid,callback: { (groupdata, error) in
        if let groupdata = groupdata
        {
            print(groupdata.status!,"my group details data")
            if groupdata.status! == 0
            {
                print("No data in Goal vc..")
            }
            else
            {
                if let projects = groupdata.projects
                {
                    self.myprojects = projects
                    for i in self.myprojects
                    {
                       print("Project data  \(i.project_name!)")
                    }
                    DispatchQueue.main.sync
                    {
                        self.tv.reloadData()
                    }
                }
                for obj in groupdata.projects!
                {
                    print(obj.goal_name!, "Available Groupname")
                    DispatchQueue.main.sync
                    {
                        //self.tv.reloadData()
                    }
                }
            }
//
        }
    })
}

    @objc func projectRefreshList(notification:NSNotification)
    {
                self.getapi(userid:currentUserId, accesskey:currentAccesskey,companyid:Companyid,subcompanyid:subcompanyid,start:Start,goalid:Goalid,callback: { (groupdata, error) in
                if let groupdata = groupdata
                {
                    print(groupdata.status!,"my group details data")
                    if groupdata.status! == 0
                    {
                        print("No data in Goal vc..")
                    }
                    else
                    {
                        if let projects = groupdata.projects
                        {
                            self.myprojects = projects
                            for i in self.myprojects
                            {
                               print("Project data  \(i.project_name!)")
                            }
                            DispatchQueue.main.sync
                            {
                                self.tv.reloadData()
                            }
                        }
                        for obj in groupdata.projects!
                        {
                            print(obj.goal_name!, "Available Groupname")
                            DispatchQueue.main.sync
                            {
                                //self.tv.reloadData()
                            }
                        }
                    }
        //
                }
            })
            
    }
func getapi(userid:Int, accesskey:String,companyid:String,subcompanyid:Int,start:Int,goalid:Int,callback: @escaping CompletionHandler)
{
            let headers = ["Content-Type": "application/x-www-form-urlencoded"]
            let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
            postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
            postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
            postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
            postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
            postData.append("&goalid=\(goalid)".data(using: String.Encoding.utf8)!)
            print(postData,"postData array")
            let request = NSMutableURLRequest(url: NSURL(string: viewProjectURl)! as URL, cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                if let error = error
                {
                    callback(nil, error as NSError)
                }
                else
                {
                    do
                    {
                        if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                        {
                            //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                            let localObj = projectView_Model.projectView_status(data: jsonResult)
                             
                            callback(localObj, nil)
                        }
                        else
                        {
                           
                        }
                    }
                    catch let error as NSError
                    {
                        callback(nil,error)
                    }
                }
            })
            dataTask.resume()
    }

}


class projectView_Model:NSObject
{
    var projects:[projectView_Model]?
    var goal_id:Int?
    var project_id:Int?
    var milestone_id:Int?
    var goal_name:String?
    var project_name:String?
    var project_alias:String?
    var project_description:String?
    var project_startdate:String?
    var project_enddate:String?
    var project_createdby:Int?
    var milestones_count:Int?
    var tasks_count:Int?
    var meetings_count:Int?
    var goal_performance:Int?

    var status:Int?
    var message:String?
    var code:Int?

class func projectView_status(data:NSDictionary) -> projectView_Model
{
    let projectObj = projectView_Model()
    projectObj.code = data["code"] as? Int
    projectObj.status = data["status"] as? Int
    projectObj.message = data["message"] as? String

    if let  viewProjectData = data["projects"] as? NSArray
    {
        var mydataObj:[projectView_Model] = []
        for obj in viewProjectData
        {
            let localdata = projectView_Model.viewprojectdata(data: obj as! NSDictionary)
                //goalView_Model.viewgoaldata(data: obj as! NSDictionary)
            mydataObj.append(localdata)
        }
        projectObj.projects = mydataObj
    }
    return projectObj
}
  

class func viewprojectdata(data:NSDictionary) -> projectView_Model
{
    let myproObj = projectView_Model()
    myproObj.goal_id = data["goal_id"] as? Int
    myproObj.project_id = data["project_id"] as? Int
    myproObj.milestone_id = data["milestone_id"] as? Int
    myproObj.goal_name = data["goal_name"] as? String
    myproObj.project_name = data["project_name"] as? String
    myproObj.project_alias = data["project_alias"] as? String
    myproObj.project_description = data["project_description"] as? String
    myproObj.project_startdate = data["project_startdate"] as? String
    myproObj.project_enddate = data["project_enddate"] as? String
    myproObj.project_createdby = data["project_createdby"] as? Int
    myproObj.milestones_count = data["milestones_count"] as? Int
    myproObj.tasks_count = data["tasks_count"] as? Int
    myproObj.meetings_count = data["meetings_count"] as? Int
    myproObj.goal_performance = data["goal_performance"] as? Int
    return myproObj
}
   

}


extension viewProjectViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
       {
          // return self.goal_Title.count
           return self.myprojects.count
       }
           
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
       {
           let cell = tableView.dequeueReusableCell(withIdentifier: "projectcell", for: indexPath) as! projectviewTableViewCell
           cell.projectTitle.text = myprojects[indexPath.row].project_name //goal_Title[indexPath.row] //goalArray[indexPath.row]
           cell.projectSdate.text = myprojects[indexPath.row].project_startdate
           cell.projectEdateTitle.text = myprojects[indexPath.row].project_enddate
           return cell
       }
           
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
       {
           return 100
       }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            self.delete_selectedProject(modelObj: myprojects[indexPath.row])
        }
    }
    func delete_selectedProject(modelObj:projectView_Model)
    {
        let projectID = modelObj.project_id
        if let myprojectID = modelObj.project_id
        {
            deleteProject(userid: currentUserId,projectid:myprojectID, accesskey: currentAccesskey,companyid: Companyid,subcompanyid: subcompanyid)
            { (deletedataStatus, error) in
                if  let deletedataStatus = deletedataStatus
                {
                   // print(deletedataStatus.delete_message!,"status of delete action")
                    DispatchQueue.main.async {
                        self.tv.reloadData()
                    NotificationCenter.default.post(name: .refreshGoal, object: nil)
                    //self.showAlert(msg: deletedataStatus.delete_message!)
                    }
                }
            }
        }
        else
        {
            print ("No data or Response..   ")
        }

    }
    
    func deleteProject(userid:Int,projectid:Int, accesskey:String,companyid:String,subcompanyid:Int,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
       // postData.append("&goalid=\(goalid)".data(using: String.Encoding.utf8)!)
       
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
       
        let request = NSMutableURLRequest(url: NSURL(string: deleteURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        //let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                      //  let localObj = goalView_Model.deletestatus(data: jsonResult)
                        //callback(localObj, nil)
                    }
                    else
                    {
                        // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
}
