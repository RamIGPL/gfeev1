//
//  editProjectViewController.swift
//  Gfee_workspace
//
//  Created by IGPL on 10/06/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class editProjectViewController: UIViewController
{

    internal typealias proDetailsHandler = ( _ responseObj:projectDetailsModel?, _ errObj:NSError?) -> ()
//    https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/getprojectdetails
    let projectDetailsURL = "\(mainURL)getprojectdetails"
    @IBOutlet weak var projectTitle: UITextField!
    @IBOutlet weak var projectAlias: UITextField!
    @IBOutlet weak var projectDescr: UITextField!
    @IBOutlet weak var projectstartDate: UITextField!
    @IBOutlet weak var projectendDate: UITextField!
    let proID = 0
  //  @IBOutlet weak var projectTitle: UITextField!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        getprojectDetails(userid: currentUserId, projectid: proID, accesskey: currentAccesskey, companyid: Companyid, subcompanyid: subcompanyid, callback: {(projectData,error) in
            if let projectData = projectData{
                if projectData.status == 0
                {
                    print("No data")
                }
                else
                {
                    print(projectData.status, "project details are ")
                }
            }
            
        })
        
    }
    
    //        userid:176
    //        projectid:10
    //        accesskey:4984d85eca5e48d13862cdd44d05cb4e
    //        companyid:1_365
    //        subcompanyid:1
    func getprojectDetails(userid:Int, projectid:Int,accesskey:String, companyid:String, subcompanyid:Int,callback:@escaping proDetailsHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&projectid=\(projectid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: projectDetailsURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                       
                        let localObj = projectDetailsModel.getProjecDetails_status(data: jsonResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                       // callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()

    }
    @IBAction func updateAction( _ sender: UIButton)
    {
        let proTitle = projectTitle.text!
    }
 }




class projectDetailsModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    var project:[projectDetailsModel]?
    var goal_id:Int?
    var dept_id:Int?
    var project_id:Int?
    var milestone_id:Int?
    var project_name:String?
    var project_alias:String?
    var project_description:String?
    var project_startdate:String?
    var project_enddate:String?
    
    class func getProjecDetails_status(data:NSDictionary) -> projectDetailsModel
    {
        let projObj = projectDetailsModel()
            projObj.code = data["code"] as? Int
            projObj.status = data["status"] as? Int
            projObj.message = data["message"] as? String
            if let getProdetails = data["project"] as? NSArray
            {
                var myproObj:[projectDetailsModel] = []
                for obj in getProdetails {
                    let localData = projectDetailsModel.projectData_details(data: obj as! NSDictionary)
                    myproObj.append(localData)
                }
                
            }
        return projObj
    }
    
    
    class func projectData_details(data:NSDictionary) -> projectDetailsModel
    {
        let myObj = projectDetailsModel()
        myObj.goal_id = data["goal_id"] as? Int
        myObj.dept_id = data["dept_id"] as? Int
        myObj.project_id = data["project_id"] as? Int
        myObj.milestone_id = data["milestone_id"] as? Int
        myObj.project_name = data["project_name"] as? String
        myObj.project_alias = data["project_alias"] as? String
        myObj.project_description = data["project_description"] as? String
        myObj.project_startdate = data["project_startdate"] as? String
        myObj.project_enddate = data["project_enddate"] as? String
        return myObj
     }
    
}
