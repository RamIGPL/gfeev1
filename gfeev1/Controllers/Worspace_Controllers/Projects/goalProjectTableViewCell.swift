//
//  goalProjectTableViewCell.swift
//  Gfee_workspace
//
//  Created by IGPL on 18/06/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class goalProjectTableViewCell: UITableViewCell
{
    @IBOutlet weak var projectTitle: UILabel!
    @IBOutlet weak var sdate: UILabel!
    @IBOutlet weak var edate: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
