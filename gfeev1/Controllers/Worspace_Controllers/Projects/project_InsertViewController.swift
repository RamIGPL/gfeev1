
//
//  project_InsertViewController.swift
//  Gfee_workspace
//
//  Created by Ramesh on 6/1/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

class project_InsertViewController: UIViewController,UITextFieldDelegate
{
    internal typealias projectHandler = ( _ respObj:createProject_Model?, _ errorObj:NSError?) -> ()
//    https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/createproject
    let createProjectURL = "\(mainURL)createproject"
    @IBOutlet weak var projectTItle: UITextField!
    @IBOutlet weak var projectAlias: UITextField!
    @IBOutlet weak var projectDescription: UITextField!
    @IBOutlet weak var datepickerTextfield: UITextField!
    @IBOutlet weak var end_datepickerTextfield: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        datepickerTextfield.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressed))
        end_datepickerTextfield.addInputViewDatePicker(target: self, selector: #selector(end_doneButtonPressed))
    }
    
    @objc func doneButtonPressed()
    {
        if let  datePicker = self.datepickerTextfield.inputView as? UIDatePicker
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.datepickerTextfield.text = dateFormatter.string(from: datePicker.date)
        }
        self.datepickerTextfield.resignFirstResponder()
    }
    
    @objc func end_doneButtonPressed()
    {
        if let  datePicker = self.end_datepickerTextfield.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.end_datepickerTextfield.text = dateFormatter.string(from: datePicker.date)
        }
        self.end_datepickerTextfield.resignFirstResponder()
    }
    @IBAction func back(_ sender:UIButton)
    {
        go_back_to_previous()
    }
    @IBAction func insert(_ sender:UIButton)
    {
        let dept = 1
        let projectname = projectTItle.text!
        let projectalias = projectAlias.text!
        let projectstartdate = datepickerTextfield.text!
        let projectenddate = end_datepickerTextfield.text!
        // let companyid = "1_365"
        //let Subcompanyid = 1
        let description = projectDescription.text!
        let email = "rameshragadi@innovativegraphics.in"
        createProject(userid: currentUserId, goalid: Goalid, projectname: projectname, projectalias: projectalias, startdate: projectstartdate, enddate: projectenddate, department: dept, description: description, accesskey: currentAccesskey, companyid: Companyid, subcompanyid: subcompanyid, email: email, callback:
            { (projectdata, error) in
            if let projectdata = projectdata
                {
                    print(projectdata.message!,"project status message insert")
                    
                    DispatchQueue.main.async
                        {
                            self.view.makeToast("created successfully..!")
                            NotificationCenter.default.post(name: .refreshproject, object: nil)
                            self.go_back_to_previous()
                            //self.showAlert(msg: groupdata.message!)
                        }
                }
        })
    }

    func createProject(userid:Int,goalid:Int,projectname:String,projectalias:String,startdate:String,enddate:String,department:Int,description:String,accesskey:String,companyid:String,subcompanyid:Int,email:String,callback:@escaping projectHandler)
    {
        let projectHeader = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let postingData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postingData.append("&goalid=\(goalid)".data(using: String.Encoding.utf8)!)
        postingData.append("&projectname=\(projectname)".data(using: String.Encoding.utf8)!)
        postingData.append("&projectalias=\(projectalias)".data(using: String.Encoding.utf8)!)
        postingData.append("&startdate=\(startdate)".data(using: String.Encoding.utf8)!)
        postingData.append("&enddate=\(enddate)".data(using: String.Encoding.utf8)!)
        postingData.append("&department=\(department)".data(using: String.Encoding.utf8)!)
        postingData.append("&description=\(description)".data(using: String.Encoding.utf8)!)
        postingData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postingData.append("&companyid=\(companyid)".data(using: String.Encoding.utf8)!)
        postingData.append("&subcompanyid=\(subcompanyid)".data(using: String.Encoding.utf8)!)
        postingData.append("&email=\(email)".data(using: String.Encoding.utf8)!)
        print(postingData,"postingData")
        print(createProjectURL,"mainURL+project")
        let req = NSMutableURLRequest(url: NSURL(string:createProjectURL)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        req.httpMethod = "POST"
        req.allHTTPHeaderFields = projectHeader
        req.httpBody = postingData as Data
        let session = URLSession.shared
        let dataTask = session.dataTask(with: req as URLRequest,completionHandler: { (data, response, error) -> Void in
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else{
                do{
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    {
                        let localdata = createProject_Model.createProject_status(data: jsonResult)
                        callback(localdata,nil)
                    }
                    else
                    {
                        
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
                
            }
        })
        dataTask.resume()
    }
}

class createProject_Model:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    class func createProject_status(data:NSDictionary) -> createProject_Model
    {
        let projectObj = createProject_Model()
        projectObj.status = data["status"] as? Int
        projectObj.code = data["code"] as? Int
        projectObj.message = data["message"] as? String
        return projectObj
    }
}


























extension UITextField
{
    func addInputViewDatePicker(target: Any, selector: Selector)
    {
        let screenWidth = UIScreen.main.bounds.width
        print("screenWidth",screenWidth)
        // let screenWidthless = UIScreen.main.bounds.width - 400
        // print("screenWidth less",screenWidthless)
        //Add DatePicker as inputView
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 260))
        datePicker.datePickerMode = .dateAndTime
        self.inputView = datePicker
        
        //        Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)
        
        self.inputAccessoryView = toolBar
    }
    
    @objc func cancelPressed()
    {
        self.resignFirstResponder()
    }
}
