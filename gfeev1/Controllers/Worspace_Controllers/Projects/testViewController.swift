//
//  testViewController.swift
//  Gfee_workspace
//
//  Created by IGPL on 12/06/20.
//  Copyright © 2020 com.shreeRam. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class testViewController: UIViewController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle
    {
        return .darkContent
    }

}
