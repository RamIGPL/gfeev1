//
//  surveyTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class surveyTableViewCell: UITableViewCell
{
    @IBOutlet weak var surveyTitle:UILabel!
    @IBOutlet weak var surveyCreated:UILabel!
    @IBOutlet weak var surveyClosed:UILabel!
    @IBOutlet weak var surveyPoints:UILabel!
    @IBOutlet weak var img:UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
