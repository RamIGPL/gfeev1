//
//  surveyViewController.swift
//  gfeev1
//
//  Created by IGPL on 30/06/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class surveyViewController: UIViewController {
 var goal_Title = ["goal","project","milestone","task","meeting","performance"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}

extension surveyViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //return goalArray.count
        return self.goal_Title.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //myindex = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "surveycell", for: indexPath) as! surveyTableViewCell
        cell.surveyTitle.text = goal_Title[indexPath.row]
            //goal_Title[indexPath.row].goal_name //
        //cell.goalSdate.text = mygoals[indexPath.row].goal_startdate//goal_sdateArray[indexPath.row]
        //cell.goalEdate.text = mygoals[indexPath.row].goal_enddate //goal_edateArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
