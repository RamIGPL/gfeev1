//
//  menuViewController.swift
//  gfeev1
//
//  Created by IGPL on 08/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
//import Sidemenu

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var menuProfileView: UIView!
    @IBOutlet weak var tv: UITableView!
    var myProfilePic:String = ""
    
    var menuArr = ["Home","Organizations","Groups","Scan","Change Password","Logout"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellid", for: indexPath) as! menuTableViewCell
        cell.menuItem.text = menuArr[indexPath.row]
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if menuArr[indexPath.row] == "Home"
        {
            dismiss(animated: true, completion: nil)
        }
        if menuArr[indexPath.row] == "Organizations"
        {
            performSegue(withIdentifier: "toOrg", sender: self)
        }
        
        if menuArr[indexPath.row] == "Scan"
        {
            performSegue(withIdentifier: "toScan", sender: self)
        }
        if menuArr[indexPath.row] == "Change Password"
        {
            performSegue(withIdentifier: "toChange", sender: self)
        }
        if menuArr[indexPath.row] == "Groups"
        {
            performSegue(withIdentifier: "toGroup", sender: self)
        }
        if menuArr[indexPath.row] == "Logout"
        {
//            let defaults = NSUserDefaults.standardUserDefaults()
//            defaults.removeObjectForKey("myKey")
//            defaults.synchronize()
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "accesskey")
            UserDefaults.standard.removeObject(forKey: "userid")
             defaults.removeObject(forKey: "CID")

            navigateToHome()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func viewDidLoad()
    {
        requestImage(fromURL: UserDefaults.standard.string(forKey: "logo")!)
        userName.text = UserDefaults.standard.string(forKey: "name")
        print("image of mine....",UserDefaults.standard.string(forKey: "logo")!)
       // let myLogo = UserDefaults.standard.string(forKey: "logo")
     //profilePic.image = UIImage(named: "\(UserDefaults.standard.string(forKey: "logo")!)")
        super.viewDidLoad()
        groupFrom = "from_MenuGroup"
        tv.tableFooterView = UIView()
        //Adding Action to UIView()
         let customGesture = UITapGestureRecognizer(target: self, action: #selector(self.addAction(_:)))
        self.menuProfileView.addGestureRecognizer(customGesture)
   }
    func requestImage(fromURL: String)
    {
          if let url = URL(string: fromURL)
          {
              profilePic.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
          }
      }
    
    @objc func addAction(_ sender: UITapGestureRecognizer)
    {
        performSegue(withIdentifier: "toProfile", sender: self)
    }
    func navigateToHome()
     {
         DispatchQueue.main.async
         {
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginViewController
            // let navigationController = UINavigationController(rootViewController: vc)
            // self.present(navigationController, animated: true, completion: nil)
            // self.show(navigationController, sender: self)
            self.navigationController?.pushViewController(vc, animated: true)
         }
        
     }

}
