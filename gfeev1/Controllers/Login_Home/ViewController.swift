//
//  ViewController.swift
//  gfeev1
//
//  Created by IGPL on 04/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import Speech
var mydefaults = UserDefaults.standard
var myAccesskey:String?

class

ViewController: UIViewController,AVSpeechSynthesizerDelegate
{
    internal typealias CompletionHandler1 = ( _ responseObj:Timeline_Model?, _ errorOBj:NSError?) -> ()
    
    @IBOutlet weak var timelineTV: UITableView!
    @IBOutlet weak var TV: UITableView!
    
    @IBOutlet weak var mycollectionView: UICollectionView!
    
    @IBOutlet weak var homeView: UIView!
    @IBOutlet weak var timelineView: UIView!
    @IBOutlet weak var timelineAction_view: UIView!
    var timeLine_Groupname:[String] = []
    var timeLine_date:[String] = []
    var timeLine_postText:[String] = []
    var timeLine_GroupLogo:[String] = []
//    tasks array variable
    var task_nameArray:[String] = []
    var task_descriptionArray:[String] = []
    var startdateArray:[String] = []
    var enddateArray:[String] = []
    var remaininghoursArray:[String] = []
    var estimatedhrsArray:[String] = []
    
    var m_meetingnameArray:[String] = []
    var m_descriptionArray:[String] = []
    var m_startdateArray:[String] = []
    var m_duration:[String] = []
//    var remaininghoursArray:[String] = []
//    var estimatedhrsArray:[Int] = []
    
    
    let timelineURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/timeline"
    let speechSynthesizer = AVSpeechSynthesizer()
    let TaskTitle = ["Task Title 1","Task Title 2","Task Title 3"]
    let descriptionText = ["Task Title 1 Task Title 1 Task Title 1 Task Title 1 Task Title 1 ","Task Title 2Task Title 2Task Title 2","Task Title 3Task Title 3Task Title 3Task Title 3Task Title 3"]
     let start = ["Task Title 1","Task Title 2","Task Title 3"]
     let end = ["Task Title 1","Task Title 2","Task Title 3"]
     let remaining = [0,4,2]
     let estimated = [8,6,9]
     let modes = ["Safe Mode","Warning Mode","Danger Mode"]
    //let localGetAccessURL = "\(baseURL)getaccesskey"
    let localGetAccessURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/getaccesskey"
    let dashboardURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/dashboard"
    internal typealias CompletionHandler = ( _ responseObject : AnyObject?,  _ errorObject : NSError?) -> ()

    var loginobj:[accessModel] = []
    var taskobj:[Timeline_Model] = []
    let subtitle = ["subtitle1subtitle1subtitle1subtitle1subtitle1subtitle1","subtitle2subtitle2subtitle2subtitle2subtitle2subtitle2subtitle2subtitle2"]
    
    let location = ["No.14/92, HM Geneva House, Cunningham Rd, SRT Road, Vasanth Nagar, Bengaluru, Karnataka 560052","Vasanth Nagar, Bengaluru, Karnataka 560052"]
    @IBOutlet weak var searchBar: UISearchBar!
   
    override func viewDidLoad()
    {
        //print("ACCESSSSSSSS KEY ISSSSSSSS=",accesskey!)
        
        homeView.isHidden = false
        timelineView.isHidden = true
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        print("Current User Name",UserDefaults.standard.string(forKey: "name")!)
        print("Current User Secret Key", UserDefaults.standard.string(forKey: "secretkey")!)
        print("Current User Id",UserDefaults.standard.integer(forKey: "userid"))
        let currentSecretKey = UserDefaults.standard.string(forKey: "secretkey") ?? "No user"
        currentUserId = UserDefaults.standard.integer(forKey: "userid")
        let currentUserNAME = UserDefaults.standard.string(forKey: "name")
      //  configureSearchBar() ios pushnotification using by fcm
        let speechUtterance = AVSpeechUtterance(string: "Welcome Mister \(currentUserNAME!)")
        
        speechSynthesizer.speak(speechUtterance)
        self.getAccesskey(secretkey:currentSecretKey, userid:currentUserId, callback:
        { (logdata, error) in
        self.loginobj = logdata as! [accessModel]
        if error == nil
        {
            if self.loginobj.count > 0
            {
                for i in self.loginobj
                {
                    DispatchQueue.main.async
                    {
                        print("viewDidLoad 111 the access key is = \(i.key!)")
                        UserDefaults.standard.set(i.key, forKey: "accesskey")
                        myAccesskey = "\(i.key!)"
                        currentAccesskey = UserDefaults.standard.string(forKey: "accesskey")!
                            //defaults.string(forKey: "accesskey")
                        self.dashboardData(userid: currentUserId, accesskey: i.key!)
                        let abc = UITapGestureRecognizer(target: self, action: #selector(self.timeLineAction (_:)))
                        self.timelineAction_view.addGestureRecognizer(abc)
                        self.view.makeToast("You Logged In \(currentUserNAME!)")
                    }
                          
                }
               
            }
        }
        else
        {
           DispatchQueue.main.async
            {
                    self.view.makeToast((error?.localizedDescription)!)
            }
        }
        })
        //cefcbf10f75fadbe05ca4843c5bef96a -Not Working
        //aa531973ae15188dcce66a760a657ba1 -Working
        
        
        //aa531973ae15188dcce66a760a657ba1 -Not Working
        //18dfbb55931b8233c602701ec194e033 -Working
      //  let gestureSwift2AndHigher = UITapGestureRecognizer(target: self, action:  #selector (timeLineAction(_:)))
        let abc = UITapGestureRecognizer(target: self, action: #selector(self.timeLineAction (_:)))
        self.timelineAction_view.addGestureRecognizer(abc)
        
        
        
//        let userid = UserDefaults.standard.integer(forKey: "userid")
//        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
       
        
     }
    
    func dashboardData(userid:Int,accesskey:String!)
    {
        
        if accesskey != nil
        {
            print("22222the userid  is = \(userid)")
            print("22222the access key is = \(accesskey!)")
        self.dashboardTaskAndMeeting(userid:userid,accesskey:accesskey!, callback:
            { (taskdata, error) in
            if let taskdata = taskdata
            {
                print("taskMeeting_status===",taskdata.taskMeeting_status!)
                if (taskdata.taskMeeting_status == 1)
                {
                    for obj in taskdata.tasks!
                    {
                        print("meeting name is1",obj.firstname!)
                        self.task_nameArray.append(obj.task_name!)
                        self.task_descriptionArray.append(obj.task_description!)
                        self.startdateArray.append(obj.startdate!)
                        self.enddateArray.append(obj.enddate!)
                        self.remaininghoursArray.append(obj.remaininghours!)
                        self.estimatedhrsArray.append(obj.estimatedhrs!)
                        DispatchQueue.main.async
                        {
                            self.TV.reloadData()
                            let abc = UITapGestureRecognizer(target: self, action: #selector(self.timeLineAction (_:)))
                            self.timelineAction_view.addGestureRecognizer(abc)
                        }
                    }
                    for obj in taskdata.meetings!
                    {
                        print("Meeeting Title goes here...",obj.m_meetingname!)
                        self.m_meetingnameArray.append(obj.m_meetingname!)
                        self.m_descriptionArray.append(obj.m_description!)
                        self.m_startdateArray.append(obj.m_startdate!)
                        self.m_duration.append(obj.m_duration!)
                        DispatchQueue.main.async
                        {
                            self.mycollectionView.reloadData()
                        }
                    }
                }
            }
            else
            {
               DispatchQueue.main.async
                {
                   self.view.makeToast((error?.localizedDescription)!)
                }
            }
            })

        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear userid  is = \(userid)")
        print("viewDidAppear access key is = \(accesskey!)")
    }
    @objc func timeLineAction(_ sender:UITapGestureRecognizer)
    {
        self.homeView.isHidden = true
        self.timelineView.isHidden = false
        print("time line action is happening..")
        // let userid = UserDefaults.standard.integer(forKey: "userid")
         //       let accesskey = UserDefaults.standard.string(forKey: "accesskey")
                let startVal = 0
        
    if myAccesskey != ""
    {
        self.getOwnGroupAPI(userid:currentUserId,accesskey:myAccesskey!,start:startVal, callback: { (orgdata, error) in
             if let orgdata = orgdata
             {
                  print(orgdata.status!,"my orgdata responsee..")
                          //  print(orgdata.myorganization1!,"my orgdata details ..")
                            print("in01 \(String(describing: orgdata.message))")
                            if orgdata.status! == 1
                            {
                                for obj in orgdata.timeline!
                                {
                                    print(obj.ge_organizationname!, obj.ge_date!, obj.ge_posttext!,"data guru..!")
                                    self.timeLine_Groupname.append(obj.ge_organizationname!)
                                    self.timeLine_date.append(obj.ge_date!)
                                    self.timeLine_Groupname.append(obj.ge_organizationname!)
                                }
                            }
                            else
                            {
                                print("No data")
                            }
                        }
                    })
    }

    }
    
    
    func getOwnGroupAPI(userid:Int,accesskey:String,start:Int,callback: @escaping CompletionHandler1)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: timelineURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = Timeline_Model.timeline_status(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    

    
    override func viewWillAppear(_ animated: Bool)
    {
//        navigationController?.isNavigationBarHidden = true
        //timelineTV.reloadData()
        TV.reloadData()
    }
    func getAccesskey(secretkey:String,userid:Int,callback:@escaping CompletionHandler)
    {
        let headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data:"userid=\(userid)".data(using:String.Encoding.utf8)!)
        postData.append("&secretkey=\(secretkey)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: localGetAccessURL)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localArray:[accessModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) -> Void in
        if response?.description == nil
        {
           DispatchQueue.main.async
           {
               self.view.makeToast("No Response from Server")
           }
         }
            
        else if(error != nil)
        {
            callback(nil, (error! as NSError))
        }
        else
        {
            do
            {
               localArray.removeAll()
               let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
               let accessObj = json["accesskey"] as? NSDictionary
               let accessStatus = json["status"] as! Int
               print("\(json["message"] as! String)")
               if accessStatus == 0
               {
                   DispatchQueue.main.async
                   {
                       self.view.makeToast((json["message"] as! String))
                    }
               }
               else
               {
                    let objdata = accessObj as! [String:Any]
                    let localData = accessModel().fetchAccesskey(data: objdata)
                    localArray.append(localData)
                    callback(localArray as AnyObject, nil)
               }
            }
            catch
            {
              print("Error handle")
            }
         }
         })
         dataTask.resume()
     }
   
    func dashboardTaskAndMeeting(userid:Int, accesskey:String,callback:@escaping CompletionHandler1)
    {
        print("dashboardTaskAndMeeting actual userid  is = \(userid)")
        print("dashboardTaskAndMeeting actual access key is = \(accesskey)")
        let headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data:"userid=\(userid)".data(using:String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: dashboardURL)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
       // var localArray:[Timeline_Model] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) -> Void in
        if response?.description == nil
        {
            DispatchQueue.main.async {
            self.view.makeToast("No Response from Server")
            }
            
        }
        else if let error = error
        {
            callback(nil, error as NSError)
        }
        else
        {
            do
            {
              if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
              {
                let localData = Timeline_Model.taskAndmeeting_status(data: json)
                //localArray.append(localData)
                callback(localData, nil)
              }
              else
              {
                callback(nil,myerror.responseError(reason: "JSON Error") as NSError)
              }
            }
            catch let error as NSError
            {
              print("Error handle")
                callback(nil,error)
            }
         }
         })
         dataTask.resume()
     }
    
    @IBAction func createOrgAction(_ sender: Any)
    {
       // performSegue(withIdentifier: "createOrg", sender: self)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "createOrg") as! createOrgViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func createGroupAction(_ sender: Any)
    {
         performSegue(withIdentifier: "createGroup", sender: self)
    }

    func configureSearchBar()
    {
        for textField in searchBar.subviews.first!.subviews where textField is UITextField {
            textField.backgroundColor = .white
        }
    }
}

extension ViewController : UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource
{
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return task_nameArray.count
    }
//    var task_nameArray:[String] = []
//       var task_descriptionArray:[String] = []
//       var startdateArray:[String] = []
//       var enddateArray:[String] = []
//       var remaininghoursArray:[String] = []
//       var estimatedhrsArray:[String] = []
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let mycell = UITableViewCell()
        if tableView == TV
        {
            let mycell = tableView.dequeueReusableCell(withIdentifier: "taskcell", for: indexPath) as! taskTableViewCell
            mycell.title.text = task_nameArray[indexPath.row]
            mycell.descriptionTask.text = task_descriptionArray[indexPath.row]
            mycell.sDate.text = startdateArray[indexPath.row]
            mycell.eDate.text = enddateArray[indexPath.row]
            mycell.remainingTime.text = remaininghoursArray[indexPath.row]
            mycell.estimatedTime.text = estimatedhrsArray[indexPath.row]
//            mycell.title.text = TaskTitle[indexPath.row]
//            mycell.descriptionTask.text = descriptionText[indexPath.row]
//            mycell.sDate.text = start[indexPath.row]
//            mycell.eDate.text = end[indexPath.row]
//            mycell.remainingTime.text = String(remaining[indexPath.row])
//            mycell.estimatedTime.text = String(estimated[indexPath.row])
            return mycell
        }
        if tableView == timelineTV
        {
            let mycell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! timelineTableViewCell
            mycell.timelineGroupName.text = TaskTitle[indexPath.row]
            mycell.timelineGroupDate.text = descriptionText[indexPath.row]
            mycell.timelineGroupPostText.text = start[indexPath.row]
                
            
            return mycell
        }
         return mycell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == timelineTV
        {
            return 100
        }
       if tableView == TV
        {
                 return 100
        }
        return 100
    }
//    var m_meetingnameArray:[String] = []
//      var m_descriptionArray:[String] = []
//      var m_startdateArray:[String] = []
//      var m_duration:[String] = []
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return m_meetingnameArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "meetingid", for: indexPath) as! meetingDashboardCollectionViewCell
    //    cell.locationDetails.text = location[indexPath.row]
        cell.meetingTitle.text = m_meetingnameArray[indexPath.row]
        cell.meetingDescription.text = m_descriptionArray[indexPath.row]
        cell.meetingDate.text = m_startdateArray[indexPath.row]
        cell.meetingTime.text = m_duration[indexPath.row]
        return cell
    }
    
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 140
      }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
//Testing git
}

class Timeline_Model:NSObject
{
   var timeline:[Timeline_Model]?
   var ge_id:Int?
   var ge_orgid:Int?
   var ge_organizationname:String?
   var ge_organizationlogo:String?
   var ge_type:Int?
   var ge_posttext:String?
   var ge_postdata:String?
    var ge_posttype:Int?
    var ge_date:String?
             
   var status:Int?
   var message:String?
   var code:Int?
   
    var tasks:[Timeline_Model]?
    var companyid:Int?
    var subcompanyid:Int?
    var goal_id:Int?
    var project_id:Int?
    var milestone_id:Int?
    var delivery_status:Int?
    var task_id:Int?
    var task_name:String?
    var task_description:String?
    var startdate:String?
    var enddate:String?
    var estimatedhrs:String?
    var remaininghours:String?
    var mode:String?
    var task_status:Int?
    var createdby:Int?
    var firstname:String?
    var lastname:String?
    var logo:String?
    
    var meetings:[Timeline_Model]?
    var m_companyid:Int?
    var m_subcompanyid:Int?
    var m_meeting_id:Int?
    var m_goal_id:Int?
    var m_goal_name:Int?
    var m_project_id:Int?
    var m_project_name:Int?
    var m_meetingname:String?
    var m_description:String?
    var m_startdate:String?
    var m_duration:String?
    var m_createdby:Int?
    var m_meetingstatus:Int?
 
    
    var taskMeeting_status:Int?
    var taskMeetingmessage:String?
    var taskMeetingcode:Int?
    
           
    class func timeline_status(data:NSDictionary) -> Timeline_Model
    {
        let fGroupObj = Timeline_Model()
        fGroupObj.code = data["code"] as? Int
        fGroupObj.status = data["status"] as? Int
        fGroupObj.message = data["message"] as? String
        
        if let  timelineData = data["timeline"] as? NSArray
        {
            var mydataObj:[Timeline_Model] = []
            for obj in timelineData
            {
                let localdata = Timeline_Model.timelinedata(data: obj as! NSDictionary)
                mydataObj.append(localdata)
            }
            fGroupObj.timeline = mydataObj
        }
        return fGroupObj
    }

    class func timelinedata(data:NSDictionary) -> Timeline_Model
    {
        let mygroupObj = Timeline_Model()
        mygroupObj.ge_id = data["ge_id"] as? Int
        mygroupObj.ge_orgid = data["ge_orgid"] as? Int
        mygroupObj.ge_organizationname = data["ge_organizationname"] as? String
        mygroupObj.ge_organizationlogo = data["ge_organizationlogo"] as? String
        mygroupObj.ge_type = data["ge_type"] as? Int
        mygroupObj.ge_posttext = data["ge_posttext"] as? String
       mygroupObj.ge_postdata = data["ge_postdata"] as? String
        mygroupObj.ge_posttype = data["ge_posttype"] as? Int
        mygroupObj.ge_date = data["ge_date"] as? String
        return mygroupObj
    }
    
    class func taskAndmeeting_status(data:NSDictionary) -> Timeline_Model
    {
        let tmObj = Timeline_Model()
        tmObj.taskMeeting_status = data["status"] as? Int
        tmObj.taskMeetingcode = data["code"] as? Int
        tmObj.taskMeetingmessage =  data["message"] as? String
        if let Taskdata = data["tasks"] as? NSArray
        {
            var taskObj:[Timeline_Model] = []
            for obj in Taskdata
            {
                let taskDetails = Timeline_Model.taskInfo(data: obj as! NSDictionary)
                taskObj.append(taskDetails)
            }
            tmObj.tasks = taskObj
        }
        
        if let meetingsdata = data["meetings"] as? NSArray
        {
            var meetingsObj:[Timeline_Model] = []
            for obj in meetingsdata
            {
                 let meetingsDetails = Timeline_Model.meetingInfo(data: obj as! NSDictionary)
                 meetingsObj.append(meetingsDetails)
            }
            tmObj.meetings = meetingsObj
        }
        return tmObj
    }
    
    class func taskInfo(data:NSDictionary) -> Timeline_Model
    {
        let mytaskObj = Timeline_Model()
        mytaskObj.companyid = data["companyid"] as? Int
        mytaskObj.subcompanyid = data["subcompanyid"] as? Int
        mytaskObj.goal_id = data["goal_id"] as? Int
        mytaskObj.project_id = data["project_id"] as? Int
        mytaskObj.milestone_id = data["milestone_id"] as? Int
        mytaskObj.delivery_status = data["delivery_status"] as? Int
        mytaskObj.task_id = data["task_id"] as? Int
        mytaskObj.task_name = data["task_name"] as? String
        mytaskObj.task_description = data["description"] as? String
        mytaskObj.startdate = data["startdate"] as? String
        mytaskObj.enddate = data["enddate"] as? String
        mytaskObj.estimatedhrs = data["estimatedhrs"] as? String
        mytaskObj.remaininghours = data["remaininghours"] as? String
        mytaskObj.mode = data["mode"] as? String
        mytaskObj.task_status = data["task_status"] as? Int
        mytaskObj.createdby = data["createdby"] as? Int
        mytaskObj.firstname = data["firstname"] as? String
        mytaskObj.lastname = data["lastname"] as? String
        mytaskObj.logo = data["logo"] as? String
         return mytaskObj
    }
    
    class func meetingInfo(data:NSDictionary) -> Timeline_Model
    {
           let mytaskObj = Timeline_Model()
           mytaskObj.m_companyid = data["companyid"] as? Int
           mytaskObj.m_subcompanyid = data["subcompanyid"] as? Int
           mytaskObj.m_meeting_id = data["meeting_id"] as? Int
           mytaskObj.m_goal_id = data["goal_id"] as? Int
           mytaskObj.m_goal_name = data["goal_name"] as? Int
           mytaskObj.m_project_id = data["project_id"] as? Int
           mytaskObj.m_project_name = data["project_name"] as? Int
           mytaskObj.m_meetingname = data["meetingname"] as? String
           mytaskObj.m_description = data["description"] as? String
           mytaskObj.m_startdate = data["startdate"] as? String
           mytaskObj.m_duration = data["duration"] as? String
           mytaskObj.m_createdby = data["createdby"] as? Int
           mytaskObj.m_meetingstatus = data["meetingstatus"] as? Int
           return mytaskObj
     }
}

