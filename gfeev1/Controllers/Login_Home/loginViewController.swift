//
//  loginViewController.swift
//  gfeev1
//
//  Created by IGPL on 04/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire
//var New_secretKey = "NO_secretKey"
//var New_otp = "NO_secretKey"
//var New_userid = 0
//var New_accessKey = "NO_accessKey"
var baseURL = "https://gfee.co/superadmin/api/wsv2/"
class loginViewController: UIViewController
{
   // https://gfee.co/superadmin/api/wsv2/login
    
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var password: UITextField!
    var loginobj:[Modeldata] = []
//    internal typealias CompletionHandler = ( _ responseObject : AnyObject?, _ errorObject:NSError?) -> ()
    internal typealias CompletionHandler = ( _ responseObject : AnyObject?,  _ errorObject : NSError?) -> ()
    
   let url:String = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/login"
 //   let url: String = "https://gfee.co/superadmin/api/wsv2/login"
// let url:String = "http://192.168.10.189/gfee/superadmin/api/wsv1/login"
 //   let url:String = "\(baseURL)login"
   
    //let url:String = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/login"
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.makeToast("Hello Gfee")
        print("Login URL\(url)")
//        self.getapi(email:"rameshragadi@innovativegraphicss.in", pwd:"rra@igpl",devicetype:"3",id:"rrgrgreg56546m5n6m5n4j645jk6n5k46n", callback: { (logdata, error) in
//                               self.loginobj = logdata as! [Modeldata]
//                               if error == nil
//                               {
//                               if self.loginobj.count > 0
//                               {
//                                   for i in self.loginobj
//                                       {
//                                           print("the id is = \(i.id!)")
//                                           print("the firstname is = \(i.firstname!)")
//                                           print("the lastname is = \(i.lastname!)")
//                                       }
//                                  //self.showAlert(msg: "Success and Your name is \(self.loginobj[0].firstname!)")
//                                DispatchQueue.main.async {
//                                    self.view.makeToast("You Logged In \(self.loginobj[0].firstname!)")
//                                }
//
//
//                                }
//                           }
//                           else
//                           {
//                               //self.showAlert(msg: (error?.localizedDescription)!)
//                            DispatchQueue.main.async {
//                              self.view.makeToast((error?.localizedDescription)!)
//                            }
//
//                           }
//                       })

    }
    
    @IBAction func signIN(_ sender: UIButton)
    {
        let email = emailID.text!
        let pswd = password.text!
        print("credentials",email,pswd)
        self.getapi(email:email, pwd:pswd,devicetype:"3",id:"rrgrgreg56546m5n6m5n4j645jk6n5k46n", callback: { (logdata, error) in
             self.loginobj = logdata as! [Modeldata]
             if error == nil
             {
             if self.loginobj.count > 0
             {
                 for i in self.loginobj
                     {
                         print("the id is = \(i.id!)")
                         print("the firstname is = \(i.firstname!)")
                         print("the lastname is = \(i.lastname!)")
                        UserDefaults.standard.set(i.secretkey, forKey: "secretkey")
                        UserDefaults.standard.set(i.firstname, forKey: "name")
                        UserDefaults.standard.set(i.id, forKey: "userid")
                        UserDefaults.standard.set(i.logo, forKey: "logo")
                        
                     }
                //self.showAlert(msg: "Success and Your name is \(self.loginobj[0].firstname!)")
              DispatchQueue.main.async {
                  self.view.makeToast("You Logged In \(self.loginobj[0].firstname!)")
              }
                self.navigateToHome()

              }
         }
         else
         {
            //self.showAlert(msg: (error?.localizedDescription)!)
            DispatchQueue.main.async {
            self.view.makeToast((error?.localizedDescription)!)
          }
          
         }
     })
    }
    
    
    func getapi(email:String,pwd:String,devicetype:String,id:String,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "email=\(email)".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(pwd)".data(using: String.Encoding.utf8)!)
        postData.append("&devicetype=\(devicetype)".data(using: String.Encoding.utf8)!)
        postData.append("&deviceid=\(id)".data(using: String.Encoding.utf8)!)

        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localarray:[Modeldata] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
           print(data)
            if response?.description == nil{
             //  self.showAlert(msg: "No response from Server")
             //   self.view.makeToast("No response from Server")
            }
            else if (error != nil) {
                 callback(nil, error as! NSError)
                } else {
                do
                  {
                      localarray.removeAll()
                      let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                      print("code----",jsonResult["code"] as! Int)
                      let loginobj = jsonResult["login"] as? NSDictionary
                      let cstate = jsonResult["status"] as! Int
                      if cstate == 0
                      {
                         // self.showAlert(msg: jsonResult["message"] as! String)
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((jsonResult["message"] as! String))
                        }
                        
                        
                      }
                      else
                      {
                         // self.navigateToHome()
                          let objdata = loginobj as! [String:Any]
                          let localobj = Modeldata().fetchdatafordisplay(display: objdata)
                          localarray.append(localobj)
                          callback(localarray as AnyObject, nil)
                        
                      }
                   }
                  catch
                  {
                      print("handle error")
                  }
                 }
            })
        dataTask.resume()
    }
    
    func navigateToHome()
    {
        DispatchQueue.main.async
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "toTab") as! UITabBarController
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
//    func showAlert(msg:String)
//    {
//        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
//        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
//
//        }
//        alert.addAction(ok)
//        DispatchQueue.main.async {
//            self.present(alert, animated: true)
//        }
//        dismiss(animated: true, completion: nil)
//    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle
    {
        return .darkContent
    }


}


class Modeldata : NSObject
{
       var id:Int?
       var firstname:String?
       var lastname:String?
       var email:String?
       var phone:String?
       var logo:String?
       var secretkey:String?
    
    func fetchdatafordisplay(display:[String:Any]) -> Modeldata
    {
        let myobj = Modeldata()
        for (key,val) in display
        {
            switch key
            {
            case "id":
                myobj.id = val as? Int ?? 0
                
            case "firstname":
                myobj.firstname = val as? String ?? ""
                
            case "lastname":
                myobj.lastname = val as? String ?? ""
                
            case "email":
                myobj.email = val as? String ?? ""
            
            case "phone":
                myobj.phone = val as? String ?? ""
            case "logo":
                myobj.logo = val as? String ?? ""
            case "secretkey":
                myobj.secretkey = val as? String ?? ""
             default:
            break
            }
        }
        return myobj
    }
}
