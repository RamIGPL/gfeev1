//
//  editMyOrgViewController.swift
//  gfeev1
//
//  Created by IGPL on 12/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class editMyOrgViewController: UIViewController {
    var access = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkAction(_ sender: UIButton)
    {
        if sender.isSelected
        {
            sender.isSelected = false
            access = 0
        }
        else
        {
            sender.isSelected = true
            access = 1
        }
    }
}
