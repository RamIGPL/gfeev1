//
//  SuggestedOrgaViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
  var typeval_forOrgListDisplay = 0
//let userid = UserDefaults.standard.integer(forKey: "userid")
//let accesskey = UserDefaults.standard.string(forKey: "accesskey")

class SuggestedOrgaViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,selectedOrgType2
{

    
    func selected2(selectedID: String)
    {
        if Int(selectedID) == 0
        {
           selectOrgTypeBtn.setTitle("Select Organization Type", for: .normal)
           typeval_forOrgListDisplay = 0
             tv.reloadData()
           refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
        if Int(selectedID) == 1
        {
            selectOrgTypeBtn.setTitle("Company", for: .normal)
            typeval_forOrgListDisplay = 1
            tv.reloadData()
            tv.reloadData()
            refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
        if Int(selectedID) == 2
        {
           selectOrgTypeBtn.setTitle("School", for: .normal)
           typeval_forOrgListDisplay = 2
           tv.reloadData()
           refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
        if Int(selectedID) == 3
        {
            selectOrgTypeBtn.setTitle("College", for: .normal)
            typeval_forOrgListDisplay = 3
             tv.reloadData()
            print("typeval_forOrgListDisplay ==",typeval_forOrgListDisplay)
             refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
        if Int(selectedID) == 4
        {
          selectOrgTypeBtn.setTitle("Institute", for: .normal)
            typeval_forOrgListDisplay = 4
             tv.reloadData()
             refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
        if Int(selectedID) == 5
        {
            selectOrgTypeBtn.setTitle("Other", for: .normal)
            typeval_forOrgListDisplay = 5
            tv.reloadData()
             refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        }
    }
    
    internal typealias CompletionHandler = ( _ responseObj:organizationList_Model?, _ errorOBj:NSError?) -> ()
    let orgListURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/organizationlist"
    func selectedOrg(selected: String)
    {
        if selected == "HM"
        {
            selectOrgTypeBtn.setTitle(selected, for: .normal)
        }
        if selected == "Organizations"
        {
            selectOrgTypeBtn.setTitle(selected, for: .normal)
        }
    }
    
    
    @IBOutlet weak var selectOrgTypeBtn: UIButton!
    
    var selectedIndex = 0
    @IBOutlet weak var tv: UITableView!
     @IBOutlet weak var segment: UISegmentedControl!
    var sugg_orgsTitle = ["Ram @HM","Organizations","Groups","Scan","Change Password","Logout"]
    var sugg_orgsDescr = ["Its HM Geneva House","Organizations","Groups","Scan","Change Password","Logout"]
    var my_orgsTitle = ["IGPL","Organizations","Groups","Scan"]
    var my_orgsDescr = ["Innovative graphics Private Limited","Organizations","Groups","Last one"]
    var orgName_Array:[String] = []
    var Sugg_orgFollower:[Int] = []
    var sugg_orgName_Array:[String] = []
    var orgFollower_Array:[Int] = []
    var orgImg_Array:[String] = []
    var sugg_orgID:[Int] = []
    var sugg_orgType:[Int] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if segment.selectedSegmentIndex == 0
        {
//              return sugg_orgsTitle.count
            return sugg_orgName_Array.count
        }
        if segment.selectedSegmentIndex == 1
        {
             return orgName_Array.count
            //return my_orgsTitle.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
          let cell = tableView.dequeueReusableCell(withIdentifier: "orgcell", for: indexPath) as! organizationTableViewCell
        if segment.selectedSegmentIndex == 0
        {
            selectOrgTypeBtn.isHidden = false
            cell.followOutlet.isHidden = true
            if sugg_orgName_Array.isEmpty
            {
                DispatchQueue.main.async
                {
                    self.view.makeToast(("Data Not Found"))
                     self.tv.reloadData()
                }
            }
            else
            {
                selectOrgTypeBtn.isHidden = false
                cell.followOutlet.isHidden = true
       //         cell.Description.text = String(orgFollower_Array[indexPath.row])
//                cell.Title.text = orgName_Array[indexPath.row]
                return cell
            }
        }
            
        if segment.selectedSegmentIndex == 1
        {
            selectOrgTypeBtn.isHidden = true
            cell.followOutlet.isHidden = false
            if sugg_orgName_Array.isEmpty
            {
                DispatchQueue.main.async
                {
                    self.view.makeToast(("Data Not Found"))
                    self.tv.reloadData()
                }
            }
            else
            {
                selectOrgTypeBtn.isHidden = true
                cell.followOutlet.isHidden = false
                cell.Title.text = orgName_Array[indexPath.row]
                return cell
            }
            
        }
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
       // segment.isHidden = false
       // refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
        self.tv.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedIndex = indexPath.row
        if segment.selectedSegmentIndex == 0
        {
            performSegue(withIdentifier: "toDetails", sender: self)
        }
        if segment.selectedSegmentIndex == 1
        {
             performSegue(withIdentifier: "toDetails2", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toDetails"
        {
            let destvc = segue.destination as! organizationDetailsViewController
            destvc.orgName = sugg_orgName_Array[selectedIndex]
            destvc.orgID = sugg_orgID[selectedIndex]
            destvc.orgType = sugg_orgType[selectedIndex]
        }
        
        if segue.identifier == "toDetails2"
        {
            let destvc = segue.destination as! myOrgViewController
            destvc.organizationName = orgName_Array[selectedIndex]
//            destvc.orgID = sugg_orgID[selectedIndex]
//            destvc.orgType = sugg_orgType[selectedIndex]
        }
    }
    
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if groupFrom == "from_CreateGroup"
        {
            
            
        }
        if groupFrom == "from_MenuGroup"
        {
            
        }
        refreshOrgList(typeval_forOrgListDisplay: typeval_forOrgListDisplay)
//        self.getOwnGroupAPI(userid:userid,accesskey:accesskey!,type:typeval_forOrgListDisplay, callback: { (orgdata, error) in
//        if let orgdata = orgdata
//        {
//        101208388145
//        MAcAEw#@&026
//        Sub: Request to correct date of birth of Adhar card in PF portal
//        Dear Sir,
//        This is to bring your kind notice that my date of birth in PF account showing old dob, my actual updated dob is 01/06/1992 but it is showing old dob as 01/01/1992. So I request you to please correct my date of birth and my UAN number is 101208388145.
//        Regards,
//            print(orgdata.status!,"my orgdata responsee..")
//          //  print(orgdata.myorganization1!,"my orgdata details ..")
//            print("in01 \(String(describing: orgdata.message))")
//            if orgdata.status! == 1
//            {
//                if orgdata.myorganization1!.isEmpty
//                {
//                    DispatchQueue.main.async
//                     {
//                         self.view.makeToast(("Data Not Found"))
//                     }
//                    print("No data Found 1")
//                }
//                else
//                {
        
        
        
//               i updated my Adhar card dob but it is not reflecting from Employer please  notice and update the same in pf portal
//                        for obj in orgdata.myorganization1!
//                    {
//                        print(obj.ge_name!, "Different types are:")
//                        self.orgFollower_Array.append(obj.followers!)
//                        self.orgName_Array.append(obj.ge_name!)
//                        DispatchQueue.main.sync
//                        {
//                             self.tv.reloadData()
//                        }
//                    }
//                }
//                if orgdata.suggestedorganization!.isEmpty
//                {
//                    DispatchQueue.main.async
//                    {
//                        self.view.makeToast(("Data Not Found"))
//                    }
//                    print("No data 2")
//               }
//                else
//                {
//                    for obj in orgdata.suggestedorganization!
//                    {
//                        self.sugg_orgName_Array.append(obj.sugg_name!)
//                        DispatchQueue.main.sync
//                        {
//                          self.tv.reloadData()
//                        }
//                    }
//                }
//                }
//            }
//               })
    
    }
    func refreshOrgList(typeval_forOrgListDisplay:Int)
    {
        self.getOwnGroupAPI(userid:userid,accesskey:accesskey!,type:typeval_forOrgListDisplay, callback: { (orgdata, error) in
        if let orgdata = orgdata
        {
            
           // var mycurrentOrgData:[orgTypModel2] = []
           // mycurrentOrgData = orgdata.myorganization1
            print(orgdata.status!,"my orgdata responsee..")
          //  print(orgdata.myorganization1!,"my orgdata details ..")
            print("in01 \(String(describing: orgdata.message))")
            if orgdata.status! == 1
            {
                if orgdata.myorganization1!.isEmpty
                {
                    DispatchQueue.main.async
                     {
                         self.view.makeToast(("Data Not Found"))
                         self.tv.reloadData()
                     }
                    print("No data Found 1")
                }
                else
                {
                   for obj in orgdata.myorganization1!
                    {
                        print(obj.ge_name!, "Different types are:")
                        self.orgFollower_Array.append(obj.followers!)
                        self.orgName_Array.append(obj.ge_name!)
                        DispatchQueue.main.sync
                        {
                             self.tv.reloadData()
                        }
                    }
                }
                if orgdata.suggestedorganization!.isEmpty
                {
                    DispatchQueue.main.async
                    {
                        self.view.makeToast(("Data Not Found"))
                         self.tv.reloadData()
                    }
                    print("No data 2")
               }
                else
                {
                    for obj in orgdata.suggestedorganization!
                    {
                        self.sugg_orgName_Array.append(obj.sugg_name!)
                        self.sugg_orgID.append(obj.sugg_org_id!)
                        self.sugg_orgType.append(obj.sugg_type!)

                        DispatchQueue.main.sync
                        {
                          self.tv.reloadData()
                        }
                    }
                }
                }
            }
               })
    }
    
    func getOwnGroupAPI(userid:Int,accesskey:String,type:Int,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: orgListURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
//                    self.orgName_Array.removeAll()
//                    self.orgImg_Array.removeAll()
//                    self.orgFollower_Array.removeAll()
//                    self.my_orgsTitle.removeAll()
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = organizationList_Model.orgList_status(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func back(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func segmentAction(_ sender: UISegmentedControl)
    {
        self.tv.reloadData()
    }
    
    
    
    @IBAction func Click(_ sender: UIButton)
    {
//            let popcontrol = storyboard?.instantiateViewController(withIdentifier: "type") as! orgTypeViewController
//            popcontrol.modalPresentationStyle = UIModalPresentationStyle.popover
//            popcontrol.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
//            popcontrol.preferredContentSize.height = 300
//            popcontrol.preferredContentSize.width =  300
//            popcontrol.popoverPresentationController?.sourceView = sender
//            popcontrol.popoverPresentationController?.sourceRect = sender.bounds
//            self.present(popcontrol, animated: true, completion: nil)
        let popController = self.storyboard?.instantiateViewController(withIdentifier: "type") as! orgTypeViewController
        popController.Delegate = self //as! selectedOrgType
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
        popController.preferredContentSize.height = 300
        popController.preferredContentSize.width =  270
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = sender.bounds
        self.present(popController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
}
 
class organizationList_Model:NSObject
{
   var myorganization1:[organizationList_Model]?
   var logo:String?
   var type:Int?
   var org_id:Int?
   var customerid:Int?
   var followers:Int?
   var ge_name:String?
   var myorganization:String?
           
   var status:Int?
   var message:String?
   var code:Int?
   
    var suggestedorganization:[organizationList_Model]?
    var sugg_org_id:Int?
    var sugg_name:String?
    var sugg_type:Int?
    var sugg_logo:String?
    var sugg_followers:Int?
    var sugg_status:Int?
    var sugg_myorganization:String?
            
    class func orgList_status(data:NSDictionary) -> organizationList_Model
    {
        let fGroupObj = organizationList_Model()
        fGroupObj.code = data["code"] as? Int
        fGroupObj.status = data["status"] as? Int
        fGroupObj.message = data["message"] as? String
        
        if let  myorgData = data["myorganization"] as? NSArray
        {
            var mydataObj:[organizationList_Model] = []
            for obj in myorgData
            {
                let localdata = organizationList_Model.orgdata(data: obj as! NSDictionary)
                mydataObj.append(localdata)
            }
            fGroupObj.myorganization1 = mydataObj
        }
        
        if let mysuggestorg = data["suggestedorganization"] as? NSArray
        {
            var mysuggestObj:[organizationList_Model] = []
            for obj in mysuggestorg
            {
                let localsuggestData = organizationList_Model.suggestOrgdata(data: obj as! NSDictionary)
                mysuggestObj.append(localsuggestData)
            }
            fGroupObj.suggestedorganization = mysuggestObj
        }
        return fGroupObj
    }

    
    class func orgdata(data:NSDictionary) -> organizationList_Model
    {
        let mygroupObj = organizationList_Model()
        mygroupObj.logo = data["id"] as? String
        mygroupObj.type = data["type"] as? Int
        mygroupObj.org_id = data["org_id"] as? Int
        mygroupObj.customerid = data["customerid"] as? Int
        mygroupObj.followers = data["followers"] as? Int
        mygroupObj.ge_name = data["ge_name"] as? String
        mygroupObj.myorganization = data["myorganization"] as? String
        return mygroupObj
    }
    
    class func suggestOrgdata(data:NSDictionary) -> organizationList_Model
    {
        let suggestObj = organizationList_Model()
            suggestObj.sugg_org_id = data["org_id"] as? Int
            suggestObj.sugg_name = data["name"] as? String
            suggestObj.sugg_type = data["type"] as? Int
            suggestObj.sugg_logo = data["logo"] as? String
            suggestObj.sugg_followers = data["followers"] as? Int
            suggestObj.sugg_status = data["status"] as? Int
            suggestObj.sugg_myorganization = data["myorganization"] as? String
            return suggestObj
    }
    
}
