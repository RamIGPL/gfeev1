//
//  myOrgViewController.swift
//  gfeev1
//
//  Created by IGPL on 12/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class myOrgViewController: UIViewController
{
    var organizationName = ""
    var orgID = 0
    var orgType = 0
    
    @IBOutlet weak var orgIcon: UIImageView!
    @IBOutlet weak var org_Name: UILabel!
    @IBOutlet weak var orgDisc: UILabel!
    @IBOutlet weak var industry: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var web: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var followerCount: UILabel!
    
    @IBOutlet weak var profImg: UIImageView!
    @IBOutlet weak var postText: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(organizationName,orgID,orgType)
    }
    
    @IBAction func editAction(_ sender: UIButton)
    {
        performSegue(withIdentifier: "toEdit", sender: self)
    }
    
    @IBAction func attach(_ sender: UIButton)
    {
        
    }
    @IBAction func send(_ sender: UIButton)
    {
        
    }
    
    @IBAction func backAction(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
}
