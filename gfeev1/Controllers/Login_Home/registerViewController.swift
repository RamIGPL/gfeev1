//
//  registerViewController.swift
//  gfeev1
//
//  Created by IGPL on 04/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import Alamofire
var New_secretKey = "NO_secretKey"
var New_otp = "NO_secretKey"
var New_userid = 0
var New_accessKey = "NO_accessKey"

var currentVC = "noVC"
class registerViewController: UIViewController
{
    var agree = 0
    var loginobj:[registerModel] = []
    let url = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/registration"
    let getAccessURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/getaccesskey"
  //  let localRegisterURL = "http://192.168.10.189/gfee/superadmin/api/wsv1/register"
    let localRegisterURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/registration"
    internal typealias CompletionHandler = ( _ responseObject : AnyObject?,  _ errorObject : NSError?) -> ()
    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var password: UITextField!
    var data = ""
    let intData = 1
    
    @IBOutlet weak var checkBtn: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func CheckBtnAction(_ sender: UIButton)
    {
        if sender.isSelected
        {
            sender.isSelected = false
            agree = 0
        }
        else
        {
            sender.isSelected = true
            agree = 1
        }
    }
    
    

    @IBAction func signUP(_ sender: UIButton)
    {
            currentVC = "registerVC"
            print("Current flow--",currentVC)
            let phone = mobile.text!
                 let Gmail = email.text!
                 let Password = password.text!
                 //let agree = 1
                 let firstname = fname.text!
                 let lastname = lname.text!
                 let devicetype = 3
                 let deviceid = "fdfdgdfgfdgdfg"
//                 let parameters: [String:Any] = ["phone":phone,
//                     "email":Gmail,
//                     "password":Password,
//                     "agree":agree,
//                     "firstname":firstname,
//                     "lastname":lastname,
//                     "devicetype":devicetype,
//                     "deviceid":deviceid]
//
        self.getapi(phone:phone, email:Gmail,password:Password,agree:agree,firstname:firstname,lastname:lastname,devicetype:devicetype,deviceid:deviceid, callback: { (logdata, error) in
                self.loginobj = logdata as! [registerModel]
                if error == nil
                {
                if self.loginobj.count > 0
                {
                    for i in self.loginobj
                        {
                            print("the id is = \(i.id!)")
                            print("the firstname is = \(i.firstname!)")
                            print("the lastname is = \(i.lastname!)")
                            currentUserId = i.id!
                            currentOTP = i.otp!
                            print("currentUserID is",currentUserId)
//                            mydefaults.set(i.id!, forKey: "userid")
//                            mydefaults.set(i.otp!, forKey: "otp")
//                            mydefaults.set(i.secretkey, forKey: "secretkey")
                          //  currentSecretKey = i.secretkey!
                           
                        }
                    currentSecretKey = self.loginobj[0].secretkey!
                    print("Current Secret key",currentSecretKey)
                   print( "Success and Your name is \(self.loginobj[0].firstname!)")
                 DispatchQueue.main.async {
                     self.view.makeToast("\(self.loginobj[0].firstname!) You are Registered Successfully")
                   // currentSecretKey = self.loginobj[0].secretkey!
                     print("Current Secret key",currentSecretKey)
                    self.navigateToHome()
                 }
                   //self.navigateToHome()
                    
                   

                 }
            }
            else
            {
                //self.showAlert(msg: (error?.localizedDescription)!)
             DispatchQueue.main.async {
               self.view.makeToast((error?.localizedDescription)!)
             }
             
            }
        })

       //  submit()
      //  performSegue(withIdentifier: "toOTP", sender: self)
    }

  func navigateToHome()
     {
         DispatchQueue.main.async
         {
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "toOTP") as! otpViewController
            vc.received_id = currentUserId
            vc.received_secretKey  = currentSecretKey
            vc.recieved_otp = currentOTP
            
            print(currentSecretKey,currentUserId)
             let navigationController = UINavigationController(rootViewController: vc)
            
             self.present(navigationController, animated: true, completion: nil)
          //   self.show(navigationController, sender: self)
         }
        
     }
    
    func getapi(phone:String,email:String,password:String,agree:Int,firstname:String, lastname:String,devicetype:Int,deviceid:String, callback: @escaping CompletionHandler)
    {
        let headers = [
          "Content-Type": "application/x-www-form-urlencoded"
        ]

        let postData = NSMutableData(data: "phone=\(phone)".data(using: String.Encoding.utf8)!)
        postData.append("&email=\(email)".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(password)".data(using: String.Encoding.utf8)!)
        postData.append("&agree=\(agree)".data(using: String.Encoding.utf8)!)
        postData.append("&firstname=\(firstname)".data(using: String.Encoding.utf8)!)
        postData.append("&lastname=\(lastname)".data(using: String.Encoding.utf8)!)
        postData.append("&devicetype=\(devicetype)".data(using: String.Encoding.utf8)!)
        postData.append("&deviceid=\(deviceid)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: localRegisterURL)! as URL,cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localarray:[registerModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
           print(data)
            if response?.description == nil{
             //  self.showAlert(msg: "No response from Server")
                DispatchQueue.main.async
                {
                    self.view.makeToast("No response from Server")
                }
                
            }
            else if (error != nil) {
                 callback(nil, error as! NSError)
                } else {
                do
                  {
                      localarray.removeAll()
                      let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                      print("code----",jsonResult["code"] as! Int)
                      let loginobj = jsonResult["register"] as? NSDictionary
                      let cstate = jsonResult["status"] as! Int
                      if cstate == 0
                      {
                         // self.showAlert(msg: jsonResult["message"] as! String)
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((jsonResult["message"] as! String))
                        }
                        
                        
                      }
                      else
                      {
                         // self.navigateToHome()
                          let objdata = loginobj as! [String:Any]
                          let localData = registerModel().fetchdata(display: objdata)
                          localarray.append(localData)
                          callback(localarray as AnyObject, nil)
                        
                      }
                   }
                  catch
                  {
                      print("handle error")
                  }
                 }
            })
        dataTask.resume()
    }
    
  /*     func submit()
       {
            
           
           AF.request(localRegisterURL, method: .post, parameters: parameters).responseJSON
           {
                response in
                print("response = \(String(describing: response.result))")
            let result = response.value
            for i in result as! NSDictionary
            {
                if  i.key as! String == "code"
                {
                    print(i.value)
                }
                else if  i.key as! String == "status"
                {
                    print(i.value)
                }
              else if ( i.key as! String == "message") && (i.value as! String == "Registration Successful")
                  
              {
                print(i.key,i.value,"both values")
                if i.key as! String == "register"
                {
                        let mydata = i.value as! [String:Any]
                    print(mydata)
                        print("Different name",mydata["firstname"] as! String)
                        for i in mydata
                        {
                            print(i.key,"----",i.value)
//                            var New_secretKey = "NO_secretKey"
//                            var New_otp = "NO_secretKey"
//                            var New_userid = 0
//                            var New_accessKey = "NO_accessKey"
                             if i.key == "id"
                             {
                                print(i.value,"Login Name")
                                print("New_userid before",New_userid)
                                New_userid = i.value as! Int
                                print("New_userid after Assigning",New_userid)
                             }
                             if i.key == "otp"
                             {
                                print("New_otp before",New_userid)
                                New_otp = i.value as! String
                             }
                             if i.key == "secretkey"
                             {
                                 print(New_secretKey,"Gmail")
                                 New_secretKey = i.value as! String
                              }
                              if i.key == "phone"
                             {
                                print(i.value,"phone number")
                             }//i["firstname"]
                           if i.key == "firstname"
                            {
                                print(i.value,"Login Name")
                                
                            }
                            if i.key == "lastname"
                            {
                                print(i.value,"Last Name")
                                
                            }
                            if i.key == "email"
                            {
                                print(i.value,"Gmail")
                                
                            }
                            if i.key == "logo"
                            {
                                print(i.value,"logoString")
                            }
                            
                            
                      }
                    }
                print("New_secretKey updated1",New_secretKey)
                print(" New_otp updated2",New_otp)
                print(" New_userid updated3",New_userid)
                print(" New_accessKey updated1",New_accessKey)
              }
              else
              {
                print("Msg not successful")
              }
              self.performSegue(withIdentifier: "toOTP", sender: self)
            }
        }
    }*/
    
    func getAcccessKey()
    {
        let secratekey = "68c21dbf88655f1373e95457f9afe7b34c8b01a1cc29aea840ff9f030a4df48f"
        let userid = 165
        
         let params:[String:Any] = ["secretkey":secratekey,"userid":userid]
         
         AF.request(getAccessURL, method: .post, parameters: params).responseJSON { (response) in
             print("access Response is",response)
         }
    }
       
}


class registerModel:NSObject
{
    var email:String?
    var firstname:String?
    var id:Int?
    var lastname:String?
    var logo:String?
    var otp:Int?
    var phone:String?
    var secretkey:String?
    
    func fetchdata(display:[String:Any]) -> registerModel
    {
        let obj = registerModel()
        for (key,val) in display
        {
            switch key
            {
            case "email":
                obj.email = val as? String ?? ""
            case "firstname":
                obj.firstname = val as? String ?? ""
            case "id":
                obj.id = val as? Int ?? 0
            case "lastname":
                obj.lastname = val as? String ?? ""
            case "logo":
                obj.logo = val as? String ?? ""
            case "otp":
                obj.otp = val as? Int ?? 0
            case "phone":
                obj.phone = val as? String ?? ""
            case "secretkey":
                obj.secretkey = val as? String ?? ""
            default:
                break
            }
        }
        return obj
    }
}
