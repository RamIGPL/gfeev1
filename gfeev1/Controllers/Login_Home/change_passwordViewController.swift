//
//  change_passwordViewController.swift
//  gfeev1
//
//  Created by IGPL on 04/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class change_passwordViewController: UIViewController {
    let resetPasswordURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/resetpassword"
    //let resetPasswordURL = "http://192.168.10.189/gfee/superadmin/api/wsv2/resetpassword"
     let forgoturl:String = "http://192.168.10.189/gfee/superadmin/api/wsv2/forgotpassword"
    internal typealias CompletionHandler = ( _ responseObject : AnyObject?, _ errorObject:NSError?) -> ()
    var logData:[cPaswdModel] = []
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var mobileText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var confirmPswdText: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func changePswdAction(_ sender: UIButton)
    {
                               let phone = mobileText.text!
                               let Gmail = emailText.text!
                               let Password = passwordText.text!
                               let confirmPswd = confirmPswdText.text!
                               let userid = currentUserId
                               let accesskey = currentAccesskey
                            print("Access key in change pswd===",accesskey)
                            print("userid in change pswd===",userid)
                            self.getapi(phone:phone, email:Gmail,password:Password,confirm_password:confirmPswd,userid:currentUserId,accesskey:currentAccesskey, callback: { (logdata, error) in
                              self.logData = logdata as! [cPaswdModel]
                                print(logdata as Any,"logdata")
                              if error == nil
                              {
                                
                                print(logdata!["message"])
//                                DispatchQueue.main.async
//                                {
//                                  self.view.makeToast((error?.localizedDescription)!)
//                                }
                         //     if self.loginobj.count > 0
                         /*     {
                                  for i in self.loginobj
                                      {
                                          print("the id is = \(i.id!)")
                                          print("the firstname is = \(i.firstname!)")
                                          print("the lastname is = \(i.lastname!)")
                                          currentUserID = i.id!
                                          currentOTP = i.otp!
                                          print("currentUserID is",currentUserID)
              //                            mydefaults.set(i.id!, forKey: "userid")
              //                            mydefaults.set(i.otp!, forKey: "otp")
              //                            mydefaults.set(i.secretkey, forKey: "secretkey")
                                        //  currentSecretKey = i.secretkey!
                                         
                                      }
                                  currentSecretKey = self.loginobj[0].secretkey!
                                  print("Current Secret key",currentSecretKey)
                                 print( "Success and Your name is \(self.loginobj[0].firstname!)")
                               DispatchQueue.main.async {
                                   self.view.makeToast("\(self.loginobj[0].firstname!) You are Registered Successfully")
                                 // currentSecretKey = self.loginobj[0].secretkey!
                                   print("Current Secret key",currentSecretKey)
                                  self.navigateToHome()
                               }
                                 //self.navigateToHome()
                               }*/
                          }
                          else
                          {
                              //self.showAlert(msg: (error?.localizedDescription)!)
                           DispatchQueue.main.async {
                             self.view.makeToast((error?.localizedDescription)!)
                           }
                           
                          }
                      })
    }
   
    func getapi(phone:String,email:String,password:String,confirm_password:String, userid:Int,accesskey:String, callback: @escaping CompletionHandler)
    {
        let headers = [
          "Content-Type": "application/x-www-form-urlencoded"
        ]

        let postData = NSMutableData(data: "phone=\(phone)".data(using: String.Encoding.utf8)!)
        postData.append("&email=\(email)".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(password)".data(using: String.Encoding.utf8)!)
        postData.append("&confirm_password=\(confirm_password)".data(using: String.Encoding.utf8)!)
        postData.append("&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: resetPasswordURL)! as URL,cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localarray:[cPaswdModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
           print(data)
            if response?.description == nil{
             //  self.showAlert(msg: "No response from Server")
                DispatchQueue.main.async
                {
                    self.view.makeToast("No response from Server")
                }
                
            }
            else if (error != nil) {
                 callback(nil, error as! NSError)
                } else {
                do
                  {
                      localarray.removeAll()
                      let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                      print("code----",jsonResult["code"] as! Int)
                      let loginobj = jsonResult["message"] as? String
                      let cstate = jsonResult["status"] as! Int
                      if cstate == 0
                      {
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((jsonResult["message"] as! String))
                        }
                      }
                      else
                      {
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((jsonResult["message"] as! String))
                        }
                        print("Nothing to go..")
                         // self.navigateToHome()
                         // let objdata = loginobj as! String
                        let localData = cPaswdModel().cpswd(data: jsonResult as! [String : Any])
                          localarray.append(localData)
                          callback(localarray as AnyObject, nil)
                        
                      }
                   }
                  catch
                  {
                      print("handle error")
                  }
                 }
            })
        dataTask.resume()
    }
}


class cPaswdModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    func cpswd(data:[String:Any]) -> cPaswdModel
    {
        var myobj = cPaswdModel()
        
        for (key,val) in data
        {
            switch key {
                case "message":
                myobj.message = val as? String ?? ""
                case "code":
                myobj.code = val as? Int ?? 0
                case "status":
                myobj.status = val as? Int ?? 0
            default:
                print("No case")
            }
        }
        return myobj
    }
}
