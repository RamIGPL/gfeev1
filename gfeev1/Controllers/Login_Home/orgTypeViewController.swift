//
//  orgTypeViewController.swift
//  gfeev1
//
//  Created by IGPL on 12/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//



import UIKit
protocol selectedOrgType2
{
    func selected2(selectedID:String)
}
//var myTableArray:[String] = []
class orgTypeViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    internal typealias CompletionHandler = ( _ responseObject : orgTypModel2?,  _ errorObject : NSError?) -> ()
    let orgtypeURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/organizationtype"
   //  var orgType = ["Select organization Type","Company","School","College","Institute","Other"]
    var orgType = ["Select organization Type","Company","School","Select organization Type","Company","School","Select organization Type","Company","School"]
    var orgObj:[orgTypModel2] = []
    var myTableArray:[String] = []
    var myTableArrayIndex:[String] = []
   
    @IBOutlet weak var tv: UITableView!
    override func viewDidLoad()
    {
         //self.activityIndicator.isHidden = true
        super.viewDidLoad()
       // activityIndicator.startAnimating()
     //   tv.tableFooterView = UIView(frame: .zero)
//        let userid = //174
//        let accesskey = "a4a6347ebfaded84d6c8c2895c9d2399"
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
        self.getapi(userid:userid,accesskey:accesskey!, callback: { (logdata, error) in
            if let logdata = logdata
        {
            print(logdata.status!,"mylogdata")
            print("in01 \(String(describing: logdata.message))")
            
                DispatchQueue.main.sync
                {
                    for obj in logdata.orgtype!
                    {
                        print(obj.type!,"Different types are:")
                        self.myTableArray.append(obj.type!)
                        self.myTableArrayIndex.append(String(obj.id!))
                        self.tv.reloadData()
                    }
                }
            
            
            
             
         }
        })
    }

    func getapi(userid:Int,accesskey:String,callback: @escaping CompletionHandler)
    {
           // activityIndicator.startAnimating()
            let headers = ["Content-Type": "application/x-www-form-urlencoded"]

            let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
            
            postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
            
            let request = NSMutableURLRequest(url: NSURL(string: orgtypeURL)! as URL,
            cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
            
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if let error = error
                {
                    callback(nil, error as NSError)
                }
                else
                {
                    do
                    {
                        if let josnResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                        {
                            let localObj = orgTypModel2.orgTypefirstLayer(data: josnResult)
                            callback(localObj, nil)
                        }
                        else
                        {
                            callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                        }
                    }
                    catch let error as NSError
                    {
                        callback(nil,error)
                    }
                }
            })
            dataTask.resume()
    }
   
    var Delegate:selectedOrgType2? = nil
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // return orgType.count
        //print(myTableArray.count)
       return myTableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typecell", for: indexPath)
        cell.textLabel?.text = myTableArray[indexPath.row]
        //cell.textLabel?.text = orgType[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       // typeDelegate?.selectedOrg(selected: orgType[indexPath.row])
    Delegate?.selected2(selectedID:myTableArrayIndex[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}

class orgTypModel2 : NSObject
{
    var orgtype:[orgTypModel2]?
    var id:Int?
    var type:String?
    var status:Int?
    var message:String?
    var code:Int?
    
    class func orgTypefirstLayer(data:NSDictionary) -> orgTypModel2
    {
        let myobj = orgTypModel2()
        myobj.status = data["status"] as? Int
        myobj.code = data["code"] as? Int
        myobj.message = data["message"] as? String
       // myobj.orgtype = data["orgtype"] as? NSArray
         if let myOrgData = data["orgtype"] as? NSArray
         {
            var myDataArray:[orgTypModel2] = []
            for obj in myOrgData
            {
                let localObj = orgTypModel2.myorgType(data: obj as! NSDictionary)
                print(localObj,"individual object of data")
                myDataArray.append(localObj)
            }
            myobj.orgtype = myDataArray
        }
        return myobj
    }
    
   class func myorgType(data:NSDictionary) -> orgTypModel2
    {
        let myobj = orgTypModel2()
        myobj.id = data["id"] as? Int
        myobj.type = data["type"] as? String
      //  myTableArray.append(myobj.type!)
        
        return myobj
     }
}

//enum myerror:Error
//{
//    case responseError(reason:String)
//}
