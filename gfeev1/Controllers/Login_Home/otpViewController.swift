//
//  otpViewController.swift
//  gfeev1
//
//  Created by IGPL on 28/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import Alamofire
class otpViewController: UIViewController {
    internal typealias CompletionHandler = ( _ responseObject : AnyObject?, _ errorObject : NSError?) -> ()
    let url = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/otpverify"
    let localotpURL =  "http://192.168.10.189/gfee/superadmin/api/wsv1/otpverify"
   // let localGetAccessURL = "http://192.168.10.189/gfee/superadmin/api/wsv2/getaccesskey"
    let forgoturl:String = "http://192.168.10.189/gfee/superadmin/api/wsv2/forgotpassword"
    let localGetAccessURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/getaccesskey"
    var received_id:Int?
    var received_secretKey:String?
    var recieved_otp:Int?
    var recieved_accesskey:String?
    var loginobj:[accessModel] = []
    var timer = Timer()
    @IBOutlet weak var timerCount: UILabel!
     @IBOutlet weak var timerLabel: UILabel!
    var seconds = 60
    //let localGetAccessURL =
    var otpobj:[otpModel] = [] //"https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/getaccesskey"
    @IBOutlet weak var otp: UITextField!
   
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
          //currentVC = "registerVC"
          //currentVC = "forgotVC"
        print("received datas are - ",received_secretKey!,received_id!,recieved_otp!)
     
           runTimer()
        self.getapi(secretkey:received_secretKey!, userid:received_id!, callback: { (logdata, error) in
                self.loginobj = logdata as! [accessModel]
                if error == nil
                {
                if self.loginobj.count > 0
                {
                    for i in self.loginobj
                        {
                            print("the access key is = \(i.key!)")
                            self.recieved_accesskey = i.key!
                            currentAccesskey = i.key!
//                            print("the firstname is = \(i.firstname!)")
//                            print("the lastname is = \(i.lastname!)")
                        }
                   //self.showAlert(msg: "Success and Your name is \(self.loginobj[0].firstname!)")
                 DispatchQueue.main.async {
                     //self.view.makeToast("You Logged In \(self.loginobj[0].key!)")
                 }
                   //self.navigateToHome()

                 }
            }
            else
            {
                //self.showAlert(msg: (error?.localizedDescription)!)
             DispatchQueue.main.async {
               self.view.makeToast((error?.localizedDescription)!)
             }
             
            }
        })


      
    }
    
    @IBAction func resendOTP(_ sender: UIButton)
    {
        seconds = 60
        timerLabel.text = "Seconds remaining :"
        timerLabel.textColor = UIColor.red
        timerLabel.textAlignment = .center
       self.runTimer()
       
        self.getapi(email:myEmail, phone:myphone, callback: { (logdata, error) in
                     mydataObj = logdata as! [forgotModel]
                       if error == nil
                       {
                       if mydataObj.count > 0
                       {
                       // self.runTimer()
                           for i in mydataObj
                               {
                                   
                                   print("the otp is = \(i.otp!)")
                                   currentSecretKey = i.secretkey!
                                   currentOTP = i.otp!
                                   currentUserId = i.userid!
                               }
                       
                       }
                   else
                   {
                       //self.showAlert(msg: (error?.localizedDescription)!)
                    DispatchQueue.main.async {
                      self.view.makeToast((error?.localizedDescription)!)
                    }
                    
                   }
            }
               })
    }
  //  }
    
    func getapi(email:String,phone:String,callback: @escaping CompletionHandler)
    {
        let headers = [
          "Content-Type": "application/x-www-form-urlencoded"
        ]

        let postData = NSMutableData(data: "email=\(email)".data(using: String.Encoding.utf8)!)
        postData.append("&phone=\(phone)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localarray:[forgotModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
           print(data)
            if response?.description == nil
            {
                self.view.makeToast("No response from Server")
            }
            else if (error != nil)
            {
                callback(nil, error as! NSError)
            }
            else
                {
                do
                  {
                      localarray.removeAll()
                      let jsonResult = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                      print("code----",jsonResult["code"] as! Int)
                      let loginobj = jsonResult["forgotpasswordotp"] as? NSDictionary
                      let cstate = jsonResult["status"] as! Int
                      if cstate == 0
                      {
                         // self.showAlert(msg: jsonResult["message"] as! String)
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((jsonResult["message"] as! String))
                        }
                       }
                      else
                      {
                         // self.navigateToHome()
                          let objdata = loginobj as! [String:Any]
                          let myobj = forgotModel().myfunc(dict: objdata)
                          localarray.append(myobj)
                          callback(localarray as AnyObject, nil)
                      }
                   }
                  catch
                  {
                      print("handle error")
                  }
                 }
            })
        dataTask.resume()
    }
    
// localGetAccessURL
//    otp:4541
//    userid:165
//    accesskey:0915686dfa38158f3dd94399f44d72d5
    
    func getapi(secretkey:String,userid:Int,callback:@escaping CompletionHandler)
    {
        let headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data:"userid=\(userid)".data(using:String.Encoding.utf8)!)
        postData.append("&secretkey=\(secretkey)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: localGetAccessURL)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localArray:[accessModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) -> Void in
        if response?.description == nil
        {
            self.view.makeToast("No Response from Server")
        }
        else if(error != nil)
        {
            callback(nil, error as! NSError)
        }
        else{
            do
            {
               localArray.removeAll()
               let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
               let accessObj = json["accesskey"] as? NSDictionary
               let accessStatus = json["status"] as! Int
               print("\(json["message"] as! String)")
               if accessStatus == 0
               {
                   DispatchQueue.main.async
                   {
                       self.view.makeToast((json["message"] as! String))
                    }
               }
               else
               {
                    let objdata = accessObj as! [String:Any]
                    let localData = accessModel().fetchAccesskey(data: objdata)
                    localArray.append(localData)
                    callback(localArray as AnyObject, nil)
               }
            }
            catch
            {
              print("Error handle")
            }
         }
         })
         dataTask.resume()
     }

    @IBAction func submit(_ sender: Any)
    {
//        let secratekey = "68c21dbf88655f1373e95457f9afe7b34c8b01a1cc29aea840ff9f030a4df48f"
//        let userid = 165
//       
//        let params:[String:Any] = ["secretkey":secratekey,"userid":userid]
//        
//        AF.request(getAccessURL, method: .post, parameters: params).responseJSON { (response) in
//            print("access Response is",response)
//        }
        submit()
    }
     
    func submit()
    {
        //let accessKey = recieved_accesskey
        let OTP = Int("\(otp.text!)")

        getotpapi(otp: OTP!, userid: received_id!, accesskey: recieved_accesskey!, callback: { (data, error) in
            self.otpobj = data as! [otpModel]
            if error == nil
            {
                if self.otpobj.count > 0
                {
                    print("you are here..\(currentVC)")
                    if currentVC == "registerVC"
                    {
                        for i in self.otpobj
                        {
                            print("otp varification final msg",i.message!)
                            self.navigateToLogin()
                        }
                        
                    }
                    if currentVC == "forgotVC"
                    {
                        for i in self.otpobj
                        {
                            print("otp varification final msg",i.message!)
                          //  currentAccessKey = i.a
                            self.navigateToForgotVC()
                        }
                    }
                }
            }
        })
        
        
    }
    func navigateToLogin()
    {
        DispatchQueue.main.async
        {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginVC") as! loginViewController
//           vc.received_id = currentUserID
//           vc.received_secretKey  = currentSecretKey
//           vc.recieved_otp = currentOTP
           
          // print(currentSecretKey,currentUserID)
            let navigationController = UINavigationController(rootViewController: vc)
           
            self.present(navigationController, animated: true, completion: nil)
            self.show(navigationController, sender: self)
        }
       
    }
    
      func navigateToForgotVC()
        {
            DispatchQueue.main.async
            {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "resetVC") as! change_passwordViewController
    //           vc.received_id = currentUserID
    //           vc.received_secretKey  = currentSecretKey
    //           vc.recieved_otp = currentOTP
               
              // print(currentSecretKey,currentUserID)
                let navigationController = UINavigationController(rootViewController: vc)
               
                self.present(navigationController, animated: true, completion: nil)
             //   self.show(navigationController, sender: self)
            }
           
        }
    
    func getotpapi(otp:Int,userid:Int,accesskey:String,callback:@escaping CompletionHandler)
    {
        let headers = ["Content-Type" : "application/x-www-form-urlencoded"]
        
        let postData = NSMutableData(data:"otp=\(otp)".data(using:String.Encoding.utf8)!)
        postData.append("&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        var localArray:[otpModel] = []
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest,completionHandler: {(data, response, error) ->
            Void in
            print(data,"response data")
            if response?.description == nil
            {
                self.view.makeToast("No Response from Server")
            }
            else if(error != nil)
            {
                callback(nil, error as! NSError)
            }
            else
            {
                do
                {
                    localArray.removeAll()
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                  //  let accessObj = json["accesskey"] as? NSDictionary
                    let accessStatus = json["status"] as! Int
                    print("\(json["message"] as! String)")
                    if accessStatus == 0
                    {
                        DispatchQueue.main.async
                        {
                            self.view.makeToast((json["message"] as! String))
                        }
                    }
                    else
                    {
                        let objdata = json as! [String:Any]
                        let localData = otpModel().fetchOTP(data: objdata)
                            
                        localArray.append(localData)
                        callback(localArray as AnyObject, nil)
                    }
                }
                catch
                {
                    print("Error handle")
                }
            }
            
        })
        dataTask.resume()
        
    }
    
   
     func runTimer()
     {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(otpViewController.updateTimer)), userInfo: nil, repeats: true)
         
         // self.pauseButton.isEnabled = true
     }
     
     @objc func updateTimer() {
         if seconds < 1 {
             timer.invalidate()
             print("timeup..")
            timerCount.text! = ""
            timerLabel.text = "Please Click on Resend OTP!"
            timerLabel.textColor = UIColor.black
             showAlert(msg: "Max. time limit is 1 minute. Please Resend Again")
         } else {
             seconds -= 1
             timerCount.text = formattedTime(time: TimeInterval(seconds))
             print("timedown..")
         }
     }
     
     
     func formattedTime(time:TimeInterval) -> String {
         let hours = Int(time) / 3600
         let minutes = Int(time) / 60 % 60
         let seconds = Int(time) % 60
         return String(format:"%02i:%02i", minutes, seconds)
     }
     
     func showAlert(msg:String)
     {
         let alert = UIAlertController.init(title: "OTP Expired!", message: msg, preferredStyle: .alert)
         let ok  = UIAlertAction(title: "OK", style: .default, handler: nil)
         self.present(alert, animated: false)
         alert.addAction(ok)
     }
     
     

}

class accessModel:NSObject
{
    var userid:Int?
    var secretkey:String?
    var key:String?
    func fetchAccesskey(data:[String:Any]) -> accessModel
    {
        let myAccessObj = accessModel()
        for(key,val) in data
           {
               switch(key)
               {
                    case "key":
                    myAccessObj.key = val as? String ?? ""
                    case "userid":
                    myAccessObj.userid = val as? Int ?? 0
                    case "secretkey":
                    myAccessObj.secretkey = val as? String ?? ""
                    default:
                    break
                }
           }
        return myAccessObj
    }
}

class otpModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    func fetchOTP(data:[String:Any]) -> otpModel
    {
        let myotpObj = otpModel()
        for(key,val) in data
           {
               switch(key)
               {
                    case "status":
                    myotpObj.status = val as? Int ?? 0
                    case "message":
                    myotpObj.message = val as? String ?? ""
                    case "code":
                    myotpObj.code = val as? Int ?? 0
                    default:
                    break
                }
           }
        return myotpObj
     }
}

//"status": 0,
//"message": "OTP Verified Unsuccessful",
//"code": 1004
