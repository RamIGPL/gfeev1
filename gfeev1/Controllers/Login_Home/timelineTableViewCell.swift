//
//  timelineTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 21/02/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class timelineTableViewCell: UITableViewCell
{
@IBOutlet weak var timelineGroupName: UILabel!
    @IBOutlet weak var timelineGroupDate: UILabel!
    @IBOutlet weak var timelineGroupPostText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
