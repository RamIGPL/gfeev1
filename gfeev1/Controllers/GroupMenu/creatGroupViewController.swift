//
//  creatGroupViewController.swift
//  gfeev1
//
//  Created by IGPL on 12/12/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
var groupFrom:String?
class creatGroupViewController: UIViewController,UIPopoverPresentationControllerDelegate,selectPeople
{
    
    var verification:Int = 0
    let createGroupURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/creategroup"
    internal typealias CompletionHandler = ( _ responseObj:createGroupModel?, _ errorObj :NSError?) -> ()
    func selectNames(select: String)
    {
        selectedPeople.setTitle(select, for: .normal)
    }
    
    @IBOutlet weak var selectedPeople: UIButton!
    @IBOutlet weak var profileOutlet:UIButton!
    var imagepicker = UIImagePickerController()
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var GroupName: UITextField!
    @IBOutlet weak var GroupDescription: UITextField!
    @IBOutlet weak var check1Outlet:UIButton!
    @IBOutlet weak var radioMale: UIButton!
    @IBOutlet weak var radioFemale: UIButton!
    @IBOutlet weak var imgOutlet: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func ch1Action(_ sender: UIButton)
    {
        if sender.isSelected {
           sender.isSelected = false
            verification = 0
        } else {
            sender.isSelected  = true
            verification = 1
        }
       /*radioMale.setImage(UIImage(named: "checkEmpty"), for: .normal)
          radioFemale.setImage(UIImage(named: "checkEmpty"), for: .normal)

          if sender.currentImage == UIImage(named: "checkEmpty"){

              sender.setImage(UIImage(named: "CheckFill"), for: .normal)

          }else{

              sender.setImage(UIImage(named: "checkEmpty"), for: .normal)
          }*/
    }
    
    @IBAction func picProfile(_ sender: UIButton)
    {
        
        pickingImg()
    }
    
    
    @IBAction func tapAtion(_ sender: UIButton)
    {
//        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations:
//        {
//            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
//        }) { (success) in
//            sender.isSelected = !sender.isSelected
//            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
//                sender.transform = .identity
//            }, completion: nil)
//        }
        if sender.isSelected {
                  sender.isSelected = false
        
               } else {
                   sender.isSelected  = true
               }
    }
    
    @IBAction func choosePeople(_ sender: UIButton)
    {
           let popController = self.storyboard?.instantiateViewController(withIdentifier: "peopleListViewController") as! peopleListViewController
                popController.Delegate = self
                popController.modalPresentationStyle = UIModalPresentationStyle.popover
                popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
                popController.preferredContentSize.height = 500
                popController.preferredContentSize.width =  350
                popController.popoverPresentationController?.delegate = self
                popController.popoverPresentationController?.sourceView = sender
                popController.popoverPresentationController?.sourceRect = sender.bounds
               self.present(popController, animated: true, completion: nil)
    }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
    
    
    @IBAction func createGroup(_ sender: UIButton)
    {
                let userid = UserDefaults.standard.integer(forKey: "userid")
                let accesskey = UserDefaults.standard.string(forKey: "accesskey")
                let groupTitle = GroupName.text!
                let groupDescreption = GroupDescription.text!
                let selectedPeopleName = 1
               // let verification = 1
               //   if self.GroupName.text == "" && self.GroupDescription.text == "" && self.verification == 0
        if self.GroupName.text == "" && self.GroupDescription.text == ""
                    {
                        DispatchQueue.main.async
                        {
                            self.view.makeToast("GroupName and GroupDescription are empty..!")
                        }
                    }
                    else if self.verification == 0
                    {
                         self.view.makeToast("Check varification..!")
                    }
             
               else
                {
                    self.getapi(userid: userid, accesskey: accesskey!,groupname: groupTitle, groupdescription: groupDescreption, verification: verification, selectedpeople: selectedPeopleName, callback:  { (orgData, error) in
                        if let orgData = orgData
                        {
                            groupFrom =  "from_CreateGroup"
                            print(orgData.message!,"Result of creation Group")
                            DispatchQueue.main.async
                            {
                                self.view.makeToast(orgData.message)
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "toGroups") as! suggestedGroupsViewController
                                let navigationController = UINavigationController(rootViewController: vc)
                                self.present(navigationController, animated: true, completion: nil)
                                self.show(navigationController, sender: self)
                                
                            }
                        }
                        else
                        {
                             DispatchQueue.main.async
                            {
                                self.view.makeToast("No Data..!")
                            }
                        }
                        
                    })
                }
        }
            //getapi(userid: userid, accesskey: accesskey!,groupname: groupTitle, groupdescription: groupDescreption!, verification: verification, selectedpeople: selectedPeopleName, callback:
        func getapi(userid:Int,accesskey:String,groupname:String,groupdescription:String,verification:Int,selectedpeople:Int, callback: @escaping CompletionHandler)
        {
                let headers = ["Content-Type": "application/x-www-form-urlencoded"]

                let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
                postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
                postData.append("&groupname=\(groupname)".data(using: String.Encoding.utf8)!)
                postData.append("&groupdescription=\(groupdescription)".data(using: String.Encoding.utf8)!)
                postData.append("&verification=\(verification)".data(using: String.Encoding.utf8)!)
                postData.append("&selectedpeople=\(selectedpeople)".data(using: String.Encoding.utf8)!)
               
                   
                
                let request = NSMutableURLRequest(url: NSURL(string: createGroupURL)! as URL,
                cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
                
                request.httpMethod = "POST"
                request.allHTTPHeaderFields = headers
                request.httpBody = postData as Data
                let session = URLSession.shared
                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                    
                    if let error = error
                    {
                        callback(nil, error as NSError)
                    }
                    else
                    {
                        do
                        {
                            if let josnResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                            {
                                let currentObj = createGroupModel.mycreategroup(data: josnResult)
                               // let localObj = orgTypModel.orgTypefirstLayer(data: josnResult)
                                callback(currentObj, nil)
                            }
                            else
                            {
                                callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                            }
                        }
                        catch let error as NSError
                        {
                            callback(nil,error)
                        }
                    }
                })
                dataTask.resume()
    }
    
    @IBAction func back(_ sender: Any)
    {
//        let vc = storyboard?.instantiateViewController(identifier: "createGroup") as! creatGroupViewController
//        navigationController?.pushViewController(vc     , animated: true)
        navigationController?.popViewController(animated: true)
    }
}

extension creatGroupViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func pickingImg()
    {
        imagepicker.delegate = self
        view.endEditing(true)
        let alert =  UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
        let alert1 = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { UIAlertAction in
            self.openCamera()
        }
        
        let alert2 = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { UIAlertAction in
            self.openGallery()
        }
        let alert3 = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        { UIAlertAction in
        }
        alert.addAction(alert1)
        alert.addAction(alert2)
        alert.addAction(alert3)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagepicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagepicker, animated: true, completion: nil)
        }
        else
        {
            print("Required Device..")
        }
        
    }
    
    func openGallery()
    {
        imagepicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagepicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        let imgData = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
       // img.image = imgData
       // imgOutlet.setImage(imgdata, for: .normal)
        imgOutlet.layer.cornerRadius = 30
        imgOutlet.layer.masksToBounds = true
        imgOutlet.setImage(imgData, for: .normal)
        if let imgurl = info[UIImagePickerController.InfoKey.imageURL] as? URL
        {
            let imgName = imgurl.lastPathComponent
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancel")
    }
}

class createGroupModel:NSObject
{
    var message:String?
    var code:Int?
    var status:Int?
    
    class func mycreategroup(data:NSDictionary) -> createGroupModel
    {
        let myobj = createGroupModel()
        myobj.code = data["code"] as? Int
        myobj.status = data["status"] as? Int
        myobj.message = data["message"] as? String
        return myobj
    }
}
