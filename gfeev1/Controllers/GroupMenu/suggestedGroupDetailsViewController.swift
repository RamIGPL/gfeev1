//
//  suggestedGroupDetailsViewController.swift
//  gfeev1
//
//  Created by IGPL on 14/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class suggestedGroupDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    var groupname = ""
    var groupConnections = 0
    var groupid:Int?
    var group_Postid:Int?
    var groupicon:[String] = []
    var groupArray:[String] = []
    
    var owngroupConections:[Int] = []
    var groupCaptions:[String] = []
    
    var group_postTextArray:[String] = []
    var group_postDateArray:[String] = []
    var group_NameArray:[String] = []
    var group_postimgArray:[String] = []
    var group_postUserIcon:[String] = []
    var group_postID:[Int] = []
   
    let GroupDetailsURL =  "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/groupdetails"
    let groupSuggestedGroup = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/groupposts"
    internal typealias CompletionHandler = ( _ responseObj:groupDetailsModel?, _ errorOBj:NSError?) -> ()
    
    override func viewDidLoad()
    {
        tv.tableFooterView = UIView(frame: .zero)
        super.viewDidLoad()
        print(groupid!,"passed group id sssss ----")
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
        print(userid,accesskey!,groupid!,"current data in GROPUP DETAILS")
        let startFrom = 0
        self.getGroupDetailswithID(userid:userid,accesskey:accesskey!,ge_id:groupid!, callback: { (groupdata, error) in
                    if let groupdata = groupdata
                {
                    print(groupdata.status!,"my group details data")
                    print("in01 \(String(describing: groupdata.message))")
                    if groupdata.status! == 0
                    {
                        print("No data..")
                    }
                    else
                    {
                       for obj in groupdata.group_details!
                       {
                         self.group_NameArray.append(obj.groupname!)
                                                print(obj.groupname!, "Available Groupname")
                                                self.groupname = obj.groupname!
                        
                        DispatchQueue.main.sync
                        {
                            self.currentGroupTitle.text = obj.groupname!
                            self.connections.text = String(obj.connections!)
                            self.Details.text = obj.groupcaption!
                            self.tv.reloadData()
                        }
                    }
                    }
                   
                }
                })
             
        self.groupPostDetails(userid:userid,accesskey:accesskey!,ge_id:groupid!,start:startFrom, callback:
            { (groupdata, error) in
            if let groupdata = groupdata
            {
                print(groupdata.group_poststatus!,"my group post details data")
                print("in01 \(String(describing: groupdata.message))")
                if groupdata.group_poststatus == 1
                {
                    for obj in groupdata.group_posts!
                    {
                        print(obj.group_postText!)
                         self.group_postDateArray.append(obj.group_postdate!)
                         self.group_postTextArray.append(obj.group_postText!)
                         self.group_postID.append(obj.group_postid!)
                         self.group_postimgArray.append(obj.group_postdata!)
                        self.group_postUserIcon.append(obj.group_postuserlogo!)
                                
                                DispatchQueue.main.sync
                                {
                                    self.tv.reloadData()
                                }
                                }
                            }
                            else
                            {
                               print("Nil data")

                            }
                            
                           }
                        })
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(suggestedGroupDetailsViewController.tapFunction))
               Details.isUserInteractionEnabled = true
               Details.addGestureRecognizer(tap)
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer)
    {
       // let myvc = storyboard?.instantiateViewController(identifier: "Main")
      //let fromvc = "suggested"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailedText") as! GroupdescriptionDetailViewController
        vc.detailText = Details.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func groupPostDetails(userid:Int,accesskey:String!,ge_id:Int!,start:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: groupSuggestedGroup)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = groupDetailsModel.Group_PostdetailsStatus(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    
    func getGroupDetailswithID(userid:Int,accesskey:String!,ge_id:Int!, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: GroupDetailsURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
          //  return Title.count
        return group_postDateArray.count
    }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupdetailCell", for: indexPath) as! suggestedGroupDetailsTableViewCell
            cell.date.text = group_postDateArray[indexPath.row]//Followers[indexPath.row]
            cell.groupTitle.text = groupname//group_NameArray[indexPath.row]
            cell.caption.text = group_postTextArray[indexPath.row]
            cell.requestImage(fromURL: group_postimgArray[indexPath.row])
            cell.requestImageforIcon(fromURL: group_postUserIcon[indexPath.row])
            return cell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "postDetails") as! PostImageWithDetailsViewController
           vc.detailText = group_postTextArray[indexPath.row]
          
           self.navigationController?.pushViewController(vc, animated: true)
       }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 300
    }
        
        var Title = ["IT & Non IT  Jobs","PHP Developers","Graphic Designers"]
        var Followers = ["8 Followers","14 Followers","7 Followers"]
        var groupCaption = ["Whatever you're looking for, find it at indeed.co.in in One Simple Search. Find Reviews and Salaries. World's Best Job Site. New Job Posting Everyday","PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.","Computer Graphic Designers , Flex Printing Services, Printers For Visiting Card, Logo Designers, Printing Press, T Shirt Printers"]
    
        @IBOutlet weak var tv: UITableView!
        @IBOutlet weak var profileImg: UIImageView!
        @IBOutlet weak var currentGroupTitle: UILabel!
        @IBOutlet weak var Details: UILabel!
        @IBOutlet weak var connections: UILabel!

    @IBAction func backAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fllowers(_ sender: Any)
    {
        performSegue(withIdentifier: "toFollowers", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toFollowers"
        {
            let destvc = segue.destination as! groupFollowerViewController
            destvc.groupid = groupid
        }

    }
   
}


class groupDetailsModel:NSObject
{
               var group_details:[groupDetailsModel]?
               var id:Int?
               var customerid:Int?
               var groupname:String?
              // var sug_status:Int?
               var grouplogo:String?
               //var grouplogo:URL?
               var groupcaption:String?
               var connections:Int?
    
                var createdate:String?
                var participant:Int?
                var Details_status:Int?
                var groupstatus:Int?
    
                var status:Int?
                var message:String?
                var code:Int?
                
                  var group_posts:[groupDetailsModel]?
                  var group_postid:Int?
                  var group_postText:String?
                  var group_posttype:Int?
                  var group_postlikes:Int?
                  var group_postdate:String?
                  var group_postfirstname:String?
                  var group_postlastname:String?
                  var group_postuserlogo:String?
                  var group_postorgtype_id:Int?
                  var group_postdata:String?
                 
                  var group_poststatus:Int?
                  var group_postmessage:String?
                  var group_postcode:Int?
     
       class func Group_PostdetailsStatus(data:NSDictionary) -> groupDetailsModel
       {
           let groupObj = groupDetailsModel()
           groupObj.group_postcode = data["code"] as? Int
           groupObj.group_poststatus = data["status"] as? Int
           groupObj.group_postmessage = data["message"] as? String
           if let  mygroupData = data["group_posts"] as? NSArray
           {
               var mydataObj:[groupDetailsModel] = []
               for obj in mygroupData
               {
                let localdata = groupDetailsModel.Group_PostdetailsData(data: obj as! NSDictionary)
                    //ownGroupModel.groupdata(data: obj as! NSDictionary)
                   mydataObj.append(localdata)
               }
            groupObj.group_posts = mydataObj
           }
            return groupObj
        }
    class func Group_PostdetailsData(data:NSDictionary) -> groupDetailsModel
    {
        let mygroupObj = groupDetailsModel()
        mygroupObj.group_postid = data["id"] as? Int
        mygroupObj.group_postText = data["posttext"] as? String
        mygroupObj.group_posttype = data["posttype"] as? Int
        mygroupObj.group_postlikes = data["likes"] as? Int
        mygroupObj.group_postdate = data["date"] as? String
        mygroupObj.group_postfirstname = data["firstname"] as? String
        mygroupObj.group_postlastname = data["lastname"] as? String
        mygroupObj.group_postuserlogo = data["userlogo"] as? String
        mygroupObj.group_postorgtype_id = data["orgtype_id"] as? Int
        mygroupObj.group_postdata = data["postdata"] as? String
        return mygroupObj
    }
    
    
    
    class func Groupdetails(data:NSDictionary) -> groupDetailsModel
    {
           let groupObj = groupDetailsModel()
           groupObj.code = data["code"] as? Int
           groupObj.status = data["status"] as? Int
           groupObj.message = data["message"] as? String
           if let  mygroupData = data["group_details"] as? NSArray
           {
               var mydataObj:[groupDetailsModel] = []
               for obj in mygroupData
               {
                let localdata = groupDetailsModel.GroupdetailswithID(data: obj as! NSDictionary)
                    //ownGroupModel.groupdata(data: obj as! NSDictionary)
                   mydataObj.append(localdata)
               }
               groupObj.group_details = mydataObj
           }
            return groupObj
    }
    
    class func GroupdetailswithID(data:NSDictionary) -> groupDetailsModel
    {
        let mygroupObj = groupDetailsModel()
        mygroupObj.id = data["id"] as? Int
        mygroupObj.customerid = data["customerid"] as? Int
        mygroupObj.groupname = data["groupname"] as? String
        mygroupObj.grouplogo = data["grouplogo"] as? String
        mygroupObj.connections = data["connections"] as? Int
        mygroupObj.groupcaption = data["groupcaption"] as? String
        mygroupObj.createdate = data["createdate"] as? String
        mygroupObj.participant = data["participant"] as? Int
        mygroupObj.Details_status = data["Details_status"] as? Int
        mygroupObj.groupstatus = data["groupstatus"] as? Int
        return mygroupObj
    }
}
