//
//  groupphotoUpdateViewController.swift
//  gfeev1
//
//  Created by IGPL on 03/02/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class groupphotoUpdateViewController: UIViewController
{
   var postedimage:UIImage?
    var ge_id:Int?
    internal typealias CompletionHandler = ( _ responseObj:UpdateGroupdataModel?, _ errorOBj:NSError?) -> ()
     let group_profileUpdateURL =  "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/UpdateGrouplogo"
    @IBOutlet weak var imageSelected: UIImageView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        imageSelected.image = postedimage
    }

    @IBAction func cancel(_ sender: UIButton)
    {
        
    }
    @IBAction func post(_ sender: UIButton)
    {
        self.insertpostData(userid:userid,accesskey:accesskey!,ge_id:ge_id!,group_logo:imageSelected.image!, callback:
        { (groupdata, error) in
                         if let groupdata = groupdata
                     {
                         if groupdata.status! != 0
                         {
                           DispatchQueue.main.async
                           {
                               self.view.makeToast(groupdata.message!)
                           }
                         }
                          else
                          {
                            self.dismiss(animated: true, completion: nil)
                          }
                       }
        })
    }
    
    func insertpostData(userid:Int,accesskey:String!,ge_id:Int,group_logo:UIImage, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id)".data(using: String.Encoding.utf8)!)
        postData.append("&group_logo=\(group_logo)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: group_profileUpdateURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = UpdateGroupdataModel.postdataStatus(data: jsonResult)
                         print("data updated today",localObj.message as! String)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
}

class UpdateGroupdataModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    class func postdataStatus(data:NSDictionary) -> UpdateGroupdataModel
    {
        let groupObj = UpdateGroupdataModel()
        groupObj.code = data["code"] as? Int
        groupObj.status = data["status"] as? Int
        groupObj.message = data["message"] as? String
        return groupObj
    }
}
