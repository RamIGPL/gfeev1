//
//  postwithImageViewController.swift
//  gfeev1
//
//  Created by IGPL on 09/01/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class postwithImageViewController: UIViewController
{
    internal typealias CompletionHandler = ( _ respObj:postdataModel?, _ errObj:NSError?) -> ()
    @IBOutlet weak var postTextfield: UITextField!
    let group_postURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/postdata"
    @IBOutlet weak var postImage: UIImageView!
    var gid:Int?
    var postedtext:String?
    var postedimage:UIImage?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        postTextfield.text = postedtext
        postImage.image = postedimage
    }
    
    
    @IBAction func postImageandText(_ sender: Any)
    {
        let postedText = postTextfield.text!
        print(postedText,"data posted")
        let postedImg =  postImage.image//"postImg.currentImage"
        let posttype = 1
        let postdatatype = 1
        let type = 6
        self.insertpostData(userid:userid,accesskey:accesskey!,ge_id:gid!,posttext:postedText, posttype:posttype,postdatatype:postdatatype,type:type,postimage:postedImg!, callback: { (groupdata, error) in
                  if let groupdata = groupdata
            {
                if groupdata.status! != 0
                {
                    print(groupdata.status!,"my group post updated successfully")
                    DispatchQueue.main.async
                    {
                        self.view.makeToast(groupdata.message!)
                        self.navigationController?.popViewController(animated: true)
                    }

                }
                   else
                {
                        print("No data")
                }
            }
            })
    }
    
    func insertpostData(userid:Int,accesskey:String!,ge_id:Int,posttext:String!, posttype:Int,postdatatype:Int,type:Int,postimage:UIImage, callback: @escaping CompletionHandler)
       {
           let headers = ["Content-Type": "application/x-www-form-urlencoded"]
           let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
           postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
           postData.append("&ge_id=\(ge_id)".data(using: String.Encoding.utf8)!)
           postData.append("&posttext=\(posttext!)".data(using: String.Encoding.utf8)!)
           postData.append("&posttype=\(posttype)".data(using: String.Encoding.utf8)!)
           postData.append("&postdatatype=\(postdatatype)".data(using: String.Encoding.utf8)!)
           postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
           postData.append("&postimage=\(postimage)".data(using: String.Encoding.utf8)!)
                
           let request = NSMutableURLRequest(url: NSURL(string: group_postURL)! as URL,
           cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
           
           request.httpMethod = "POST"
           request.allHTTPHeaderFields = headers
           request.httpBody = postData as Data
           
           let session = URLSession.shared
           let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
               
               if let error = error
               {
                   callback(nil, error as NSError)
               }
               else
               {
                   do
                   {
                    
                       if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                       {
//                        data?.removeAll()
                        let localObj = postdataModel.postdataStatus(data: jsonResult)
                        let objToBeSent = "Test Message from Notification"
                            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: objToBeSent)
                           callback(localObj, nil)
                       }
                       else
                       {
                           callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                       }
                   }
                   catch let error as NSError
                   {
                       callback(nil,error)
                   }
               }
           })
           dataTask.resume()
       }
}
