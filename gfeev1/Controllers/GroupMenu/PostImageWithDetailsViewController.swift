//
//  PostImageWithDetailsViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/02/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class PostImageWithDetailsViewController: UIViewController
{
    var detailText = ""
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var details: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        details.text = detailText
    }
    
    @IBAction func dismiss(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
}
