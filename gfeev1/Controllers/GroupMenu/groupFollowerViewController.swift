//
//  groupFollowerViewController.swift
//  gfeev1
//
//  Created by IGPL on 15/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class groupFollowerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    @IBOutlet weak var followerTV: UITableView!
    var groupid:Int?
    var userName:[String] = []
    var userProfileIcons:[String] = []
    let followersURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/group_users"
    var names = ["Manu","Krish","Naveen","Nikhil","Ganesh","Sachin","Vikram","Ramesh"]
    
    @IBOutlet weak var tv: UITableView!
    internal typealias CompletionHandler = ( _ responsObj:groupUsersModel?, _ errorOBj:NSError?) -> ()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupFollowercell", for: indexPath) as! followerTableViewCell
        cell.name.text = userName[indexPath.row]
        cell.requestImageforIcon(fromURL: userProfileIcons[indexPath.row])
       // cell.imageView?.image = UIImage(named: "Cast Copy")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        followerTV.tableFooterView = UIView(frame: .zero)
               let userid = UserDefaults.standard.integer(forKey: "userid")
               let accesskey = UserDefaults.standard.string(forKey: "accesskey")
               print(userid,accesskey!,groupid!,"current data in GROPUP DETAILS")
               let startFrom = 0
               self.groupPostDetails(userid:userid,                accesskey:accesskey!,ge_id:groupid!,start:startFrom, callback: { (groupdata, error) in
                if let groupdata = groupdata
                {
                  print(groupdata.status!,"my user group details data")
                  print("in01 \(String(describing: groupdata.message!))")
                    if groupdata.status! == 1
                    {
                       for obj in groupdata.group_users!
                       {
                           if ((obj.ge_firstname == nil) && (obj.ge_lastname == nil) && (obj.ge_id == nil))
                            {
                                print("Nil data")
                            }
                            else
                            {
                               self.userName.append(obj.ge_firstname!)
                                self.userProfileIcons.append(obj.ge_logo!)
                               DispatchQueue.main.sync
                               {
                                  self.tv.reloadData()
                               }
                             }
                          }
                       }
                       else
                       {
                          print("Nil data")
                       }
               }
         })
      
    }
    
    func groupPostDetails(userid:Int,accesskey:String!,ge_id:Int!,start:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: followersURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = groupUsersModel.Group_usersStatus(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func toBack(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
    
}

class groupUsersModel:NSObject
{
               var group_users:[groupUsersModel]?
               var ge_id:Int?
               var ge_firstname:String?
               var ge_lastname:String?
               var ge_logo:String?
               
                var status:Int?
                var message:String?
                var code:Int?
    
               
       class func Group_usersStatus(data:NSDictionary) -> groupUsersModel
       {
           let groupObj = groupUsersModel()
           groupObj.code = data["code"] as? Int
           groupObj.status = data["status"] as? Int
           groupObj.message = data["message"] as? String
           if let  mygroupData = data["group_users"] as? NSArray
           {
               var mydataObj:[groupUsersModel] = []
               for obj in mygroupData
               {
                    let localdata = groupUsersModel.Group_usersDetails(data: obj as! NSDictionary)
                    //ownGroupModel.groupdata(data: obj as! NSDictionary)
                    mydataObj.append(localdata)
               }
            groupObj.group_users = mydataObj
           }
           return groupObj
        }
    
    class func Group_usersDetails(data:NSDictionary) -> groupUsersModel
    {
        let mygroupObj = groupUsersModel()
        mygroupObj.ge_id = data["ge_id"] as? Int
        mygroupObj.ge_firstname = data["ge_firstname"] as? String
        mygroupObj.ge_lastname = data["ge_lastname"] as? String
        mygroupObj.ge_logo = data["ge_logo"] as? String
        return mygroupObj
    }
   
}
