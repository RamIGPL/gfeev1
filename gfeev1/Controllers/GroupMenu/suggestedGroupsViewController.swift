//
//  suggestedGroupsViewController.swift
//  gfeev1
//
//  Created by IGPL on 14/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit
import SDWebImage
class suggestedGroupsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   let ownGroupURL =  "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv1/grouplist"
    let followGroupURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/followgrp"
    let UnfollowGroupURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/unfollowgrp"
    internal typealias CompletionHandler = ( _ responseObj:ownGroupModel?, _ errorOBj:NSError?) -> ()
    var selectedIndex = 0
      var g_idArray:[Int]=[]
       var g_ownidArray:[Int]=[]
      var groupArray:[String] = []
      var owngroupConections:[Int] = []
      var sug_groupArray:[String] = []
      var sug_groupConections:[Int] = []
      var groupicon:[String] = []
      var sugg_groupicon:[String] = []
      var myimgArray:[UIImage] = []
      var myURL_imgArray:[String] = []
      var mysuggestStatus:[Int] = []
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var groupTitle: UILabel!
    @IBOutlet weak var tv:UITableView!
    var currentgrooupID:Int = 0
    @IBOutlet weak var groupSubtitle: UILabel!
    @IBOutlet weak var selectGroupTypeBtn: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
//    var sugg_orgsTitle = ["HM","Organizations","Groups","Scan","Change Password","Logout"]
//    var sugg_orgsDescr = ["Its HM Geneva House","Organizations","Groups","Scan","Change Password","Logout"]
//    var my_orgsTitle = ["IGPL","Organizations","Groups","Scan"]
//    var my_orgsDescr = ["Innovative graphics Private Limited","Organizations","Groups","Last one"]

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
//        let userid = UserDefaults.standard.integer(forKey: "userid")
//        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
//
        self.getOwnGroupAPI(userid:currentUserId,accesskey:currentAccesskey, callback: { (groupdata, error) in
            if let groupdata = groupdata
            {
            print(groupdata.status!,"my group data")
            print("in01 \(String(describing: groupdata.message))")
            if groupdata.status! == 1
            {
                for obj in groupdata.group!
                {
                  self.groupArray.append(obj.groupname!)
                  self.owngroupConections.append(obj.connections!)
                      self.groupicon.append(obj.grouplogo!)
                        self.g_ownidArray.append(obj.id!)
                        DispatchQueue.main.sync
                        {
                            self.tv.reloadData()
                        }
                  }
                            
                 for obj in groupdata.suggest!
                 {
                    self.sug_groupArray.append(obj.sug_groupname!)
                    print(self.sug_groupArray,"suggested group array")
                    self.sug_groupConections.append(obj.sug_connections!)
                    self.sugg_groupicon.append(obj.sug_grouplogo!)
                    self.g_idArray.append(obj.sug_id!)
                     self.mysuggestStatus.append(obj.sug_status!)
                    print("Group icons---",self.sugg_groupicon)
                    DispatchQueue.main.sync
                    {
                        self.tv.reloadData()
                    }
                }
            }
            else
            {
                print("No data")
            }
            
         }
        })
        
        if groupFrom == "from_CreateGroup"
        {
            segment.selectedSegmentIndex = 1
        }
         else if groupFrom == "from_MenuGroup"
        {
            segment.selectedSegmentIndex = 0
        }

    }
    
//    func getImage(from string: String) -> UIImage?
//    {
//        //2. Get valid URL
//        guard let url = URL(string: string)
//            else {
//                print("Unable to create URL")
//                return nil
//        }
//
//        var image: UIImage? = nil
//        do {
//            //3. Get valid data
//            let data = try Data(contentsOf: url, options: [])
//
//            //4. Make image
//            image = UIImage(data: data)
//        }
//        catch {
//            print(error.localizedDescription)
//        }
//
//        return image
//    }

    //1. Get valid string
   
    
    func getOwnGroupAPI(userid:Int,accesskey:String,callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        
        postData.append("&accesskey=\(accesskey)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: ownGroupURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = ownGroupModel.myownGroupdata(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
        
    }
    

 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
           if segment.selectedSegmentIndex == 0
           {
               return sug_groupArray.count
           }
           if segment.selectedSegmentIndex == 1
           {
               return groupArray.count
           }
           return 1
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupcell1", for: indexPath) as! sugstedGroupsTableViewCell
           if segment.selectedSegmentIndex == 0
           {
               //selectGroupTypeBtn.isHidden = true
            print(mysuggestStatus[indexPath.row],"value of the data")
            if mysuggestStatus[indexPath.row] == 0
            {
                cell.selectGroupTypeBtn.isHidden = false
                cell.unfollowBtn.isHidden = true
                cell.groupTitle.text = sug_groupArray[indexPath.row]
                cell.groupSubtitle.text = String(sug_groupConections[indexPath.row])
                currentgrooupID = g_idArray[indexPath.row]
                print("currentgrooupID in cellfor row at indexpath is---------",currentgrooupID)
                cell.selectGroupTypeBtn.tag = indexPath.row
               // cell.unfollowBtn.image
                cell.selectGroupTypeBtn.addTarget(self, action: #selector(followconnected(sender:)), for: .touchUpInside)
                cell.requestImage(fromURL: sugg_groupicon[indexPath.row])
                return cell

            }
            else if mysuggestStatus[indexPath.row] == 1
            {
                cell.unfollowBtn.isHidden = false
                cell.selectGroupTypeBtn.isHidden = true
                cell.groupTitle.text = sug_groupArray[indexPath.row]
                cell.groupSubtitle.text = String(sug_groupConections[indexPath.row])
                currentgrooupID = g_idArray[indexPath.row]
                cell.unfollowBtn.tag = indexPath.row
                cell.unfollowBtn.backgroundColor = UIColor.white
                print("currentgrooupID in mysuggestStatus[indexPath.row] == 1",currentgrooupID)
                print("currentgrooupID in mysuggestStatus[indexPath.row] == 1 as tag",cell.unfollowBtn.tag)
                cell.unfollowBtn.addTarget(self, action: #selector(unfollowconnected(sender:)), for: .touchUpInside)
                cell.requestImage(fromURL: sugg_groupicon[indexPath.row])
//
            }
            
//            for i in self.sugg_groupicon
//            {
//                let imgUrl = URL(string:i)
//               cell.profilePic.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
//
//            }
         }
        if segment.selectedSegmentIndex == 1
        {
                   //selectOrgTypeBtn.isHidden = false
                cell.selectGroupTypeBtn.isHidden = true
                cell.unfollowBtn.isHidden = true
                cell.groupTitle.text = groupArray[indexPath.row]
                cell.groupSubtitle.text = String(owngroupConections[indexPath.row])
                cell.requestImage(fromURL: groupicon[indexPath.row])
//                for i in self.groupicon
//                {
//                    let imgUrl = URL(string:i)
//                    cell.profilePic.sd_setImage(with: imgUrl, placeholderImage: UIImage(named: "placeholder.png"))
//                }

       }
           
           return cell
       }
    

    func call_FollowAPI(userid:Int,accesskey:String!,ge_id:Int!, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: followGroupURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = ownGroupModel.followGroupData(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    func call_UNFollowAPI(userid:Int,accesskey:String!,ge_id:Int!, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: UnfollowGroupURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = ownGroupModel.UnfollowGroupData(data: jsonResult)
                            //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @objc func followconnected(sender:UIButton)
    {
//        let buttonTag = sender.tag
//        print("Follow and unfollow Group Clicked..",buttonTag)
//        let indexPath = IndexPath(row: sender.tag, section: 0)
//        let cell = tv.cellForRow(at: indexPath) as! sugstedGroupsTableViewCell
//        cell.selectGroupTypeBtn.backgroundColor = UIColor.blue
//        cell.selectGroupTypeBtn.setTitle("UNFOLLOW ", for: .normal)
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
        let tappedGID = g_idArray[sender.tag]
        let suggestedSelected_ID = currentgrooupID
        print("tappedGID Group ID is",suggestedSelected_ID,"CurrenttappedGID is",tappedGID)
            //g_idArray[indexPath.row]
               // print(userid,accesskey!,groupid!,"current data in GROPUP DETAILS")
                self.call_FollowAPI(userid:userid,accesskey:accesskey!,ge_id:tappedGID, callback: { (groupdata, error) in
                            if let groupdata = groupdata
                        {
                            print(groupdata.followGroup_status!,"Followed Group status")
                           //print("in01 \(String(describing: groupdata.message))")
                            print(groupdata.followGroup_message!,"Followed Group message")
                         }
                    })
//                        self.getOwnGroupAPI(userid:userid,accesskey:accesskey!, callback: { (groupdata, error) in
//                            if let groupdata = groupdata
//                        {
//                            print(groupdata.status!,"my group data")
//                            print("in01 \(String(describing: groupdata.message))")
//                            for obj in groupdata.group!
//                            {
//                              //print(obj.groupname!,"Different types are:")
//                                self.groupArray.append(obj.groupname!)
//                                self.owngroupConections.append(obj.connections!)
//                                self.groupicon.append(obj.grouplogo!)
//                                self.g_ownidArray.append(obj.id!)
//
//                                DispatchQueue.main.sync
//                                {
//                                    self.tv.reloadData()
//                                }
//                            }
//
//                            for obj in groupdata.suggest!
//                            {
//                              //print(obj.groupname!,"Different types are:")
//                                self.sug_groupArray.append(obj.sug_groupname!)
//                                print(self.sug_groupArray,"suggested group array")
//                                self.sug_groupConections.append(obj.sug_connections!)
//                                self.sugg_groupicon.append(obj.sug_grouplogo!)
//                                self.g_idArray.append(obj.sug_id!)
//                                self.mysuggestStatus.append(obj.sug_status!)
//                                print("Group icons---",self.sugg_groupicon)
//
//                                for i in self.sugg_groupicon
//                                {
//                                    print("image url from server--",i)
//                                    if let image = self.getImage(from: i)
//                                    {
//                                        self.myimgArray.append(image)
//                                    }
//                                }
//                                DispatchQueue.main.sync
//                                {
//                                    self.tv.reloadData()
//                                }
//                            }
//                         }
//                        })
    }
    
    @objc func unfollowconnected(sender:UIButton)
    {
        let userid = UserDefaults.standard.integer(forKey: "userid")
        let accesskey = UserDefaults.standard.string(forKey: "accesskey")
        let tappedGroupID = g_idArray[sender.tag]
        let suggestedSelected_ID = currentgrooupID
        print("followed UnGroup ID is ",suggestedSelected_ID,"now the current UNGrooupid is",tappedGroupID)
        
        self.call_UNFollowAPI(userid:userid,accesskey:accesskey!,ge_id:tappedGroupID, callback: { (groupdata, error) in
        if let groupdata = groupdata
        {
            //Error nil gett8ing here
        print(groupdata.UnfollowGroup_status!,"Followed UNGroup status")
           //  print("in01 \(String(describing: groupdata.message))")
        print(groupdata.UnfollowGroup_message!,"Followed UNGroup message")
        }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //error index out of range
      //  currentgrooupID = g_idArray[indexPath.row]
       // print(currentgrooupID, "currentgrooupID in didSelectRowAt")
        selectedIndex = indexPath.row
        if segment.selectedSegmentIndex == 0
        {
            currentgrooupID = g_idArray[indexPath.row]
            //performSegue(withIdentifier: "togroupDetails", sender: self)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myvc = storyboard.instantiateViewController(withIdentifier: "togroupDetails") as! mygroupdetailsViewController
             myvc.groupid = g_idArray[selectedIndex]
            navigationController?.pushViewController(myvc, animated: true)
          
        }
        if segment.selectedSegmentIndex == 1
        {
            currentgrooupID = g_ownidArray[indexPath.row]
            performSegue(withIdentifier: "tomygroupDetails", sender: self)
        }
     }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
      /*  if segue.identifier == "togroupDetails"
        {
           let destvc = segue.destination as! suggestedGroupDetailsViewController
           // print("selected group name!!!!!",sug_groupArray[selectedIndex])
//            destvc.groupname = sug_groupArray[selectedIndex]
//            destvc.groupConnections = sug_groupConections[selectedIndex]
           // navigationController?.pushViewController(destvc, animated: true)
            destvc.groupid = g_idArray[selectedIndex]
         }*/
        
         if segue.identifier == "tomygroupDetails"
         {
            let destvc = segue.destination as! mygroupdetailsViewController
            destvc.groupid = g_ownidArray[selectedIndex]
            
//           
         }
    }
    
    @IBAction func back(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl)
    {
        tv.reloadData()
    }
}

class ownGroupModel:NSObject
{
        var group:[ownGroupModel]?
           var id:Int?
           var customerid:Int?
           var groupname:String?
           var grouplogo:String?
           var connections:Int?
           var mygroup:String?
           
           var status:Int?
           var message:String?
           var code:Int?
    
            var suggest:[ownGroupModel]?
            var sug_id:Int?
            var sug_customerid:Int?
            var sug_groupname:String?
            var sug_status:Int?
            var sug_grouplogo:String?
            var sug_mygroup:String?
            var sug_connections:Int?
            
            var followGroup_status:Int?
            var followGroup_message:String?
            var followGroup_code:Int?
    
            var UnfollowGroup_status:Int?
            var UnfollowGroup_message:String?
            var UnfollowGroup_code:Int?
       
        class func followGroupData(data:NSDictionary) -> ownGroupModel
        {
                let fGroupObj = ownGroupModel()
                fGroupObj.followGroup_code = data["code"] as? Int
                fGroupObj.followGroup_status = data["status"] as? Int
                fGroupObj.followGroup_message = data["message"] as? String
                return fGroupObj
        }
    
        class func UnfollowGroupData(data:NSDictionary) -> ownGroupModel
        {
                let UfGroupObj = ownGroupModel()
                UfGroupObj.UnfollowGroup_code = data["code"] as? Int
                UfGroupObj.UnfollowGroup_status = data["status"] as? Int
                UfGroupObj.UnfollowGroup_message = data["message"] as? String
                return UfGroupObj
        }
      
  
    class func myownGroupdata(data:NSDictionary) -> ownGroupModel
    {
        let groupObj = ownGroupModel()
        groupObj.code = data["code"] as? Int
        groupObj.status = data["status"] as? Int
        groupObj.message = data["message"] as? String
        if let  mygroupData = data["group"] as? NSArray
        {
            var mydataObj:[ownGroupModel] = []
            for obj in mygroupData
            {
                let localdata = ownGroupModel.groupdata(data: obj as! NSDictionary)
                mydataObj.append(localdata)
            }
            groupObj.group = mydataObj
        } 
      //  return groupObj
        
        
        if let mysuggestgroup = data["suggest"] as? NSArray
        {
            var mysuggestObj:[ownGroupModel] = []
            for obj in mysuggestgroup
            {
                let localsuggestData = ownGroupModel.suggestGroupdata(data: obj as! NSDictionary)
                mysuggestObj.append(localsuggestData)
            }
            groupObj.suggest = mysuggestObj
            
        }
        return groupObj
     }
    
    class func groupdata(data:NSDictionary) -> ownGroupModel
    {
        let mygroupObj = ownGroupModel()
        mygroupObj.id = data["id"] as? Int
        mygroupObj.customerid = data["customerid"] as? Int
        mygroupObj.groupname = data["groupname"] as? String
        mygroupObj.grouplogo = data["grouplogo"] as? String
        mygroupObj.connections = data["connections"] as? Int
        mygroupObj.mygroup = data["mygroup"] as? String
        return mygroupObj
    }
    
    class func suggestGroupdata(data:NSDictionary) -> ownGroupModel
    {
            let suggestObj = ownGroupModel()
            suggestObj.sug_id = data["id"] as? Int
            suggestObj.sug_customerid = data["customerid"] as? Int
            suggestObj.sug_groupname = data["groupname"] as? String
            suggestObj.sug_grouplogo = data["grouplogo"] as? String
            suggestObj.sug_connections = data["connections"] as? Int
            suggestObj.sug_mygroup = data["mygroup"] as? String
            suggestObj.sug_status = data["status"] as? Int
            return suggestObj
    }
    
    
}

class BorderedButton: UIButton
{
    override init(frame: CGRect) {
        super.init(frame: frame)

        layer.borderColor = tintColor.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 5

        contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("NSCoding not supported")
    }

    override func tintColorDidChange() {
        super.tintColorDidChange()

        layer.borderColor = tintColor.cgColor
    }

    override var isHighlighted: Bool {
        didSet {
            let fadedColor = tintColor.withAlphaComponent(0.2).cgColor

            if isHighlighted {
                layer.borderColor = fadedColor
            } else {
                layer.borderColor = tintColor.cgColor

                let animation = CABasicAnimation(keyPath: "borderColor")
                animation.fromValue = fadedColor
                animation.toValue = tintColor.cgColor
                animation.duration = 0.4
                layer.add(animation, forKey: "")
            }
        }
    }
}

extension UIView {

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
