//
//  GroupdescriptionDetailViewController.swift
//  gfeev1
//
//  Created by IGPL on 11/02/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit

class GroupdescriptionDetailViewController: UIViewController
{
    var detailText = ""
    @IBOutlet weak var detailDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailDescription.text = detailText
        // Do any additional setup after loading the view.
    }

    @IBAction func back(_ sender: Any)
    {
        navigationController?.popViewController(animated: true)
    }
}
