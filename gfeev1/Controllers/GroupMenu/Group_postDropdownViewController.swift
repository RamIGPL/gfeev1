//
//  Group_postDropdownViewController.swift
//  gfeev1
//
//  Created by IGPL on 10/01/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit
protocol postData {
    func postAdded(added:String)
}
class Group_postDropdownViewController:
UIViewController,UIPopoverPresentationControllerDelegate,UITableViewDelegate,UITableViewDataSource
{
    let myArray = ["Edit","Delete","Hide From Timeline"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = myArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if myArray[indexPath.row] == "Edit"
        {
            let popController = self.storyboard?.instantiateViewController(withIdentifier: "EditpostViewController") as! EditpostViewController
                    //popController.Delegate = self
                    popController.currentPostText = currentPost!
                    popController.postid = postid!
                    popController.modalPresentationStyle = UIModalPresentationStyle.popover
                popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
                    popController.preferredContentSize.height = 150
                    popController.preferredContentSize.width =  200
                    popController.popoverPresentationController?.delegate = self
                    popController.popoverPresentationController?.sourceView = view.superview
                    popController.popoverPresentationController?.sourceRect = view.bounds
                    self.present(popController, animated: true, completion: nil)
//                     let objToBeSent = "Test Message from Notification ho"
//                     NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierFromEdit"), object: objToBeSent)
          //  close()
            
           /*         self.present(popController, animated: true)
                    {
                        let objToBeSent = "Test Message from Notification ho"
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierFromEdit"), object: objToBeSent)
//                        let vc = mygroupdetailsViewController()
//                        self.navigationController?.popToViewController(vc, animated: true)
//                         self.dismiss(animated: true, completion: nil)
                    }*/
            
//            DispatchQueue.main.async {
//                self.dismiss(animated: true, completion: nil)
//            }
        }
        if myArray[indexPath.row] == "Delete"
        {
        self.deletePost(userid:userid,accesskey:accesskey!,type:orgtype,postid:postid!, callback:
            { (groupdata, error) in
            if let groupdata = groupdata
            {
              if groupdata.status! != 0
                {
                    DispatchQueue.main.async
                    {
                        self.view.makeToast(groupdata.message!)
                    }
                }
                else
                {
                   print("No data")
                }
              }
            })
            dismiss(animated: true, completion: nil)
        }
        if myArray[indexPath.row] == "Hide From Timeline"
        {
            self.hidePost(userid:userid,accesskey:accesskey!,type:orgtype,postid:postid!, callback:
                { (groupdata, error) in
                if let groupdata = groupdata
                {
                  print(groupdata.hide_status!,"current status of Time line hide post data")
                  if groupdata.hide_status! != 0
                  {
                     DispatchQueue.main.async
                     {
                        self.view.makeToast(groupdata.hide_message!)
                     }
                  }
                  else
                  {
                     print("No data")
                  }
                }
            })

        }
    }
    
    func close()
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    var Delegate:postData?
    var currentPost:String?
    var postid:Int?
    let orgtype = 6
  
    var deletePostURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/deletepost"
    var hidePostURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/hidepost"
    internal typealias CompletionHandler = ( _ responseObj:deletepostModel?, _ errorOBj:NSError?) -> ()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func Edit_post(_ sender: UIButton)
    {
        print(currentPost!,"currentPost in Edit_post(UIButton)")
        let popController = self.storyboard?.instantiateViewController(withIdentifier: "EditpostViewController") as! EditpostViewController
               //popController.Delegate = self
        popController.currentPostText = currentPost!
        popController.postid = postid!
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        popController.preferredContentSize.height = 150
        popController.preferredContentSize.width =  200
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender.superview
       popController.popoverPresentationController?.sourceRect = sender.bounds
        self.present(popController, animated: true, completion: nil)
//        self.present(popController, animated: true)
//        {
//            let vc = mygroupdetailsViewController()
//            self.navigationController?.popToViewController(vc, animated: true)
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
    
    @IBAction func Delete_post(_ sender: UIButton)
    {
       print("present data are in delete action",accesskey!,userid,postid!,orgtype)
        self.deletePost(userid:userid,accesskey:accesskey!,type:orgtype,postid:postid!, callback:
                    { (groupdata, error) in
                if let groupdata = groupdata
                {
                    print(groupdata.status!,"current status of edit post data")
                    if groupdata.status! != 0
                    {
                       DispatchQueue.main.async
                        {
                            self.view.makeToast(groupdata.message!)
                        }
                    }
                    else
                    {
                       print("No data")
                    }
                 }
            })
        dismiss(animated: true, completion: nil)
    }
    
    func deletePost(userid:Int,accesskey:String!,type:Int,postid:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        postData.append("&postid=\(postid)".data(using: String.Encoding.utf8)!)

        let request = NSMutableURLRequest(url: NSURL(string: deletePostURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if let error = error
        {
            callback(nil, error as NSError)
        }
        else
        {
            do
            {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                {
                    let localObj = deletepostModel.deletePostdataStatus(data: jsonResult)
                    callback(localObj, nil)
                }
                else
                {
                    callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                }
            }
            catch let error as NSError
            {
                callback(nil,error)
            }
        }
        })
        dataTask.resume()
    }
    
    @IBAction func HideTimeline_post(_ sender: UIButton)
    {
        self.hidePost(userid:userid,accesskey:accesskey!,type:orgtype,postid:postid!, callback:
            { (groupdata, error) in
            if let groupdata = groupdata
            {
                print(groupdata.hide_status!,"current status of Time line hide post data")
                if groupdata.hide_status! != 0
                {
                  DispatchQueue.main.async
                  {
                     self.view.makeToast(groupdata.hide_message!)
                  }
                }
                else
                {
                   print("No data")
                }
            }
        })
    }
    
    func hidePost(userid:Int,accesskey:String!,type:Int,postid:Int, callback: @escaping CompletionHandler)
    {
       // print(userid,accesskey!,postid,postfeedtext!,type, "Data's are insside methods..")
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        postData.append("&postid=\(postid)".data(using: String.Encoding.utf8)!)

        let request = NSMutableURLRequest(url: NSURL(string: hidePostURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if let error = error
        {
            callback(nil, error as NSError)
        }
        else
        {
            do
            {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                {
                    let localObj = deletepostModel.hidePostdataStatus(data: jsonResult)
                    callback(localObj, nil)
                }
                else
                {
                    callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                }
            }
            catch let error as NSError
            {
                callback(nil,error)
            }
        }
        })
        dataTask.resume()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
//        dismiss(animated: true, completion: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification:Notification)
    {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
         
    }
}


class deletepostModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    var hide_status:Int?
    var hide_message:String?
    var hide_code:Int?
    
    
    class func deletePostdataStatus(data:NSDictionary) -> deletepostModel
    {
        let groupObj = deletepostModel()
        groupObj.code = data["code"] as? Int
        groupObj.status = data["status"] as? Int
        groupObj.message = data["message"] as? String
        return groupObj
    }
    
    class func hidePostdataStatus(data:NSDictionary) -> deletepostModel
     {
         let groupObj = deletepostModel()
         groupObj.hide_code = data["code"] as? Int
         groupObj.hide_status = data["status"] as? Int
         groupObj.hide_message = data["message"] as? String
         return groupObj
     }
}
