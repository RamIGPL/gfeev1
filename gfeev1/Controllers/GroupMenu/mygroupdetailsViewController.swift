//
//  mygroupdetailsViewController.swift
//  gfeev1
//
//  Created by IGPL on 15/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//
import Foundation
import UIKit
import SDWebImage
let userid = UserDefaults.standard.integer(forKey: "userid")
let accesskey = UserDefaults.standard.string(forKey: "accesskey")

class mygroupdetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPopoverPresentationControllerDelegate
{
    var mytitle = ""
    var groupname = ""
    var myconnections = 0
    var groupid:Int?
    var group_postid:Int?
    var startFrom = 0
    var fromgroupIcon = ""
    var Selected_groupid = 0
    @IBOutlet weak var tv: UITableView!
          @IBOutlet weak var profileImg: UIImageView!
          @IBOutlet weak var currentGroupTitle: UILabel!
          @IBOutlet weak var Details: UILabel!
          @IBOutlet weak var currentConnections: UILabel!
    
    @IBOutlet weak var postText: UITextField!
    @IBOutlet weak var groupIcon: UIButton!
     @IBOutlet weak var postImg: UIButton!
    var imagepicker = UIImagePickerController()
    var group_postTextArray:[String] = []
    var group_postDateArray:[String] = []
    var group_postidArray:[Int] = []
    var group_postImgArray:[String] = []
    var group_postuserIcon:[String] = []
    var groupicon:String = ""
    var postObj:[groupDetailsModel] = []
  let GroupDetailsURL =  "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/groupdetails"
     let groupSuggestedGroup = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/groupposts"
    let group_postURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/postdata"
    internal typealias CompletionHandler = ( _ responseObj:groupDetailsModel?, _ errorOBj:NSError?) -> ()
    internal typealias CompletionHandler2 = ( _ responseObj:postdataModel?, _ errorOBj:NSError?) -> ()
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tv.tableFooterView = UIView(frame: .zero)
        Selected_groupid = groupid!
        self.getGroupDetailswithID(userid:userid,accesskey:accesskey!,ge_id:groupid!, callback: { (groupdata, error) in
        if let groupdata = groupdata
        {
            if groupdata.status! != 0
            {
                for obj in groupdata.group_details!
                {
                    self.groupname = obj.groupname!
                     self.groupicon.append(obj.grouplogo!)
                    print(self.groupicon.count)
                    DispatchQueue.main.sync
                    {
                         self.currentGroupTitle.text = obj.groupname!
                         self.currentConnections.text = String(obj.connections!)
                         self.Details.text = obj.groupcaption!
                        print("my group logo is here----",obj.grouplogo!)

                        
                        self.requestImage(fromURL: self.groupicon)
                   }
                }
            }
        }
         
        else
        {
            print("No data")
        }
      })
    self.groupPost_ownDetails(userid:userid,accesskey:accesskey!,ge_id:groupid!,start:startFrom, callback: { (groupdata, error) in
//        if let groupdata = groupdata
//        {
        if let postObj = groupdata
        {
            
            if postObj.group_poststatus == 1
            {
               for obj in postObj.group_posts!
               {
                self.group_postDateArray.append(obj.group_postdate!)
                  self.group_postTextArray.append(obj.group_postText!)
                  self.group_postidArray.append(obj.group_postid!)
                self.group_postImgArray.append(obj.group_postdata!)
                self.group_postuserIcon.append(obj.group_postuserlogo!)
                DispatchQueue.main.sync
                {
                    self.tv.reloadData()
                }
                }
            }
            else
            {
               print("Nil data")
            }
       }
        })
    let tap = UITapGestureRecognizer(target: self, action: #selector(mygroupdetailsViewController.tapFunction))
        Details.isUserInteractionEnabled = true
        Details.addGestureRecognizer(tap)
    }
    @objc func tapFunction(sender:UITapGestureRecognizer)
    {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailedText") as! GroupdescriptionDetailViewController
        vc.detailText = Details.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
      if let postObj = groupdata
      {
          
          if groupdata.group_poststatus == 1
          {
             for obj in groupdata.group_posts!
             {
                print("Current post data-----",obj.group_postText!,obj.group_postdate!)
                self.group_postDateArray.append(obj.group_postdate!)
                self.group_postTextArray.append(obj.group_postText!)
                self.group_postidArray.append(obj.group_postid!)
              self.group_postImgArray.append(obj.group_postdata!)
              self.group_postuserIcon.append(obj.group_postuserlogo!)
              DispatchQueue.main.sync
              {
                  self.tv.reloadData()
              }
              }
          }
          else
          {
             print("Nil data")
          }
     }

     */
     
    
    func requestImage(fromURL: String)
    {
        if let url = URL(string: fromURL)
        {
            profileImg.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
            groupIcon.sd_setImage(with: url, for: .normal, placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
//        func requestImageforIcon(fromURL: String)
//         {
//               if let url = URL(string: fromURL)
//               {
//                   profileImg.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
//                   groupIcon.sd_setImage(with: url, for: .normal, placeholderImage: UIImage(named: "placeholder.png"))
//               }
//           }
    override func viewWillAppear(_ animated: Bool)
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationFromEdit(notification:)), name: Notification.Name("NotificationIdentifierFromEdit"), object: nil)
    }
    @objc func methodOfReceivedNotificationFromEdit(notification:Notification)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification)
    {
    self.groupPost_ownDetails(userid:userid,accesskey:accesskey!,ge_id:groupid!,start:startFrom, callback: { (groupdata, error) in
        if let postObj = groupdata
        {
            if postObj.group_poststatus == 1
            {
                for obj in postObj.group_posts!
                {
                   self.group_postDateArray.append(obj.group_postdate!)
                   self.group_postTextArray.append(obj.group_postText!)
                    self.group_postidArray.append(obj.group_postid!)
                    self.group_postImgArray.append(obj.group_postdata!)
                self.group_postuserIcon.append(obj.group_postuserlogo!)
                    DispatchQueue.main.sync
                    {
                        self.tv.reloadData()
                    }
                }
            }
            else
            {
                print("Nil data")
            }
        }
    })
}
    
    func groupPost_ownDetails(userid:Int,accesskey:String!,ge_id:Int!,start:Int, callback: @escaping CompletionHandler)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
        postData.append("&start=\(start)".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: groupSuggestedGroup)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if let error = error
        {
            callback(nil, error as NSError)
        }
        else
        {
            do
            {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                {
                    self.postObj.removeAll()
                    let localObj = groupDetailsModel.Group_PostdetailsStatus(data: jsonResult)
                    callback(localObj, nil)
                }
                else
                {
                    callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                }
            }
            catch let error as NSError
            {
                callback(nil,error)
            }
        }
    })
        dataTask.resume()
    }
    
    func getGroupDetailswithID(userid:Int,accesskey:String!,ge_id:Int!, callback: @escaping CompletionHandler)
    {
           let headers = ["Content-Type": "application/x-www-form-urlencoded"]
           let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
           postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
           postData.append("&ge_id=\(ge_id!)".data(using: String.Encoding.utf8)!)
           let request = NSMutableURLRequest(url: NSURL(string: GroupDetailsURL)! as URL,
           cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
           request.httpMethod = "POST"
           request.allHTTPHeaderFields = headers
           request.httpBody = postData as Data
           
           let session = URLSession.shared
           let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = groupDetailsModel.Groupdetails(data: jsonResult)
                               //orgTypModel.orgTypefirstLayer(data: josnResult)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }


    @IBAction func dopAction(_ sender: UIButton)
    {
        
    }
    
    @IBAction func postAction(_ sender: UIButton)
    {
       // groupid = groupid
        let postedText = postText.text
        let postedImg =  UIImage(named: "goal")
        let posttype = 1
        let postdatatype = 1
        let type = 6
        self.insertpostData(userid:userid,accesskey:accesskey!,ge_id:Selected_groupid,posttext:postedText, posttype:posttype,postdatatype:postdatatype,type:type,postimage:postedImg!, callback:
            { (groupdata, error) in
                  if let groupdata = groupdata
              {
                  if groupdata.status! != 0
                  {
                        //print(groupdata.status!,"my group post status")
                        //print("my group post message\(String(describing: groupdata.message))")
                    DispatchQueue.main.async
                    {
                        self.view.makeToast(groupdata.message!)
                        self.tv.reloadData()
                    }

                   }
                   else
                   {
                        print("No data")
                    }
                }
               })
    }
    
    func insertpostData(userid:Int,accesskey:String!,ge_id:Int,posttext:String!, posttype:Int,postdatatype:Int,type:Int,postimage:UIImage, callback: @escaping CompletionHandler2)
    {
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]

        let postData = NSMutableData(data: "userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&ge_id=\(ge_id)".data(using: String.Encoding.utf8)!)
        postData.append("&posttext=\(posttext!)".data(using: String.Encoding.utf8)!)
        postData.append("&posttype=\(posttype)".data(using: String.Encoding.utf8)!)
        postData.append("&postdatatype=\(postdatatype)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
        postData.append("&postimage=\(postimage)".data(using: String.Encoding.utf8)!)
             
        let request = NSMutableURLRequest(url: NSURL(string: group_postURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if let error = error
            {
                callback(nil, error as NSError)
            }
            else
            {
                do
                {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                    {
                        let localObj = postdataModel.postdataStatus(data: jsonResult)
                        let objToBeSent = "Test Message from Notification"
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: objToBeSent)
                        callback(localObj, nil)
                    }
                    else
                    {
                        callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                    }
                }
                catch let error as NSError
                {
                    callback(nil,error)
                }
            }
        })
        dataTask.resume()
    }
    
    @IBAction func PicGroupImage(_ sender: UIButton)
    {
        fromgroupIcon = "GroupIcon"
        pickingImg()
    }
    
    override func viewWillDisappear(_ animated: Bool)
     {
        super.viewWillDisappear(true)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return group_postDateArray.count
    }
         
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       let cell = tableView.dequeueReusableCell(withIdentifier: "mygroupdetailCell", for: indexPath) as! mygroupdetailsTableViewCell
        cell.caption.text = group_postTextArray[indexPath.row]
        cell.date.text = group_postDateArray[indexPath.row]
        cell.groupTitle.text = groupname
        cell.requestImage(fromURL: group_postImgArray[indexPath.row])
        cell.requestImageforIcon(fromURL: group_postuserIcon[indexPath.row])
        cell.menuBtn.addTarget(self, action: #selector(menuBtn(sender:)), for: .touchUpInside)
        cell.menuBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            //  let fromvc = "suggestedPost"
              let vc = self.storyboard?.instantiateViewController(withIdentifier: "postDetails") as! PostImageWithDetailsViewController
              vc.detailText = group_postTextArray[indexPath.row]
              self.navigationController?.pushViewController(vc, animated: true)
          }
    @objc func menuBtn(sender:UIButton)
    {
//        let buttonTag = sender.tag
//             print("Follow and unfollow Group Clicked..",buttonTag)
//             let indexPath = IndexPath(row: sender.tag, section: 0)
//             let cell = tv.cellForRow(at: indexPath) as! sugstedGroupsTableViewCell
//             cell.selectGroupTypeBtn.backgroundColor = UIColor.blue
//             cell.selectGroupTypeBtn.setTitle("UNFOLLOW ", for: .normal
          let currentTag = sender.tag
//        let indexPath = IndexPath(row: currentTag, section: 0)
//        let cell = tv.cellForRow(at: indexPath) as! mygroupdetailsTableViewCell
        
        print(group_postTextArray[currentTag],"current post")
        let popController = self.storyboard?.instantiateViewController(withIdentifier: "Group_postDropdownViewController") as! Group_postDropdownViewController
       // popController.Delegate = self
        popController.currentPost = group_postTextArray[currentTag]
        popController.postid = group_postidArray[currentTag]
         popController.modalPresentationStyle = UIModalPresentationStyle.popover
         popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
         popController.preferredContentSize.height = 150
         popController.preferredContentSize.width =  200
         popController.popoverPresentationController?.delegate = self
         popController.popoverPresentationController?.sourceView = sender
         popController.popoverPresentationController?.sourceRect = sender.bounds
//        present(popController, animated: true) {
//            self.dismiss(animated: true, completion: nil)
//        }
         self.present(popController, animated: true, completion: nil)
        //self.present(popController,)
       // dismiss(animated: true, completion: nil)
    }
         func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
             return 200
         }
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
         var GroupName = ""
         var Title = ["IT & Non IT  Jobs","PHP Developers","Graphic Designers"]
         var Followers = ["8 Followers","14 Followers","7 Followers"]
         var groupCaption = ["Whatever you're looking for, find it at indeed.co.in in One Simple Search. Find Reviews and Salaries. World's Best Job Site. New Job Posting Everyday","PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is especially suited for web development and can be embedded into HTML.","Computer Graphic Designers , Flex Printing Services, Printers For Visiting Card, Logo Designers, Printing Press, T Shirt Printers"]


    @IBAction func backAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func fllowers(_ sender: Any)
    {
        performSegue(withIdentifier: "toFollowers", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.identifier == "toFollowers"
        {
            let destvc = segue.destination as! groupFollowerViewController
            destvc.groupid = groupid
        }
        if segue.identifier == "postImg_View"
        {
            let destvc = segue.destination as! postwithImageViewController
            destvc.gid = groupid
            destvc.postedtext = postText.text!
        }
    }
    
    @IBAction func attachImg(_ sender: UIButton)
    {
        fromgroupIcon = "AttachImage"
        imagepicker.delegate = self
               view.endEditing(true)
        self.openGallery()
    }
}

extension mygroupdetailsViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func pickingImg()//30426702617
    {
        imagepicker.delegate = self
        view.endEditing(true)
        let alert = UIAlertController(title: "ChooseImage", message: nil, preferredStyle: .alert)
        let alert1 = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        { UIAlertAction in
            self.openCamera()
        }
        let alert2 = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        { UIAlertAction in
            self.openGallery()
        }
        let alert3 = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        { UIAlertAction in
            
        }
        alert.addAction(alert1)
        alert.addAction(alert2)
        alert.addAction(alert3)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagepicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagepicker, animated: true, completion: nil)
        }
        else
        {
            print("Required Device")
        }
    }
    
    func openGallery()
    {
        imagepicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagepicker, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        //picker.dismiss(animated: true, completion: nil)
        if fromgroupIcon == "GroupIcon"
        {
            print("fromgroupIcon")
            let imgdata = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let destvc = storyboard?.instantiateViewController(identifier: "previewImg") as! groupphotoUpdateViewController
                       destvc.ge_id = groupid
                       destvc.postedimage = imgdata //UIImage(named: imgdata)
            navigationController?.pushViewController(destvc, animated: true)
            groupIcon.setImage(imgdata, for: .normal)
            groupIcon.layer.cornerRadius = 40
            groupIcon.layer.masksToBounds = true
        }
        if fromgroupIcon == "AttachImage"
        {
            print("AttachImage")
            let imgdata = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let destvc = storyboard?.instantiateViewController(identifier: "postImg_View") as! postwithImageViewController
            destvc.gid = groupid
            destvc.postedtext = postText.text
            destvc.postedimage = imgdata //UIImage(named: imgdata)
           // navigationController?.showDetailViewController(destvc, sender: self)
            navigationController?.pushViewController(destvc, animated: true)
        }
            self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIImageView
{
    func setImage(imageString: String, localUri: String = "", placeHolderImage: UIImage? = nil) {

     //   self.sd_setShowActivityIndicatorView(loader)
     //   self.sd_setIndicatorStyle(loaderType)

        if let url = URL(string: localUri), let data = try? Data(contentsOf: url), let img = UIImage(data: data) {

            self.sd_setImage(with: URL(string: imageString), placeholderImage: img, options: SDWebImageOptions(), progress: { (progress, total, url) in

            }) { (img, err, type, url) in

            }


        } else {
            self.sd_setImage(with: URL(string: imageString), placeholderImage: placeHolderImage, options: SDWebImageOptions(), progress: { (progress, total, url) in 

            }) { (img, err, type, url) in

            }
        }
    }
}

class postdataModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    class func postdataStatus(data:NSDictionary) -> postdataModel
    {
        let groupObj = postdataModel()
        groupObj.code = data["code"] as? Int
        groupObj.status = data["status"] as? Int
        groupObj.message = data["message"] as? String
        return groupObj
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
