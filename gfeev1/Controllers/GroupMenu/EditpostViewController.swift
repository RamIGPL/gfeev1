//
//  EditpostViewController.swift
//  gfeev1
//
//  Created by IGPL on 10/01/20.
//  Copyright © 2020 IGPL. All rights reserved.
//

import UIKit
var groupdetailsVC  = mygroupdetailsViewController()
class EditpostViewController: UIViewController
{
    let editPostURL = "https://innovativegraphics.in/projects/gfee/superadmin/api/wsv2/postupdate"
    var currentPostText:String?
    var postid:Int?
    internal typealias CompletionHandler = ( _ responseObj:editpostModel?, _ errorOBj:NSError?) -> ()
    @IBOutlet weak var editPostText: UITextField!
    
    @IBOutlet weak var profilePic: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        editPostText.text = currentPostText!
       // dismisspop()
    //app will stop running
    /*    let objToBeSent = "Test Message from Notification ho"
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierFromEdit"), object: objToBeSent)*/
    }
    func dismisspop()
    {
        let objToBeSent = "Test Message from Notification ho"
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierFromEdit"), object: objToBeSent)
    }

    @IBAction func updatePost(_ sender: UIButton)
    {
       //Data is not updating on postdata from application
        let postedText = currentPostText!
        let orgtype = 6
        print(postedText,"postedText")
        print("present data are",accesskey!,userid,currentPostText!,postid!,orgtype)
        self.updatepostData(userid:userid,accesskey:accesskey!,postid:postid!,postfeedtext:editPostText.text!, type:orgtype, callback:
                    { (groupdata, error) in
                if let groupdata = groupdata
                {
                    print(groupdata.status!,"current status of edit post data")
                    if groupdata.status! != 0
                    {
                       DispatchQueue.main.async
                        {
                            self.view.makeToast(groupdata.message!)
                        //  groupdetailsVC.tv.reloadData()
                        //  tv.reloadData()
                            self.dismiss(animated: true, completion: nil)
                            }

                           }
                           else
                           {
                                print("No data")
                            }
                        }

                      })
    
    }
    
    @IBAction func close(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
    
   func updatepostData(userid:Int,accesskey:String!,postid:Int,postfeedtext:String!,type:Int, callback: @escaping CompletionHandler)
    {
        print(userid,accesskey!,postid,postfeedtext!,type, "Data's are insside methods..")
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        let postData = NSMutableData(data: "&userid=\(userid)".data(using: String.Encoding.utf8)!)
        postData.append("&accesskey=\(accesskey!)".data(using: String.Encoding.utf8)!)
        postData.append("&postid=\(postid)".data(using: String.Encoding.utf8)!)
        postData.append("&postfeedtext=\(postfeedtext!)".data(using: String.Encoding.utf8)!)
        postData.append("&type=\(type)".data(using: String.Encoding.utf8)!)
             
        let request = NSMutableURLRequest(url: NSURL(string: editPostURL)! as URL,
        cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if let error = error
        {
            callback(nil, error as NSError)
        }
        else
        {
            do
            {
                if let jsonResult = try JSONSerialization.jsonObject(with: data!, options:  .mutableLeaves) as? NSDictionary
                {
                    let localObj = editpostModel.editPostdataStatus(data: jsonResult)
                   //NotificationCenter.default.post(name: Notification.Name("editPostUpdate"), object: nil)
                   // callback(localObj, nil)
                    
                    let objToBeSent = "Test Message from Notification"
                                               NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: objToBeSent)
                    callback(localObj, nil)
                }
                else
                {
                    callback(nil, myerror.responseError(reason: "JSON Error") as NSError)
                }
            }
            catch let error as NSError
            {
                callback(nil,error)
            }
        }
        })
        dataTask.resume()
    }
}

class editpostModel:NSObject
{
    var status:Int?
    var message:String?
    var code:Int?
    
    class func editPostdataStatus(data:NSDictionary) -> editpostModel
    {
        let groupObj = editpostModel()
        groupObj.code = data["code"] as? Int
        groupObj.status = data["status"] as? Int
        groupObj.message = data["message"] as? String
        return groupObj
    }
}
