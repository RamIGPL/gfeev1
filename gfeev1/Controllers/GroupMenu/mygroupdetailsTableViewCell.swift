//
//  mygroupdetailsTableViewCell.swift
//  gfeev1
//
//  Created by IGPL on 15/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class mygroupdetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var groupTitle: UILabel!
    
    @IBOutlet weak var groupIcon: UIImageView!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
  
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func requestImage(fromURL: String)
    {
        if let url = URL(string: fromURL)
        {
            groupImage.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
    func requestImageforIcon(fromURL: String)
      {
          if let url = URL(string: fromURL)
          {
              groupIcon.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"))
          }
      }

}
