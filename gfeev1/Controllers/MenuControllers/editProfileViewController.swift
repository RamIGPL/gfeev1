//
//  editProfileViewController.swift
//  gfeev1
//
//  Created by IGPL on 18/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class editProfileViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    let gender = ["Male","Female","Other"]
    let contries = ["India","Pak","US","California","Canada"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gender.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gender[row]
    }
    
    @IBOutlet weak var pickerOutlet: UIPickerView!
    
    @IBOutlet weak var EducationalView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

    
}
