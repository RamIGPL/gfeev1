//
//  profileViewController.swift
//  gfeev1
//
//  Created by IGPL on 18/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import UIKit

class profileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func editProfile(_ sender: Any)
    {
        performSegue(withIdentifier: "toEditProfile", sender: self)
    }
    
    
    @IBOutlet weak var personalview: UIView!
    
}
