//
//  dataModel.swift
//  gfeev1
//
//  Created by IGPL on 21/11/19.
//  Copyright © 2019 IGPL. All rights reserved.
//

import Foundation
class dataModel:NSObject
{
    var login:dataModel?
    var id:Int?
    var firstname:String?
    var lastname:String?
    var email:String?
    var phone:String?
    var logo:String?
    var secretkey:String?
    var status:Bool?
    var message:String?
    var code:Int?
    
    
    func fetchLoginData(data:Dictionary<String,Any>) -> dataModel
    {
        let obj = dataModel()
        
        for (key, val) in data
        {
            if key == "status"
            {
                obj.status = val as? Bool
            }
            
            if key == "message"
            {
                obj.message = val as? String
            }
            
            if key == "code"
            {
                obj.code = val as? Int
            }
            
            if key == "login"
            {
                obj.login = dataModel.initLogin(data: val as! Dictionary<String,Any>)
            }
        }
        return obj
    }
    
    class func initLogin(data:Dictionary<String,Any>) -> dataModel
    {
        let obj = dataModel()
        for(key,val) in data
        {
            
            if key == "id"
            {
                obj.id = val as? Int
            }
            if key == "firstname"
            {
                obj.firstname = val as? String
            }
            
            if key == "lastname"
            {
                 obj.lastname = val as? String
            }
           
            if key == "email"
            {
                 obj.email = val as? String
            }
            if key == "phone"
            {
                 obj.phone = val as? String
            }
            if key == "logo"
            {
                 obj.logo = val as? String
            }
            if key == "secretkey"
            {
                 obj.secretkey = val as? String
            }
        }
       return obj
    }
//
//    func fetchLoginData(data:[String:Any]) -> dataModel
//      {
//          let obj = dataModel()
//          for (key, val) in data
//          {
//              switch key
//              {
//              case "id":
//                  obj.id = val as? Int
//              case "firstname":
//                  obj.firstname = val as? String ?? ""
//              case "lastname":
//                  obj.lastname = val as? String
//              case "email":
//                  obj.email = val as? String
//              case "phone":
//                  obj.phone = val as? String
//              case "logo":
//                  obj.logo = val as? String
//              case "secretkey":
//                   obj.secretkey = val as? String
//              case "status":
//                   obj.status = val as? Bool
//              case "message":
//                  obj.message = val as? String
//              case "code":
//                   obj.code = val as? Int
//              default:
//                  print("Enter the cprrect")
//
//              }
//          }
//          return obj
//      }
    
    
}





//{"login":{"id":182,"firstname":"Ramesh","lastname":"A","email":"rameshragadi@innovativegraphics.in","phone":8546886448,"logo":"https:\/\/gfee.co\/superadmin\/images\/admin\/images\/users\/thumb-176X17658wnirdD1j.jpg","secretkey":"fd40fc1fba0d62eb0a1ccea66f5276d6a73b2ad5088ae7a01a65b961bbd6a29e"},"status":1,"message":"Login Successful","code":201}
